/* Copyright (c) 2022-2022, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef LIBDAG_CANCELABLE_HPP
#define LIBDAG_CANCELABLE_HPP

#include <functional>
#include <memory>
#include <atomic>

namespace libdag {

class icancellable {
public:
    virtual ~icancellable() = default;
    virtual bool is_cancellable() const noexcept = 0;
    virtual bool is_cancelled() const noexcept = 0;
    virtual bool cancel() = 0;
    virtual bool set_uncancellable() noexcept = 0;
};

class cancellable : public icancellable {
public:
    using event_callback = std::function<void(const icancellable &)>;
    void on_cancelled(event_callback &&fn) noexcept
    {
        on_cancelled_ = std::move(fn);
    }

    bool is_cancellable() const noexcept override
    {
        return state_.load() == state::CANCELLABLE;
    }

    bool is_cancelled() const noexcept override
    {
        return state_.load() == state::CANCELLED;
    }

    bool cancel() override
    {
        state expect = state::CANCELLABLE;
        if (std::atomic_compare_exchange_strong(
            &state_, &expect, state::CANCELLED)) {
            on_cancelled_(*this);
            return true;
        }
        return false;
    }

    bool set_uncancellable() noexcept override
    {
        state expect = state::CANCELLABLE;
        return std::atomic_compare_exchange_strong(
            &state_, &expect, state::UNCANCELLABLE);
    }

private:
    enum class state : uint8_t {
        CANCELLABLE,
        CANCELLED,
        UNCANCELLABLE,
    };

    static void default_on_cancel(const icancellable &) {}

    std::atomic<state> state_{state::CANCELLABLE};
    event_callback on_cancelled_{default_on_cancel};
};

class cancellation_point {
public:
    explicit cancellation_point(std::shared_ptr<icancellable> &&cancellation)
        : cancellation_{std::move(cancellation)}
    {}

    bool is_cancellable() const noexcept
    {
        return cancellation_ ? cancellation_->is_cancellable() : false;
    }

    bool is_cancelled() const noexcept
    {
        return cancellation_ ? cancellation_->is_cancelled() : false;
    }

    bool cancel() noexcept
    {
        return cancellation_ ? cancellation_->cancel() : false;
    }

private:
    std::shared_ptr<icancellable> cancellation_;
};

} // namespace libdag

#endif
