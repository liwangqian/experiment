/* Copyright (c) 2022-2022, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef LIBDAG_CHANNEL_HPP
#define LIBDAG_CHANNEL_HPP

#include <vector>
#include <atomic>
#include <algorithm>
#include "libdag/link.hpp"

namespace libdag {

class channel : public link {
public:
    ~channel() override = default;
    channel() = default;

    channel(channel &&ch) noexcept
    {
        buff_.swap(ch.buff_);
    }

    channel &operator=(channel &&ch) noexcept
    {
        buff_.swap(ch.buff_);
        return *this;
    }

    size_t bytes_available() const noexcept override
    {
        return buff_.size();
    }

    size_t bytes_writable() const noexcept override
    {
        return 0;
    }

    size_t read(std::byte *bytes, size_t count) noexcept override
    {
        if (bytes == nullptr ||
            count == 0 ||
            count > buff_.size()) {
            return 0;
        }

        std::copy(buff_.begin(), buff_.begin() + count, bytes);
        return count;
    }

    size_t write(const std::byte *bytes, size_t count) noexcept override
    {
        if (bytes == nullptr || count == 0) {
            return 0;
        }
        buff_.reserve(count);
        std::copy(bytes, bytes + count, buff_.begin());
        return count;
    }

private:
    std::vector<std::byte> buff_;
};

} // namespace libdag


#endif
