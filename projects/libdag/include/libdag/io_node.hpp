/* Copyright (c) 2022-2022, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef IO_NODE_HPP_INCLUDED
#define IO_NODE_HPP_INCLUDED

#include <vector>
#include <stdexcept>
#include "libdag/node.hpp"
#include "libdag/readable.hpp"
#include "libdag/writable.hpp"

namespace libdag {

class io_node : public node {
public:
    ~io_node() override = default;

    size_t num_input() const noexcept
    {
        return inputs_.size();
    }

    size_t num_output() const noexcept
    {
        return outputs_.size();
    }

    readable_ptr input(size_t index) noexcept
    {
        if (index >= num_input()) {
            return nullptr;
        }
        return inputs_[index];
    }

    writable_ptr output(size_t index) noexcept
    {
        if (index >= num_output()) {
            return nullptr;
        }
        return outputs_[index];
    }

    const readable_ptr input(size_t index) const noexcept
    {
        if (index >= num_input()) {
            return nullptr;
        }
        return inputs_[index];
    }

    const writable_ptr output(size_t index) const noexcept
    {
        if (index >= num_output()) {
            return nullptr;
        }
        return outputs_[index];
    }

    bool set_input(size_t index, readable_ptr input) noexcept
    {
        if (index >= num_input()) {
            return false;
        }
        inputs_[index] = input;
        return true;
    }

    bool set_output(size_t index, writable_ptr output) noexcept
    {
        if (index >= num_output()) {
            return false;
        }
        outputs_[index] = output;
        return true;
    }

protected:
    io_node(size_t n_input, size_t n_output)
        : inputs_{n_input}, outputs_{n_output}
    {}

    std::vector<readable_ptr> inputs_;
    std::vector<writable_ptr> outputs_;
};

using io_node_ptr = io_node *;

} // namespace libdag


#endif /* IO_NODE_HPP_INCLUDED */
