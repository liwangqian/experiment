/* Copyright (c) 2021, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "libfsm/state_machine.h"
#include <string>
#include <unordered_map>

namespace libfsm {

struct state_machine::data {
    std::string name;
    core::transition_ptr transition{nullptr};
    std::unordered_map<state_id, core::state_ptr> states;
    state_id init_state{};
    state_id curr_state{};
    state_id last_state{};

    explicit data(const char *name_)
        : name{name_}
    {
    }
};

state_machine::state_machine(state_id id, const char *name_)
    : state(id)
    , d_{new state_machine::data{name_}}
{
}

state_machine::~state_machine()
{
    delete d_;
}

void state_machine::handle(core::event_ptr evt)
{
    if (evt == nullptr) {
        return;
    }

    auto new_state = d_->curr_state;
    auto &sta = d_->states[d_->curr_state];
    if (sta != nullptr) {
        sta->handle(evt);
        new_state = d_->transition->transit(sta->id(), evt->id());
    }

    if (new_state != d_->curr_state) {
        d_->last_state = d_->curr_state;
        d_->curr_state = new_state;
        //!TODO: notify state changed.
    }

    return;
}

const char *state_machine::name() const noexcept
{
    return d_->name.c_str();
}

void state_machine::set_transition(core::transition_ptr transition)
{
    d_->transition = transition;
}

core::transition_ptr state_machine::get_transition()
{
    return d_->transition;
}

void state_machine::add_state(core::state_ptr sta)
{
    d_->states[sta->id()] = sta;
}

core::state_ptr state_machine::del_state(state_id id)
{
    auto r = get_state(id);
    if (r != nullptr) {
        (void)d_->states.erase(id);
    }
    return r;
}

core::state_ptr state_machine::get_state(state_id id)
{
    auto iter = d_->states.find(id);
    if (iter != d_->states.end()) {
        return iter->second;
    }
    return nullptr;
}

void state_machine::set_init_state(state_id id)
{
    d_->init_state = id;
}

core::state::state_id state_machine::get_current_state() const
{
    return d_->curr_state;
}

} // namespace libfsm
