/* Copyright (c) 2021, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "cpplua/core/lang/keyword.h"
#include <unordered_map>

CPPLUA_NS_BEGIN

namespace {
std::unordered_map<string_t, const keyword_t> keyword_table{
    {"do",       KW_DO},
    {"if",       KW_IF},
    {"in",       KW_IN},
    {"or",       KW_OR},
    {"and",      KW_AND},
    {"end",      KW_END},
    {"for",      KW_FOR},
    {"not",      KW_NOT},
    {"else",     KW_ELSE},
    {"then",     KW_THEN},
    {"goto",     KW_GOTO},
    {"break",    KW_BREAK},
    {"local",    KW_LOCAL},
    {"until",    KW_UNTIL},
    {"while",    KW_WHILE},
    {"elseif",   KW_ELSEIF},
    {"repeat",   KW_REPEAT},
    {"return",   KW_RETURN},
    {"function", KW_FUNCTION}
};
}

bool is_keyword(const string_t &val)
{
    return keyword_table.find(val) != keyword_table.end();
}

keyword_t get_keyword_id(const string_t &val)
{
    return keyword_table.at(val);
}

CPPLUA_NS_END
