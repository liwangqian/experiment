/* Copyright (c) 2021, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "cpplua/tool/symbol/table_type.h"
#include <unordered_map>
#include <vector>

CPPLUA_NS_BEGIN
namespace tool::__detail {

struct table_fields {
    ~table_fields() = default;
    table_fields() = default;

    bool define(const string_t &name, symbol *s);
    bool define(int index, symbol *s);

    symbol *resolve(const string_t &name);
    symbol *resolve(int index);

    std::unordered_map<string_t, symbol *> named_fields;
    std::vector<symbol *> index_fields;
};

bool table_fields::define(const string_t &name, symbol *s)
{
    if (named_fields.find(name) != named_fields.end()) {
        return false;
    }

    named_fields[name] = s;
    return true;
}

bool table_fields::define(int index, symbol *s)
{
    if (index < 0) index = index_fields.size() + index;
    if (index < 0) return false;

    if (index >= index_fields.size()) index_fields.resize(index + 1);
    index_fields[index] = s;
    return true;
}

symbol *table_fields::resolve(const string_t &name)
{
    return named_fields[name];
}

symbol *table_fields::resolve(int index)
{
    if (index < 0) index = index_fields.size() + index;
    if (index < 0) return nullptr;
    if (index >= index_fields.size()) return nullptr;

    return index_fields[index];
}

////////////////////////////////////////////////////////////////////////////////

table_type::~table_type()
{
    delete fields;
}

table_type::table_type(scope *parent, uint32_t depth)
    : scope{parent, depth + 1}
{
    fields = new table_fields;
}

bool table_type::define(const string_t &name, symbol *s)
{
    return fields->define(name, s);
}

bool table_type::define(int index, symbol *s)
{
    return fields->define(index, s);
}

symbol *table_type::resolve(const string_t &name)
{
    return fields->resolve(name);
}

symbol *table_type::resolve(int index)
{
    return fields->resolve(index);
}

const char *table_type::type_name() const noexcept
{
    return "table";
}

} // namespace tool::__detail
CPPLUA_NS_END
