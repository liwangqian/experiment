/* Copyright (c) 2021, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#pragma once

#include "cpplua/config.h"
#include "cstring"

CPPLUA_NS_BEGIN
namespace __detail {

#define BIT(n) (1U << n)

enum feature_bits : uint16_t {
    label = BIT(0U),
    empty_statement = BIT(1U),
    hex_escapes = BIT(2U),
    skip_whitespace_escape = BIT(3U),
    strict_escapes = BIT(4U),
    unicode_escapes = BIT(5U),
    bitwise_operators = BIT(6U),
    integer_division = BIT(7U),
    contextual_goto = BIT(8U),
};

class feature {
public:
    bool supports(feature_bits features)
    {
        return (m_feature & features) == features;
    }

    ~feature() = default;

    feature(unsigned int val) : m_feature(val)
    {}

private:
    unsigned int m_feature;
};

} // namespace __detail

// exports
using feature_enum_t = __detail::feature_bits;
using feature_t = __detail::feature;

static inline feature_t make_feature(const char *lua_version)
{
    if (strcmp(lua_version, "5.1") == 0) {
        return {0};
    }

    if (strcmp(lua_version, "5.2") == 0) {
        return {
            feature_enum_t::label |
            feature_enum_t::empty_statement |
            feature_enum_t::hex_escapes |
            feature_enum_t::skip_whitespace_escape |
            feature_enum_t::strict_escapes};
    }

    if (strcmp(lua_version, "5.3") == 0) {
        return {
            feature_enum_t::label |
            feature_enum_t::empty_statement |
            feature_enum_t::hex_escapes |
            feature_enum_t::skip_whitespace_escape |
            feature_enum_t::strict_escapes |
            feature_enum_t::unicode_escapes |
            feature_enum_t::bitwise_operators |
            feature_enum_t::integer_division};
    }

    if (strcmp(lua_version, "LuaJit") == 0) {
        return {
            feature_enum_t::label |
            feature_enum_t::contextual_goto |
            feature_enum_t::hex_escapes |
            feature_enum_t::skip_whitespace_escape |
            feature_enum_t::strict_escapes |
            feature_enum_t::unicode_escapes};
    }

    return {0};
}

CPPLUA_NS_END
