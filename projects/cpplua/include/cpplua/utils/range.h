/* Copyright (c) 2021, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#pragma once

#include "cpplua/config.h"
#include "cpplua/utils/position.h"
#include "fmt/format.h"
#include "nlohmann/json.hpp"

CPPLUA_NS_BEGIN

namespace __detail {

template <typename T>
struct range {
    T start{};
    T end{};

    inline bool operator==(const range &r) const
    {
        return start == r.start && end == r.end;
    }

    inline bool operator!=(const range &r) const
    {
        return start != r.start || end != r.end;
    }

    inline bool contains(const range &r) const
    {
        return start <= r.start && end >= r.end;
    }

    inline string_t to_string() const
    {
        return fmt::format("{} start = {}, end = {} {}", '{', start, end, '}');
    }
};

template <typename T>
inline void to_json(nlohmann::json &json, const range<T> &r)
{
    json["start"] = r.start;
    json["end"] = r.end;
}

} // namespace __detail

// exports
using range_t = __detail::range<position_t>;
using vrange_t = __detail::range<uint32_t>;

CPPLUA_NS_END

namespace fmt {
template<typename T>
struct formatter<cpplua::__detail::range<T>>
    : formatter<string_view> {
    template<typename FormatContext>
    auto format(const cpplua::__detail::range<T> &r, FormatContext &ctx)
    {
        return formatter<string_view>::format(r.to_string(), ctx);
    }
};
}
