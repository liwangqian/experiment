/* Copyright (c) 2021, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#pragma once

#include "cpplua/config.h"
#include <vector>
#include <algorithm>

CPPLUA_NS_BEGIN
namespace __detail {
template<typename _T>
class stack {
public:
    typedef std::vector<_T> container_type;
    typedef typename std::vector<_T>::size_type size_type;
    typedef _T value_type;

    size_type size() const
    {
        return m_stack.size();
    }

    void push(const value_type &elem)
    {
        m_stack.push_back(elem);
    }

    void pop(size_type n_elem = 1)
    {
        size_type new_size = n_elem >= m_stack.size() ? 0 : m_stack.size() - n_elem;
        m_stack.resize(new_size);
    }

    bool contains(const value_type &elem) const
    {
        return std::find(m_stack.crbegin(), m_stack.crend(), elem) != m_stack.crend();
    }

    value_type &find(const value_type &elem)
    {
        return std::find(m_stack.rbegin(), m_stack.rend(), elem);
    }

    const value_type &find(const value_type &elem) const
    {
        return std::find(m_stack.crbegin(), m_stack.crend(), elem);
    }

    stack() = default;
    ~stack() = default;

private:
    std::vector<_T> m_stack;
};

} // namespace __detail

// exports
template<typename _T> using stack_t = __detail::stack<_T>;

CPPLUA_NS_END
