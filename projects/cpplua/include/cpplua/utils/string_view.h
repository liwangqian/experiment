/* Copyright (c) 2021, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#pragma once


#include "cpplua/config.h"

CPPLUA_NS_BEGIN
namespace __detail {

template<typename _CharT = char>
class string_view {
public:
    typedef _CharT value_type;

    string_view() = default;
    ~string_view() = default;

    string_view(const string_view &) = default;
    string_view &operator=(const string_view &) = default;

    string_view(const _CharT *ref, std::size_t size)
        : m_ref{ref}, m_size(size)
    {}

    _CharT operator[](int index)
    {
        return m_ref[index > 0 ? index : (index + m_size)];
    }

    _CharT at(int index) throw(std::out_of_range)
    {
        if (std::abs(index) >= m_size)
            throw std::out_of_range("index out-of-range");

        return this->operator[](index);
    }

    std::basic_string<_CharT> to_string()
    {
        return std::basic_string<_CharT>(m_ref, m_size);
    }

    bool is_valid() const
    {
        return (m_ref != nullptr) && (m_size > 0);
    }

private:
    const _CharT *m_ref = nullptr;
    std::size_t m_size = 0;
};

} // namespace __detail

using string_view_t = __detail::string_view<char>;
using wstring_view_t = __detail::string_view<wchar_t>;

CPPLUA_NS_END


