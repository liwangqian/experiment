/* Copyright (c) 2021, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#pragma once

#include "cpplua/config.h"
#include "nlohmann/json.hpp"
#include "fmt/format.h"

CPPLUA_NS_BEGIN

namespace __detail {

struct position {
    std::uint32_t line{0};   /* 行号 */
    std::uint32_t column{0}; /* 列号 */
    std::uint32_t offset{0}; /* 与第一个字符的偏移量 */

    inline bool operator==(const position &other) const
    {
        return line == other.line && column == other.column && offset == other.offset;
    }

    inline bool operator!=(const position &other) const
    {
        return !this->operator==(other);
    }

    inline bool operator<(const position &other) const
    {
        return offset < other.offset;
    }

    inline bool operator>(const position &other) const
    {
        return offset > other.offset;
    }

    inline bool operator<=(const position &other) const
    {
        return offset <= other.offset;
    }

    inline bool operator>=(const position &other) const
    {
        return offset >= other.offset;
    }

    inline string_t to_string() const
    {
        return fmt::format("{} line = {}, column = {}, offset = {} {}", '{', line, column, offset, '}');
    }

};

inline void to_json(nlohmann::json &json, const position &p)
{
    json["line"] = p.line;
    json["column"] = p.column;
    json["offset"] = p.offset;
}

} // namespace __detail

// exports
using position_t = __detail::position;

CPPLUA_NS_END

namespace fmt {
template<>
struct formatter<cpplua::position_t>
    : formatter<string_view> {
    template<typename FormatContext>
    auto format(const cpplua::position_t &p, FormatContext &ctx)
    {
        return formatter<string_view>::format(p.to_string(), ctx);
    }
};
}
