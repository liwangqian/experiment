/* Copyright (c) 2021, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#pragma once

#include "cpplua/config.h"
#include "cpplua/utils/range.h"

#include <vector>
#include <array>
#include <limits>

CPPLUA_NS_BEGIN
namespace utils {

struct lineinfo_t {
    enum { LINE_COUNT_PER_CHUNK = 512 }; // 512 * 4 = 2K Bytes, 小于CPU一级缓存
    using line_info_chunk_t = std::array<uint32_t, LINE_COUNT_PER_CHUNK>;
    using chunk_vec_t = std::vector<line_info_chunk_t *>;

    ~lineinfo_t();
    lineinfo_t();

    lineinfo_t(const lineinfo_t &) = delete;
    lineinfo_t &operator=(const lineinfo_t &) = delete;

    lineinfo_t(lineinfo_t &&rhs) noexcept;
    lineinfo_t &operator=(lineinfo_t &&rhs) noexcept;

    void new_line(uint32_t offset);

    position_t to_position(uint32_t offset) const;
    uint32_t to_offset(position_t pos) const;

    std::size_t chunk_count() const
    {
        return chunks.size();
    }

private:
    chunk_vec_t chunks;
    line_info_chunk_t *current_chunk{nullptr};
    uint32_t slot{0};
};

} // namespace utils
CPPLUA_NS_END


