/* Copyright (c) 2021, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#pragma once

#include "cpplua/config.h"
#include "cpplua/feature.h"
#include "cpplua/core/lang/token.h"
#include "cpplua/core/lang/error.h"
#include "cpplua/utils/noncopyable.h"
#include "cpplua/utils/lineinfo.h"

CPPLUA_NS_BEGIN

namespace __detail {

class lexer : public noncopyable_t {
public:
    ~lexer() = default;
    lexer() = delete;
    lexer(feature_t features, const char *input, uint32_t len);

    token_t lex();
    const utils::lineinfo_t &lineinfo() const;

private:
    void skip_space();
    bool skip_eol();
    void new_line(uint32_t offset);

    char get();

    token_t scan_comment();
    token_t scan_ident_or_keyword();
    token_t scan_string_literal();
    token_t scan_long_string_literal();
    token_t scan_numeric_literal();
    token_t scan_vararg_literal();
    token_t scan_punctuator(const char *expr, std::size_t len);

    token_t make_eof() const;

    string_view_t read_hex_literal();
    string_view_t read_dec_literal();
    string_view_t read_long_string(bool is_comment);
    bool expect_n_char(char c, std::size_t n);
    inline position_t curr_position() const;

    const char *m_input;
    uint32_t m_length;
    uint32_t m_index;
    feature_t m_feature;
    utils::lineinfo_t m_lineinfo;
};

inline position_t lexer::curr_position() const
{
    return m_lineinfo.to_position(m_index);
}

} // namespace __detail

// exports
using lexer_t = __detail::lexer;

CPPLUA_NS_END
