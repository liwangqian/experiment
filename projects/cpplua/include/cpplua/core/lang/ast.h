/* Copyright (c) 2021, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#pragma once

#include "cpplua/core/lang/ast/base.h"
#include "cpplua/core/lang/ast/chunk.h"
#include "cpplua/core/lang/ast/label.h"
#include "cpplua/core/lang/ast/local.h"
#include "cpplua/core/lang/ast/ident.h"
#include "cpplua/core/lang/ast/unary.h"
#include "cpplua/core/lang/ast/cond.h"
#include "cpplua/core/lang/ast/dostmt.h"
#include "cpplua/core/lang/ast/while.h"
#include "cpplua/core/lang/ast/goto.h"
#include "cpplua/core/lang/ast/repeat.h"
#include "cpplua/core/lang/ast/break.h"
#include "cpplua/core/lang/ast/return.h"
#include "cpplua/core/lang/ast/member.h"
#include "cpplua/core/lang/ast/func.h"
#include "cpplua/core/lang/ast/loop.h"
#include "cpplua/core/lang/ast/assign.h"
#include "cpplua/core/lang/ast/call.h"
#include "cpplua/core/lang/ast/binary.h"
#include "cpplua/core/lang/ast/index.h"
#include "cpplua/core/lang/ast/literal.h"
#include "cpplua/core/lang/ast/table.h"

#include <memory>

CPPLUA_NS_BEGIN

namespace ast {

template<typename NodeType, typename ...Args>
node_ptr_t make_node(Args... args)
{
    return std::make_shared<NodeType>(args...);
}

} // namespace ast

CPPLUA_NS_END


