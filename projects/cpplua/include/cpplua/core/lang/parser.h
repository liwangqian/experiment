/* Copyright (c) 2021, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#pragma once


#include "cpplua/config.h"
#include "cpplua/core/lang/ast.h"
#include "cpplua/core/lang/error.h"
#include "cpplua/core/lang/lexer.h"
#include "cpplua/core/lang/marker.h"
#include "cpplua/core/lang/token.h"
#include "cpplua/utils/scope.h"

#include <vector>
#include <stack>

CPPLUA_NS_BEGIN

namespace __detail {

struct parser_config_t {
    const char *lua_version;
};

using ast_node_t = ast::node_ptr_t;
using marker_ptr_t = std::shared_ptr<marker_t>;

class parser {
public:
    parser(const parser_config_t &config, const char *input, uint32_t len);
    ~parser() = default;

    ast_node_t parse();

private:
    void next();
    bool consume(const char *value);
    void expect(const char *value);

    void validate_var(const ast_node_t &var) const;

    void start_node();
    ast_node_t finish_node(ast_node_t node);
    void create_scope();
    void destroy_scope();
    static void attach_scope(ast_node_t &node, bool is_local);
    bool scope_has_name(const string_t &name);
    void scope_ident_name(const string_t &name);
    void scope_ident(ast_node_t &node);
    void ensure_eof();
    void check_body(const std::vector<ast_node_t> &body);

    ast_node_t parse_chunk();
    std::vector<ast_node_t> parse_block();

    ast_node_t parse_stmt();
    ast_node_t parse_local_stmt();
    ast_node_t parse_label_stmt();
    ast_node_t parse_if_stmt();
    ast_node_t parse_do_stmt();
    ast_node_t parse_for_stmt();
    ast_node_t parse_goto_stmt();
    ast_node_t parse_while_stmt();
    ast_node_t parse_break_stmt();
    ast_node_t parse_repeat_stmt();
    ast_node_t parse_return_stmt();
    ast_node_t parse_function_stmt();
    ast_node_t parse_table_constructor();
    ast_node_t parse_assignment_or_call_stmt();

    ast_node_t parse_ident();
    ast_node_t parse_expr();
    ast_node_t parse_sub_expr(int min_precedence);
    ast_node_t parse_expect_expr();
    ast_node_t parse_prefix_expr();
    ast_node_t parse_primary_expr();
    ast_node_t parse_call_expr(ast_node_t &base);

    ast_node_t parse_function_name();
    ast_node_t parse_function_decl(const ast_node_t &name, bool is_local);

    bool is_block_follow(const token_t &token) const;
    bool is_unary_op(const token_t &token) const;
    bool is_call_expr(const ast_node_t &expr) const;
    int binary_precedence(const string_t &op) const;

    marker_ptr_t create_location_marker();
    void complete_location_marker(ast_node_t &node);
    void pop_location_marker();

    ast_node_t unexpected(const token_t &tok) const;
    ast_node_t raise_unexpected_token(const string_t &desc, const token_t &tok) const;

    std::stack<marker_ptr_t> m_locations;
    std::stack<scope_t<string_t> *> m_scopes;
    stack_t<string_t> m_local_stack;
    token_t m_token;
    token_t m_prev;
    token_t m_lookahead;
    lexer_t m_lexer;
};

} // namespace __detail

// exports
using parser_t = __detail::parser;

CPPLUA_NS_END



