/* Copyright (c) 2021, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#pragma once

#include "cpplua/core/lang/ast/base.h"

CPPLUA_NS_BEGIN

namespace ast {
namespace __detail {

class binaryop_node : public base_node {
public:
    static constexpr node_type class_type = expr_binary;

    const node_ptr_t &left() const
    {
        return m_left;
    }

    const node_ptr_t &right() const
    {
        return m_right;
    }

    const string_t &oper() const
    {
        return m_oper;
    }

    void to_json(nlohmann::json &json) const override
    {
        base_node::to_json(json);
        json["operator"] = m_oper;
        json["left"] = m_left;
        json["right"] = m_right;
    }

    explicit binaryop_node(string_t oper, node_ptr_t left, node_ptr_t right)
        : base_node{class_type}, m_oper{std::move(oper)}, m_left{std::move(left)}, m_right{std::move(right)}
    {
    }

    ~binaryop_node() = default;
private:
    string_t m_oper;
    node_ptr_t m_left;
    node_ptr_t m_right;
};

} // namespace __detail

// exports
using binaryop_t = __detail::binaryop_node;

} // namespace ast

CPPLUA_NS_END
