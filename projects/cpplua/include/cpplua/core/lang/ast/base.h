/* Copyright (c) 2021, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#pragma once

#include "cpplua/config.h"
#include "cpplua/utils/range.h"
#include "nlohmann/json.hpp"
#include <memory>
#include <cassert>
#include <sstream>

CPPLUA_NS_BEGIN

namespace ast {
namespace __detail {

enum node_type {
    stmt_label,
    stmt_break,
    stmt_goto,
    stmt_return,
    stmt_if,
    stmt_if_clause,
    stmt_elseif_clause,
    stmt_else_clause,
    stmt_do,
    stmt_while,
    stmt_repeat,
    stmt_local,
    stmt_assignment,
    stmt_call,
    stmt_function,
    stmt_for_numeric,
    stmt_for_generic,
    stmt_chunk,
    stmt_ident,
    stmt_literal,
    stmt_table_key,
    stmt_table_key_string,
    stmt_table_value,

    expr_table_constructor,
    expr_binary,
    expr_unary,
    expr_member,
    expr_index,
    expr_call,
    expr_table_call,
    expr_string_call,

    comment,

    node_type_max
};

struct base_node {
    node_type type;
    vrange_t range;
    bool is_local;

    base_node(node_type type)
        : type{type}
    {
    }

    bool is_type(node_type target)
    {
        return target == type;
    }

    template<typename child>
    const child &cast_to() const
    {
        assert(child::class_type == type);
        return static_cast<const child &>(*this);
    }

    template<typename child>
    child &cast_to()
    {
        assert(child::class_type == type);
        return static_cast<child &>(*this);
    }

    virtual void to_json(nlohmann::json &json) const
    {
        json["type"] = type;
        json["range"] = range;
    }
};

} // namespace __detail

// exports
using node_type_t = __detail::node_type;
using node_ptr_t = std::shared_ptr<__detail::base_node>;

namespace __detail {

inline void to_json(nlohmann::json &json, const node_ptr_t &v)
{
    if (v) v->to_json(json);
}

}

} // namespace ast

CPPLUA_NS_END
