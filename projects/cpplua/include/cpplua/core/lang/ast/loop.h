/* Copyright (c) 2021, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#pragma once

#include "cpplua/core/lang/ast/base.h"
#include <vector>

CPPLUA_NS_BEGIN

namespace ast {
namespace __detail {

class fornumeric_node : public base_node {
public:
    static constexpr node_type class_type = stmt_for_numeric;

    const node_ptr_t &variable() const
    { return m_var; }

    const node_ptr_t &start() const
    { return m_start; }

    const node_ptr_t &end() const
    { return m_end; }

    const node_ptr_t &step() const
    { return m_step; }

    const std::vector<node_ptr_t> &body() const
    { return m_body; }

    void to_json(nlohmann::json &json) const override
    {
        base_node::to_json(json);
        json["variable"] = m_var;
        json["start"] = m_start;
        json["end"] = m_end;
        if (m_step) json["step"] = m_step;
        json["body"] = m_body;
    }

    fornumeric_node(node_ptr_t var, node_ptr_t start, node_ptr_t end, node_ptr_t step, std::vector<node_ptr_t> body)
        : base_node{class_type}, m_var{std::move(var)}, m_start{std::move(start)}, m_end{std::move(end)},
          m_step{std::move(step)}, m_body{std::move(body)}
    {
    }

private:
    node_ptr_t m_var;
    node_ptr_t m_start;
    node_ptr_t m_end;
    node_ptr_t m_step;
    std::vector<node_ptr_t> m_body;
};

class forgeneric_node : public base_node {
public:
    static constexpr node_type class_type = stmt_for_generic;

    const std::vector<node_ptr_t> &variables() const
    { return m_vars; }

    const std::vector<node_ptr_t> &iters() const
    { return m_iters; }

    const std::vector<node_ptr_t> &body() const
    { return m_body; }

    void to_json(nlohmann::json &json) const override
    {
        base_node::to_json(json);
        json["variables"] = m_vars;
        json["iterators"] = m_iters;
        json["body"] = m_body;
    }

    forgeneric_node(std::vector<node_ptr_t> vars, std::vector<node_ptr_t> iters, std::vector<node_ptr_t> body)
        : base_node{class_type}, m_vars{std::move(vars)}, m_iters{std::move(iters)}, m_body{std::move(body)}
    {
    }

private:
    std::vector<node_ptr_t> m_vars;
    std::vector<node_ptr_t> m_iters;
    std::vector<node_ptr_t> m_body;
};

} // namespace __detail

// exports
using fornumeric_t = __detail::fornumeric_node;
using forgeneric_t = __detail::forgeneric_node;

} // namespace ast

CPPLUA_NS_END

