/* Copyright (c) 2021, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#pragma once

#include "cpplua/core/lang/ast/base.h"
#include <vector>

CPPLUA_NS_BEGIN

namespace ast {
namespace __detail {

class table_construct_expr_node : public base_node {
public:
    static constexpr node_type class_type = expr_table_constructor;

    const std::vector<node_ptr_t> &fields() const
    {
        return m_fields;
    }

    void to_json(nlohmann::json &json) const override
    {
        base_node::to_json(json);
        json["fields"] = m_fields;
    }

    table_construct_expr_node(std::vector<node_ptr_t> fields)
        : base_node{class_type}, m_fields{std::move(fields)}
    {
    }

    ~table_construct_expr_node() = default;

private:
    std::vector<node_ptr_t> m_fields;
};

class tablekey_node : public base_node {
public:
    static constexpr node_type class_type = stmt_table_key;

    const node_ptr_t &key() const
    {
        return m_key;
    }

    const node_ptr_t &value() const
    {
        return m_value;
    }

    void to_json(nlohmann::json &json) const override
    {
        base_node::to_json(json);
        json["key"] = m_key;
        json["value"] = m_value;
    }

    tablekey_node(node_ptr_t key, node_ptr_t value)
        : base_node{class_type}, m_key{std::move(key)}, m_value{std::move(value)}
    {
    }

    ~tablekey_node() = default;

private:
    node_ptr_t m_key;
    node_ptr_t m_value;
};

class tablekey_string_node : public base_node {
public:
    static constexpr node_type class_type = stmt_table_key_string;

    const node_ptr_t &key() const
    {
        return m_key;
    }

    const node_ptr_t &value() const
    {
        return m_value;
    }

    void to_json(nlohmann::json &json) const override
    {
        base_node::to_json(json);
        json["key"] = m_key;
        json["value"] = m_value;
    }

    tablekey_string_node(node_ptr_t key, node_ptr_t value)
        : base_node{class_type}, m_key{std::move(key)}, m_value{std::move(value)}
    {
    }

    ~tablekey_string_node() = default;

private:
    node_ptr_t m_key;
    node_ptr_t m_value;
};

class tablevalue_node : public base_node {
public:
    static constexpr node_type class_type = stmt_table_value;

    const node_ptr_t &value() const
    {
        return m_value;
    }

    void to_json(nlohmann::json &json) const override
    {
        base_node::to_json(json);
        json["value"] = m_value;
    }

    tablevalue_node(node_ptr_t value)
        : base_node{class_type}, m_value{std::move(value)}
    {
    }

    ~tablevalue_node() = default;

private:
    node_ptr_t m_value;
};

} // namespace __detail

// exports
using tablekey_t = __detail::tablekey_node;
using tableval_t = __detail::tablevalue_node;
using tablekey_string_t = __detail::tablekey_string_node;
using table_construct_expr_t = __detail::table_construct_expr_node;

} // namespace ast

CPPLUA_NS_END

