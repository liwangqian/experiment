/* Copyright (c) 2021, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#pragma once


#include "cpplua/core/lang/ast/base.h"
#include <vector>

CPPLUA_NS_BEGIN

namespace ast {
namespace __detail {

class ifstmt_node : public base_node {
public:
    static constexpr node_type class_type = stmt_if;

    void add_clause(const node_ptr_t &node)
    { m_clauses.push_back(node); }

    const std::vector<node_ptr_t>& clauses() const
    {
        return m_clauses;
    }

    void to_json(nlohmann::json &json) const override
    {
        base_node::to_json(json);
        json["clauses"] = m_clauses;
    }

    ifstmt_node()
        : base_node{class_type}
    {
    }

private:
    std::vector<node_ptr_t> m_clauses;
};

class ifclause_base_node : public base_node {
public:
    inline const node_ptr_t &condition() const
    {
        return m_condition;
    }

    inline const std::vector<node_ptr_t> &body() const
    {
        return m_body;
    }

    void to_json(nlohmann::json &json) const override
    {
        base_node::to_json(json);
        json["condition"] = m_condition;
        json["body"] = m_body;
    }

    ifclause_base_node(node_type class_type, node_ptr_t condition, std::vector<node_ptr_t> body)
        : base_node{class_type}, m_condition{std::move(condition)}, m_body{std::move(body)}
    {
    }

private:
    node_ptr_t m_condition;
    std::vector<node_ptr_t> m_body;
};

class ifclause_node : public ifclause_base_node {
public:
    static constexpr node_type class_type = stmt_if_clause;

    ifclause_node(const node_ptr_t &condition, std::vector<node_ptr_t> body)
        : ifclause_base_node{class_type, condition, std::move(body)}
    {
    }
};

class elseifclause_node : public ifclause_base_node {
public:
    static constexpr node_type class_type = stmt_elseif_clause;

    elseifclause_node(node_ptr_t condition, std::vector<node_ptr_t> &body)
        : ifclause_base_node{class_type, std::move(condition), std::move(body)}
    {
    }
};

class elseclause_node : public base_node {
public:
    static constexpr node_type class_type = stmt_else_clause;

    inline const std::vector<node_ptr_t> &body() const
    {
        return m_body;
    }

    void to_json(nlohmann::json &json) const override
    {
        base_node::to_json(json);
        json["body"] = m_body;
    }

    elseclause_node(std::vector<node_ptr_t> body)
        : base_node{class_type}, m_body{std::move(body)}
    {
    }

private:
    std::vector<node_ptr_t> m_body;
};

} // namespace __detail

// exports
using ifstmt_t = __detail::ifstmt_node;
using ifclause_t = __detail::ifclause_node;
using elseifclause_t = __detail::elseifclause_node;
using elseclause_t = __detail::elseclause_node;

} // namespace ast

CPPLUA_NS_END

