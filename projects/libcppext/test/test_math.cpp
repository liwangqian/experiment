/* Copyright (c) 2023-2023, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <catch2/catch.hpp>
#include "libcppext/algorithm/math.h"
#include <algorithm>
#include <array>

#define NBITS(tv) (sizeof(tv) * 8)

template <typename Int>
constexpr auto make_p2s()
{
    std::array<Int, NBITS(Int)> p{};
    Int x = 1u;
    for (auto i = 0u; i < p.size(); ++i) {
        p[i] = (x << i);
    }
    return p;
}

constexpr auto p1 = make_p2s<uint64_t>();
constexpr auto p2 = make_p2s<uint32_t>();
constexpr auto p3 = make_p2s<uint16_t>();
constexpr auto p4 = make_p2s<uint8_t>();

template <typename P>
void test_flp2(const P &p)
{
    typename P::value_type v = 0;
    REQUIRE(libcppext::math::flp2(v+0u) == 0u);
    REQUIRE(libcppext::math::flp2(v+1u) == 1u);
    REQUIRE(libcppext::math::flp2(v+2u) == 2u);
    for (auto i = 3u; i < p.size(); ++i) {
        REQUIRE(libcppext::math::flp2(p[i] + 1u) == p[i]);
    }
}

template <typename P>
void test_clp2(const P &p)
{
    typename P::value_type v = 0;
    REQUIRE(libcppext::math::clp2(v+0u) == 0u);
    REQUIRE(libcppext::math::clp2(v+1u) == 1u);
    REQUIRE(libcppext::math::clp2(v+2u) == 2u);
    for (auto i = 3u; i < p.size(); ++i) {
        REQUIRE(libcppext::math::clp2(p[i] - 1u) == p[i]);
    }
}

TEST_CASE("libcppext.algorithm.math.flp2")
{
    test_flp2(p1);
    test_flp2(p2);
    test_flp2(p3);
    test_flp2(p4);
    static_assert(libcppext::math::flp2(1010u) == 512u);
}

TEST_CASE("libcppext.algorithm.math.clp2")
{
    test_clp2(p1);
    test_clp2(p2);
    test_clp2(p3);
    test_clp2(p4);
    static_assert(libcppext::math::clp2(1010u) == 1024u);
}
