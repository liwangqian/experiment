/* Copyright (c) 2022-2022, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#pragma once

namespace libcppext {

template <typename T>
inline bool result<T>::success() const noexcept
{
    return success_;
}

template <typename T>
inline const T &result<T>::value() const noexcept
{
    return std::get<T>(v_);
}

template <typename T>
inline const libcppext::error &result<T>::error() const noexcept
{
    return std::get<libcppext::error>(v_);
}

template <typename T>
inline result<T>::result(T &&t) noexcept
    : success_{true}, v_{std::move(t)}
{
}

template <typename T>
inline result<T>::result(const T &t)
    : success_{true}, v_{t}
{
}

template <typename T>
inline result<T>::result(libcppext::error &&e) noexcept
    : success_{false}, v_{std::move(e)}
{
}

template <typename T>
inline result<T>::result(const libcppext::error &e) noexcept
    : success_{false}, v_{e}
{
}

template <typename T>
inline result<T>::result(const result<T> &r)
    : success_{r.success_}, v_{r.v_}
{
}

template <typename T>
inline result<T>::result(result<T> &&r) noexcept
    : success_{r.success_}, v_{std::move(r.v_)}
{
}

template <typename T>
inline result<T> &result<T>::operator=(const result<T> &r)
{
    if (&r != this) {
        success_ = r.success_;
        v_ = r.v_;
    }
    return *this;
}

template <typename T>
inline result<T> &result<T>::operator=(result<T> &&r) noexcept
{
    if (&r != this) {
        std::swap(success_, r.success_);
        std::swap(v_, r.v_);
    }
    return *this;
}

///////////////////////////////////////////////////////////

template <typename T>
inline bool result<T*>::success() const noexcept
{
    return success_;
}

template <typename T>
inline const typename result<T*>::pointer result<T*>::value() const noexcept
{
    return std::get<pointer>(v_);
}

template <typename T>
inline typename result<T*>::pointer result<T*>::defer() noexcept
{
    pointer ret = std::get<pointer>(v_);
    v_ = (pointer)nullptr;
    return ret;
}

template <typename T>
inline const libcppext::error &result<T*>::error() const noexcept
{
    return std::get<libcppext::error>(v_);
}

template <typename T>
inline result<T*>::result(libcppext::error &&e) noexcept
    : success_{false}, v_{std::move(e)}
{}

template <typename T>
inline result<T*>::result(const libcppext::error &e) noexcept
    : success_{false}, v_{e}
{}

template <typename T>
inline result<T*>::result(pointer p) noexcept
    : success_{true}, v_{p}
{}

template <typename T>
inline result<T*>::result(const result &r)
    : success_{false}, v_{r.v_}
{}

template <typename T>
inline result<T*>::result(result &&r) noexcept
    : success_{r.success_}, v_{std::move(r.v_)}
{}

template <typename T>
inline result<T*> &result<T*>::operator=(const result &r)
{
    if (&r != this) {
        success_ = r.success_;
        v_ = r.v_;
    }
    return *this;
}

template <typename T>
inline result<T*> &result<T*>::operator=(result &&r) noexcept
{
    if (&r != this) {
        std::swap(success_, r.success_);
        std::swap(v_, r.v_);
    }
    return *this;
}

} // namespace libcppext
