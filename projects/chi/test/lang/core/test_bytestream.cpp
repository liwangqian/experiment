/* Copyright (c) 2023-2023, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <catch2/catch.hpp>
#include <fstream>
#include "chi/lang/core/bytestream.h"

using namespace chi::lang;

void file_write(const char *file, const char *content)
{
    std::fstream fs;
    fs.open(file, std::ios::out | std::ios::trunc);
    fs << content;
}

TEST_CASE("test.chi.lang.core.bytestream.from_file")
{
    const char *file = "./bytestream_code.chi";
    file_write(file, "let x = 1;");
    core::bytestream bytestream;
    bytestream.from_file(file);
    REQUIRE(bytestream.valid());
    REQUIRE(bytestream.offset() == 0);
    REQUIRE(bytestream.length() == 10);
    REQUIRE(bytestream.available_bytes() == 10);
}

TEST_CASE("test.chi.lang.core.bytestream.from_string")
{
    core::bytestream bytestream;
    bytestream.from_string("let x = 1;");
    REQUIRE(bytestream.valid());
    REQUIRE(bytestream.offset() == 0);
    REQUIRE(bytestream.length() == 10);
    REQUIRE(bytestream.available_bytes() == 10);
}

TEST_CASE("test.chi.lang.core.bytestream.substr")
{
    core::bytestream bytestream;
    bytestream.from_string("let x = 1;");
    REQUIRE(bytestream.substr(0, 3) == "let");
    REQUIRE(bytestream.substr(10, 1) == "");
    REQUIRE(bytestream.substr(10, 2) == "");
    REQUIRE_THROWS(bytestream.substr(11, 1) == "");
    REQUIRE(bytestream.offset() == 0);
}

TEST_CASE("test.chi.lang.core.bytestream.operator>>")
{
    core::bytestream bytestream;
    bytestream.from_string("let x = 1;");
    REQUIRE(bytestream.offset() == 0);
    char c;
    bytestream >> c;
    REQUIRE(c == 'l');
    REQUIRE(bytestream.offset() == 1);
    while (bytestream.valid()) {
        bytestream >> c;
    }
    REQUIRE(bytestream.offset() == bytestream.length());
    REQUIRE(bytestream.available_bytes() == 0);
}

TEST_CASE("test.chi.lang.core.bytestream.consume")
{
    core::bytestream bytestream;
    bytestream.from_string("let x = 1;");
    REQUIRE(bytestream.offset() == 0);
    bytestream.consume(1);
    REQUIRE(bytestream.offset() == 1);
    bytestream.consume(9);
    REQUIRE(bytestream.offset() == 10);
    REQUIRE(bytestream.available_bytes() == 0);
    bytestream.consume(2);
    REQUIRE(bytestream.offset() == 10);
    REQUIRE(bytestream.available_bytes() == 0);
}

TEST_CASE("test.chi.lang.core.bytestream.consume_if")
{
    core::bytestream bytestream;
    bytestream.from_string("let x = 1;");
    REQUIRE(bytestream.offset() == 0);
    bytestream.consume_if([](char c) {
        return c == 'l' or c == 'e' or c == 't';
    });

    REQUIRE(bytestream.offset() == 3);
    REQUIRE(bytestream.available_bytes()
        == bytestream.length() - bytestream.offset());
}

TEST_CASE("test.chi.lang.core.bytestream.consume_until")
{
    core::bytestream bytestream;
    bytestream.from_string("let x = 1;");
    REQUIRE(bytestream.offset() == 0);
    bytestream.consume_until([](char c) {
        return c == ' ';
    });

    REQUIRE(bytestream.offset() == 3);
    REQUIRE(bytestream.available_bytes()
        == bytestream.length() - bytestream.offset());
}
