/* Copyright (c) 2023-2023, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <catch2/catch.hpp>
#include "chi/lang/core/tokenstream.h"

using namespace chi::lang;

class test_tokenstream : public core::tokenstream {
public:
    test_tokenstream()
        : tokenstream(4u)
    {}

protected:
    std::size_t fetch(std::size_t n) override
    {
        std::size_t m = std::min(n, index_ + depth_ - num_tokens_);
        for (auto i = 0u; i < m; i++) {
            auto str = std::to_string(index_);
            push(factory_->create(0, str.c_str(), str.length(), {}));
        }
        return m;
    }
};

TEST_CASE("test.chi.lang.core.tokenstream")
{
    test_tokenstream stream;
    REQUIRE(stream.empty());
    REQUIRE(!stream.full());
    REQUIRE(stream.next_token() != nullptr);
    REQUIRE(stream.index() == 0);
    stream.consume();
    REQUIRE(stream.index() == 1);
    REQUIRE(stream.get(0) != nullptr);
    REQUIRE(stream.get(-1) != nullptr);
    REQUIRE(stream.get(0)->text() == "1");  // LA(0)
    REQUIRE(stream.get(-1)->text() == "0"); // LB(1)

    REQUIRE(stream.get(3) != nullptr);
    REQUIRE(stream.get(4) == nullptr);
    REQUIRE(stream.index() == 1);
    REQUIRE(stream.get(0)->to_string() == "token<type=0 text=\"1\">");

    REQUIRE(!stream.empty());
    REQUIRE(stream.full());
}
