/* Copyright (c) 2023-2023, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "chi/lang/core/bytestream.h"
#include <exception>
#include <fstream>
#include <fmt/format.h>

namespace chi::lang::core {

std::size_t file_size(std::fstream &is) noexcept
{
    if (not is) return 0;

    auto cur = is.tellg();
    is.seekg(0, std::ios::end);
    auto len = is.tellg();
    is.seekg(cur, std::ios::beg);
    return len;
}

void bytestream::from_file(const char *file)
{
    reset();

    std::fstream is{file, std::ios::in};
    auto len = file_size(is);
    if (len == -1) return;

    str_.reserve(len);

    char c;
    while ((c = is.get()) != EOF) {
        str_.push_back(c);
    }
}

void bytestream::from_file(std::string_view file)
{
    from_file(file.data());
}

void bytestream::from_file(const std::string &file)
{
    from_file(file.c_str());
}

void bytestream::range_check(std::size_t pos, const char *desc) const
{
    if (pos > str_.length()) {
        throw std::out_of_range(
            fmt::format("{}: pos({}) > size({})",
                desc, pos, str_.length()));
    }
}

} /* chi::lang::core */
