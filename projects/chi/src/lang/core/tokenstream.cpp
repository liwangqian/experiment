/* Copyright (c) 2023-2023, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "chi/lang/core/tokenstream.h"
#include <cmath>

namespace chi::lang::core {

inline std::size_t lound_to_2power(std::size_t n)
{
    n = n - 1;
    n = n | (n >>  1);
    n = n | (n >>  2);
    n = n | (n >>  4);
    n = n | (n >>  8);
    n = n | (n >> 16);
    return n + 1;
}

inline std::size_t rq_index(std::size_t index, std::size_t size)
{
    return index & (size - 1);
}

tokenstream::~tokenstream()
{
    if (token_rq_ != nullptr) {
        clear();
        delete token_rq_;
        token_rq_ = nullptr;
    }
}

tokenstream::tokenstream(std::size_t depth)
    : rq_size_{lound_to_2power(depth) << 1}, depth_{depth}
{
    factory_ = default_tokenfactory();
    token_rq_ = new const token* [rq_size_];
    if (token_rq_ == nullptr) return;

    for (auto i = 0; i < rq_size_; ++i) {
        token_rq_[i] = nullptr;
    }
}

void tokenstream::clear() noexcept
{
    num_tokens_ = 0;
    index_ = 0;

    if (token_rq_ == nullptr) return;

    for (auto i = 0; i < rq_size_; ++i) {
        if (token_rq_[i] != nullptr) {
            factory_->destroy(token_rq_[i]);
            token_rq_[i] = nullptr;
        }
    }
}

void tokenstream::consume() noexcept
{
    if (tokens_available() > 0) {
        ++index_;
    }
}

bool tokenstream::push(const token *tok) noexcept
{
    if (tok == nullptr) return false;
    if (full()) return false;

    auto insert_pos = rq_index(num_tokens_++, rq_size_);
    auto tmp = token_rq_[insert_pos];
    token_rq_[insert_pos] = tok;
    factory_->destroy(tmp);
    return true;
}

const token *tokenstream::next_token() noexcept
{
    return get(0);
}

const token *tokenstream::get(int k) noexcept
{
    /* valid range: [-depth, 0, depth) */
    /* look forward */
    if ((k > 0) && (k >= depth_)) {
        return nullptr;
    }

    /* look backward */
    if ((k < 0) && ((-k) > depth_)) {
        return nullptr;
    }

    auto index_k = index_ + k;
    auto nfetch  = index_k >= num_tokens_ ? (index_k - num_tokens_ + 1) : 0;

    if (fetch(nfetch) < nfetch) {
        return nullptr;
    }

    return token_rq_[rq_index(index_k, rq_size_)];
}

} // namespace chi::lang::core
