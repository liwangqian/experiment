/* Copyright (c) 2023-2023, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef TEXT_H_INCLUDED
#define TEXT_H_INCLUDED

#include <string>
#include <string_view>
#include <fmt/format.h>
#include "chi/lang/utils/comparable.h"

namespace chi::lang::utils {

/// @brief ReadOnly string representation
class text
    : public comparable<text, text>
    , public comparable<text, const char *>
    , public comparable<text, std::string &>
    , public comparable<text, std::string_view &> {
public:
    ~text();
    text();

    text(const char *str, bool copy = true);
    text(const char *str, std::size_t len, bool copy = true);
    text(const std::string &str);
    text(const std::string_view &str);

    text(const text &other);
    text(text &&other) noexcept;
    text &operator=(const text &other);
    text &operator=(text &&other) noexcept;

    int compara(const text &) const noexcept;
    int compara(const char *str) const noexcept;
    int compara(const std::string &str) const noexcept;
    int compara(const std::string_view &str) const noexcept;

    const char *data() const noexcept;

    auto empty() const noexcept
    {
        return size_ == 0;
    }

    auto length() const noexcept
    {
        return size_;
    }

    auto size() const noexcept
    {
        return size_;
    }

    void reset() noexcept;
    
    void swap(text &) noexcept;

    bool shared() const noexcept;

private:
    union {
        const char  ss_[sizeof(char*)]{0}; /* short string */
        const char *ls_;                   /* long string */
    };

    std::size_t size_{0};          /* string size */
    bool owns_ls_{false};          /* owner of long string */
};

inline std::string to_string(const text &text) noexcept
{
    return {text.data(), text.length()};
}

template <typename Out>
inline Out &operator<<(Out & out, const text &text)
{
    out << text.data();
    return out;
}

} // namespace chi::lang::utils

namespace std {

template <>
struct hash<chi::lang::utils::text> {
    using text = chi::lang::utils::text;
    size_t operator()(const text& s) const noexcept
    {
        return std::hash<std::string_view>()({s.data(), s.size()});
    }
};

} // namespace std

template <>
struct fmt::formatter<chi::lang::utils::text> {

    constexpr auto parse(format_parse_context &ctx)
        -> decltype(ctx.begin())
    {
        return ctx.begin();
    }

    template <typename FormatContext>
    constexpr auto format(const chi::lang::utils::text &text,
        const FormatContext &ctx) const noexcept
        -> decltype(ctx.out)
    {
        fmt::format_to(ctx.out(), "{}", text.data());
        return ctx.out();
    }

};

#endif /* TEXT_H_INCLUDED */
