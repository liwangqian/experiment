/* Copyright (c) 2023-2023, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef BYTESTREAM_H_INCLUDED
#define BYTESTREAM_H_INCLUDED

#include <string>
#include <string_view>
#include "chi/lang/utils/text.h"

namespace chi::lang::core {

class bytestream {
public:
    ~bytestream() = default;
    bytestream() = default;

    void from_file(const char *file);
    void from_file(std::string_view file);
    void from_file(const std::string &file);

    void from_string(const char *code)
    {
        reset();
        str_.assign(code);
    }

    void from_string(std::string_view code)
    {
        reset();
        str_ = code;
    }

    void from_string(const std::string &code)
    {
        reset();
        str_ = code;
    }

    bool valid() const noexcept
    {
        return offset_ < length();
    }

    std::size_t length() const noexcept
    {
        return str_.length();
    }

    std::size_t offset() const noexcept
    {
        return offset_;
    }

    std::size_t available_bytes() const noexcept
    {
        return valid() ? length() - offset() : 0;
    }

    utils::text substr(std::size_t pos, std::size_t n) const
    {
        range_check(pos, "bytestream:substr");
        return {str_.c_str() + pos, n};
    }

    bytestream &operator>>(char &c) noexcept
    {
        if (valid()) {
            c = str_[offset_++];
        } else {
            c = EOF;
        }
        return *this;
    }

    char get(std::size_t k) noexcept
    {
        return str_[offset_ + k];
    }

    void consume(std::size_t len = 1)
    {
        std::size_t tmp = offset_ + len;
        offset_ = std::min(tmp, length());
    }

    template <typename Predicate>
    void consume_if(Predicate pred) noexcept
    {
        while (offset_ < length()) {
            auto c = str_[offset_];
            if (!pred(c)) break;
            ++offset_;
        }
    }

    template <typename Predicate>
    void consume_until(Predicate pred)
    {
        while (offset_ < length()) {
            auto c = str_[offset_];
            if (pred(c)) break;
            ++offset_;
        }
    }

    void reset() noexcept
    {
        str_.clear();
        offset_ = 0;
    }

private:
    void range_check(std::size_t pos, const char *desc) const;

    std::string str_;
    std::size_t offset_{0};
};


} // namespace chi::lang::core

#endif /* BYTESTREAM_H_INCLUDED */
