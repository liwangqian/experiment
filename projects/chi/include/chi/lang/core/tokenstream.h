/* Copyright (c) 2023-2023, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef TOKENSTREAM_H_INCLUDED
#define TOKENSTREAM_H_INCLUDED

#include "chi/lang/core/token.h"
#include "chi/lang/core/tokenfactory.h"

namespace chi::lang::core {

class tokenstream {
public:
    ~tokenstream();
    tokenstream(const tokenstream &) = delete;
    tokenstream &operator=(const tokenstream &) = delete;

    explicit tokenstream(std::size_t depth);

    void clear() noexcept;

    bool empty() const noexcept
    {
        return index_ == num_tokens_;
    }

    bool full() const noexcept
    {
        return num_tokens_ - index_ >= depth_;
    }

    void consume() noexcept;

    bool push(const token *tok) noexcept;

    const token *next_token() noexcept;

    const token *get(int k = 0) noexcept;;

    std::size_t index() const noexcept
    {
        return index_;
    }

    std::size_t tokens_available() const noexcept
    {
        return num_tokens_ - index_;
    }

    void set_tokenfactory(tokenfactory *factory) noexcept
    {
        factory_ = factory;
    }

    tokenfactory *get_tokenfactory() const noexcept
    {
        return factory_;
    }

protected:
    virtual std::size_t fetch(std::size_t n) = 0;

    tokenfactory *factory_{nullptr};
    const token **token_rq_{nullptr};
    const std::size_t rq_size_{0};
    const std::size_t depth_{0};
    std::size_t num_tokens_{0};
    std::size_t index_{0};
};

} // namespace chi::lang::core

#endif /* TOKENSTREAM_H_INCLUDED */
