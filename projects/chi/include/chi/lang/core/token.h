/* Copyright (c) 2023-2023, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef TOKEN_H_INCLUDED
#define TOKEN_H_INCLUDED

#include <fmt/format.h>
#include "chi/lang/utils/text.h"
#include "chi/lang/core/sourceinfo.h"

namespace chi::lang::core {

class token {
public:
    static constexpr std::size_t TYPE_INVLAID = 0;
    static constexpr std::size_t TYPE_EOF = -1;

    token() = default;

    token(std::size_t type, utils::text text, sourceinfo source)
        : type_{type}, text_{std::move(text)}, source_{source}
    {
    }

    token(const token &tok)
        : type_{tok.type_}
        , text_{tok.text_}
        , source_{tok.source_}
    {
    }

    token(token &&tok) noexcept
    {
        swap(tok);
    }

    token &operator=(const token &tok)
    {
        if (this != &tok) {
            type_ = tok.type_;
            text_ = tok.text_;
            source_ = tok.source_;
        }
        return *this;
    }

    token &operator=(token &&tok) noexcept
    {
        swap(tok);
        return *this;
    }

    void swap(token &tok) noexcept
    {
        if (this != &tok) {
            std::swap(type_, tok.type_);
            std::swap(text_, tok.text_);
            std::swap(source_, tok.source_);
        }
    }

    std::size_t type() const noexcept
    {
        return type_;
    }

    const auto &text() const noexcept
    {
        return text_;
    }

    const auto &source() const noexcept
    {
        return source_;
    }

private:
    std::size_t type_{TYPE_INVLAID};
    utils::text text_;
    sourceinfo source_;
};

std::string to_string(const token &tok) noexcept
{
    return fmt::format("token<type={} text=\"{}\">", tok.type(), tok.text());
}

} // namespace chi::lang::core

template <>
struct fmt::formatter<chi::lang::core::token> {

    template <typename FormatContext>
    auto format(const chi::lang::core::token &token,
        const FormatContext &ctx) const noexcept
    {
        fmt::format("{}", chi::lang::core::to_string(token));
        return ctx.out();
    }

};

#endif /* TOKEN_H_INCLUDED */
