/* Copyright (c) 2022-2022, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "libflexe/infra/error.h"

namespace libflexe::infra {

error::error(error_code code, const char *what)
    : code_{code}, mesg_{what}
{
}

error::error(error_code code, const std::string &what)
    : code_{code}, mesg_{what}
{
}

error::error(error_code code, std::string_view what)
    : code_{code}, mesg_{what}
{
}

error::error(const error &e)
    : code_{e.code_}, mesg_{e.mesg_}
{
}

error::error(error &&e) noexcept
    : code_{e.code_}, mesg_{std::move(e.mesg_)}
{
    e.code_ = error_code::OK;
}

error &error::operator=(const error &e)
{
    if (&e != this) {
        code_ = e.code_;
        mesg_ = e.mesg_;
    }
    return *this;
}

error &error::operator=(error &&e) noexcept
{
    if (&e != this) {
        swap(e);
    }
    return *this;
}

void error::swap(error &e) noexcept
{
    std::swap(code_, e.code_);
    std::swap(mesg_, e.mesg_);
}

error_code error::code() const noexcept
{
    return code_;
}

const char *error::what() const noexcept
{
    return mesg_.c_str();
}

} // namespace libflexe::infra
