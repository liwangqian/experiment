/* Copyright (c) 2022-2022, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "libflexe/infra/event/topic.h"
#include "libflexe/infra/event/ievent.h"
#include "libflexe/infra/event/listener.h"
#include "libflexe/infra/error.h"
#include <algorithm>

namespace libflexe::infra::event {

static inline bool contains_unlock(
    const stl::forward_list<stl::shared_ptr<listener>> &ls,
    const stl::string &name)
{
    return std::find_if(std::begin(ls), std::end(ls),
        [&](const auto &l) {
            return l->name() == name;
        }) != std::end(ls);
}

topic::~topic()
{
    if (thd_ != nullptr) {
        stop_ = true;
        cv_.notify_all();
        if (thd_->joinable()) {
            thd_->join();
        }
    }
}

topic::topic(stl::string name) noexcept
    : name_{std::move(name)}
{
}

int topic::setup() noexcept
{
    thd_ = new_object<std::thread>(&topic::async_loop, this);
    return thd_ != nullptr ? OK : E_OUT_MEMORY;
}

const stl::string &topic::name() const noexcept
{
    return name_;
}

int topic::sub(const stl::string &name, listener_ptr l)
{
    std::lock_guard<std::mutex> lock{mtx_};
    if (contains_unlock(listeners_, name)) {
        return E_EXISTED;
    }
    listeners_.push_front(std::move(l));
    return OK;
}

void topic::unsub(const stl::string &name)
{
    std::lock_guard<std::mutex> lock{mtx_};
    listeners_.remove_if([&](const auto &li) {
        return li->name() == name;
    });
}

int topic::pub(stl::shared_ptr<ievent> evt)
{
    mtx_.lock();
    evt->on_start();
    for (auto &li : listeners_) {
        evt->on_result(li->on_event(*evt.get()), li->name());
    }
    mtx_.unlock();
    evt->on_finished();
    return 0;
}

int topic::pub_async(stl::shared_ptr<ievent> evt)
{
    std::unique_lock<std::mutex> lock{queue_mtx_};
    evt_queue_.push(std::move(evt));
    cv_.notify_all();
    return OK;
}

bool topic::contains(const stl::string &name) const
{
    std::lock_guard<std::mutex> lock{mtx_};
    return contains_unlock(listeners_, name);
}

void topic::async_loop()
{
    do {
        std::unique_lock<std::mutex> lock{queue_mtx_};
        cv_.wait(lock, [this]() { return !evt_queue_.empty() || stop_;});
        while (!evt_queue_.empty()) {
            auto evt = evt_queue_.front();
            evt_queue_.pop();
            pub(std::move(evt));
        }
    } while (!stop_);
}

}
