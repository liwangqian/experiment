/* Copyright (c) 2023-2023, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <string.h>
#include "fg/fgcard.h"
#include "fg/fgdrv.h"
#include "fgdrv_priv.h"

FG_STDC_BEGIN

fgdrv *fgdrv_create(fgdrv_if *intf)
{
    fgdrv *self = (fgdrv *)intf->malloc(intf, sizeof(fgdrv), __FILE__, __LINE__);
    (void)memset(self, 0, sizeof(fgdrv));
    self->intf = intf;
    return self;
}

void fgdrv_destroy(fgdrv *self)
{
    fgdrv_if *intf = self->intf;
    for (int i = 0; i < FGDRV_MAX_CARDNUM; ++i) {
        if (self->cards[i] != NULL) {
            fgcard_destroy(self->cards[i]);
        }
    }
    intf->free(intf, self);
    self = NULL;
}

int fgdrv_add_card(fgdrv *self, unsigned int id, fgcard *card)
{
    if (id < FGDRV_MAX_CARDNUM) {
        self->cards[id] = card;
        return 0;
    }
    return -1;
}

int fgdrv_del_card(fgdrv *self, unsigned int id)
{
    if (id < FGDRV_MAX_CARDNUM) {
        self->cards[id] = NULL;
        return 0;
    }
    return -1;
}

fgcard *fgdrv_get_card(fgdrv *self, unsigned int id)
{
    if (id < FGDRV_MAX_CARDNUM) {
        return self->cards[id];
    }
    return NULL;
}

void fgdrv_set_if(fgdrv *self, fgdrv_if *intf)
{
    self->intf = intf;
}

fgdrv_if *fgdrv_get_if(const fgdrv *self)
{
    return self->intf;
}

FG_STDC_END
