#ifndef FGDRV_PRIV_H_INCLUDED
#define FGDRV_PRIV_H_INCLUDED

#include "fg/fgdrv.h"
#include "fg/fgspec.h"

FG_TYPE(fgdrv)
{
    fgdrv_if   *intf;
    fgcard     *cards[FGDRV_MAX_CARDNUM];
};

#endif /* FGDRV_PRIV_H_INCLUDED */
