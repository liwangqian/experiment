/* Copyright (c) 2023-2023, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef FGCHIP_PRIV_H_INCLUDED
#define FGCHIP_PRIV_H_INCLUDED

#include <stdbool.h>
#include "fg/fgchip.h"
#include "fg/fgspec.h"

FG_TYPE(fgchip)
{
    fgchip_if   *intf;                      /* interface */
    fgcard      *card;                      /* parent */
    fgphy       *phys[FGCHIP_MAX_PHYNUM];   /* children */
    fgpipe      *pipes[FGCHIP_MAX_PIPENUM]; /* children */
    unsigned int phynum;
    unsigned int pipenum;
    unsigned int id;
    bool         inited;
};

/// @brief 访问芯片功能
/// @param self     芯片实例指针
/// @param cmd      命令字
/// @param inbuf    输入缓冲区
/// @param insize   输入缓冲区长度
/// @param outbuf   输出缓冲区
/// @param outsize  输出缓冲区长度
/// @return E_OK表示成功，其他表示失败
int fgchip_access(fgchip *self, int cmd,
    void *inbuf, unsigned int insize,
    void *outbuf, unsigned int outsize);

#endif /* FGCHIP_PRIV_H_INCLUDED */
