/* Copyright (c) 2023-2023, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <string.h>
#include "fg/fgcard.h"
#include "fg/fgdrv.h"
#include "fg/fgpipe.h"
#include "fgchip_priv.h"

FG_STDC_BEGIN

void destroy_all_pipes(fgchip *self)
{
    for (int i = 0; i < FGCHIP_MAX_PIPENUM; ++i) {
        if (self->pipes[i] != NULL) {
            fgpipe_destroy(self->pipes[i]);
        }
    }
}

fgchip *fgchip_create(fgcard *card, unsigned int id)
{
    fgdrv *drv = fgcard_get_drv(card);
    fgchip *self = (fgchip*)FGDRV_MALLOC(drv, sizeof(fgchip));
    (void)memset(self, 0, sizeof(fgchip));
    self->id = id;
    self->card = card;
    fgcard_add_chip(card, id, self);
    return self;
}

int fgchip_destroy(fgchip *self)
{
    fgcard *card = fgchip_get_card(self);
    fgdrv *drv = fgcard_get_drv(card);
    fgcard_del_chip(card, self->id);
    destroy_all_pipes(self);
    FGDRV_FREE(drv, self);
    return 0;
}

int fgchip_init(fgchip *self, fgchip_if *intf, const fgchip_init_config *cfg)
{
    if (self->inited) {
        return -1;
    }

    self->intf = intf;
    if (intf->chip_init != NULL &&
        intf->chip_init(self, cfg) == 0) {
        self->inited = true;
        return 0;
    }

    return -2;
}

int fgchip_exit(fgchip *self)
{
    if (!self->inited) {
        return 0;
    }

    if (self->intf->chip_exit != NULL) {
        self->intf->chip_exit(self);
    }

    self->inited = false;
    self->intf = NULL;
    return 0;
}

bool fgchip_is_inited(const fgchip *self)
{
    return self->inited;
}

unsigned int fgchip_get_id(const fgchip *self)
{
    return self->id;
}

fgcard *fgchip_get_card(const fgchip *self)
{
    return self->card;
}

int fgchip_add_pipe(fgchip *self, unsigned int id, fgpipe *pipe)
{
    if (id < FGCHIP_MAX_PIPENUM) {
        self->pipes[id] = pipe;
        self->pipenum++;
        return 0;
    }
    return -1;
}

int fgchip_del_pipe(fgchip *self, unsigned int id)
{
    if (id < FGCHIP_MAX_PIPENUM) {
        self->pipes[id] = NULL;
        self->pipenum--;
        return 0;
    }
    return -1;
}

fgpipe *fgchip_get_pipe(fgchip *self, unsigned int id)
{
    if (id < FGCHIP_MAX_PIPENUM) {
        return self->pipes[id];
    }
    return NULL;
}


FG_STDC_END
