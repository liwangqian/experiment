/* Copyright (c) 2023-2023, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <string.h>
#include "fg/fgchip.h"
#include "fg/fgdrv.h"
#include "fgcard_priv.h"

FG_STDC_BEGIN

fgcard *fgcard_create(fgdrv *drv, unsigned int id)
{
    fgcard *self = (fgcard *)FGDRV_MALLOC(drv, sizeof(fgcard));
    (void)memset(self, 0, sizeof(fgcard));
    self->drv = drv;
    self->id = id;
    fgdrv_add_card(drv, id, self);
    return self;
}

int fgcard_destroy(fgcard *self)
{
    fgdrv_del_card(self->drv, self->id);

    for (unsigned int i = 0; i < FGCARD_MAX_CHIPNUM; ++i) {
        if (self->chip[i] != NULL) {
            (void)fgchip_destroy(self->chip[i]);
        }
    }

    FGDRV_FREE(self->drv, self);
    return 0;
}

void fgcard_set_if(fgcard *self, fgcard_if *intf)
{
    self->intf = intf;
}

fgcard_if *fgcard_get_if(const fgcard *self)
{
    return self->intf;
}

fgdrv *fgcard_get_drv(const fgcard *self)
{
    return self->drv;
}

fgchip *fgcard_get_chip(fgcard *self, unsigned int id)
{
    if (id < FGCARD_MAX_CHIPNUM) {
        return self->chip[id];
    }
    return NULL;
}

int fgcard_add_chip(fgcard *self, unsigned int id, fgchip *chip)
{
    if (id >= FGCARD_MAX_CHIPNUM) {
        return -1;
    }

    if (self->chip[id] != NULL) {
        return -2;
    }

    self->chip[id] = chip;
    self->chipnum++;
    return 0;
}

void fgcard_del_chip(fgcard *self, unsigned int id)
{
    if (id >= FGCARD_MAX_CHIPNUM) {
        return;
    }

    self->chip[id] = NULL;
    self->chipnum--;
}

unsigned int fgcard_get_id(const fgcard *self)
{
    return self->id;
}

FG_STDC_END
