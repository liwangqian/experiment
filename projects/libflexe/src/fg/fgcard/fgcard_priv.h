#ifndef FGCARD_PRIV_H_INCLUDED
#define FGCARD_PRIV_H_INCLUDED

#include <stdbool.h>
#include "fg/fgcard.h"
#include "fg/fgspec.h"

FG_TYPE(fgcard)
{
    fgcard_if   *intf;                      /* interface */
    fgdrv       *drv;                       /* parent */
    fgchip      *chip[FGCARD_MAX_CHIPNUM];  /* children */
    unsigned int chipnum;
    unsigned int id;
    bool         inited;
};

#endif /* FGCARD_PRIV_H_INCLUDED */
