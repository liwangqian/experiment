/* Copyright (c) 2023-2023, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "fg/contract.h"
#include "fg/fgchip.h"
#include "fg/fgcard.h"
#include "fg/fgdrv.h"
#include "fgpipe_priv.h"

#include <string.h>

FG_STDC_BEGIN

fgpipe *fgpipe_create(fgchip *chip, unsigned int id)
{
    fgcard *card = fgchip_get_card(chip);
    FG_EXPECTS(card != NULL);
    fgdrv *drv = fgcard_get_drv(card);
    FG_EXPECTS(drv != NULL);

    fgpipe *self = (fgpipe *)FGDRV_MALLOC(drv, sizeof(fgpipe));
    FG_EXPECTS(self != NULL);

    (void)memset(self, 0, sizeof(fgpipe));

    self->chip = chip;
    self->id = id;

    FG_EXPECTS(fgchip_add_pipe(chip, id, self) == 0);

    FG_ENSURES(self != NULL);
    return self;
}

int fgpipe_destroy(fgpipe *self)
{
    fgcard *card = fgchip_get_card(self->chip);
    fgdrv *drv = fgcard_get_drv(card);
    fgchip_del_pipe(self->chip, self->id);
    FGDRV_FREE(drv, self);
}

FG_STDC_END
