#ifndef FGPIPE_PRIV_H_INCLUDED
#define FGPIPE_PRIV_H_INCLUDED

#include "fg/fgpipe.h"

FG_TYPE(fgpipe)
{
    fgpipe_if   *intf;
    fgchip      *chip;
    fgport      *ports;
    unsigned int id;
};

#endif /* FGPIPE_PRIV_H_INCLUDED */
