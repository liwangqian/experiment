/* Copyright (c) 2022-2022, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <catch2/catch.hpp>
#include "libflexe/dto/mac/mtu.h"

using namespace libflexe::dto::mac;

TEST_CASE("test.dto.mtu.valid")
{
    for (auto v = mtu::min(); v <= mtu::max(); ++v) {
        auto m = new mtu{v};
        REQUIRE(m != nullptr);
        REQUIRE(m->valid() == true);
        REQUIRE(m->value() == v);
        delete m;
    }
}

TEST_CASE("test.dto.mtu.invalid")
{
    auto v1 = new mtu{mtu::min() - 1};
    REQUIRE(v1 != nullptr);
    REQUIRE(v1->valid() == false);
    delete v1;

    auto v2 = new mtu{mtu::max() + 1};
    REQUIRE(v2 != nullptr);
    REQUIRE(v2->valid() == false);
    delete v2;
}

TEST_CASE("test.dto.mtu.value")
{
    auto v1 = new mtu{128};
    REQUIRE(v1 != nullptr);
    REQUIRE(v1->value() == 128);
    REQUIRE(v1->cref() == 128);
    delete v1;
}
