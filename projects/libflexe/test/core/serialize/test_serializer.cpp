/* Copyright (c) 2022-2022, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <catch2/catch.hpp>
#include "libflexe/core/serialize/serializer.h"

namespace {

using namespace libflexe;
using namespace libflexe::core;

class test_serializer : public serializer {
public:
    void rewind() noexcept { ri = wi = 0; }

protected:
    infra::result<void> read_byte(int8_t *x) override
    {
        *x = buff[ri++];
        return {};
    }

    infra::result<void> write_byte(int8_t x) override
    {
        buff[wi++] = x;
        return {};
    }

    infra::result<void> read_byte_array(int8_t *x, size_t l) override
    {
        assert(l+ri <= 12);
        (void)memcpy(x, buff + ri, l);
        return {};
    }

    infra::result<void> write_byte_array(const int8_t *x, size_t l) override
    {
        assert(l+wi <= 12);
        (void)memcpy(buff, x, l);
        return {};
    }

private:
    int8_t buff[12]{0};
    int8_t ri{0};
    int8_t wi{0};
};

}

TEST_CASE("test.core.serialize.write_and_read_i8")
{
    test_serializer ser{};

    int8_t x = 0x12, y = 0;
    ser.write(x);
    ser.read(&y);
}
