/* Copyright (c) 2022-2022, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <catch2/catch.hpp>
#include "libflexe/infra/allocator.h"
#include "libflexe/infra/memory.h"
#include "libflexe/infra/stl/unordered_map.h"

using namespace libflexe;

TEST_CASE("test.infra.allocator.scalar")
{
    auto x = infra::new_object<uint32_t>(1u);
    REQUIRE(x != nullptr);
    REQUIRE(*x == 1u);

    infra::delete_object(x);
}

TEST_CASE("test.infra.allocator.array")
{
    auto x = infra::new_object_n<uint32_t>(100, 100u);
    REQUIRE(x != nullptr);
    REQUIRE(x[0] == 100u);
    REQUIRE(x[99] == 100u);

    infra::delete_object_n(x, 100);
}

TEST_CASE("test.infra.allocator.class")
{
    struct object {
        ~object() { x = 0; }
        object(int &x) : x{x} {}
        void update(int d) { x = d; }
    private:
        int &x;
    };


    int x = -1;
    auto obj = infra::new_object<object>(x);
    REQUIRE(obj != nullptr);
    obj->update(100);
    REQUIRE(x == 100);
    infra::delete_object(obj);
    REQUIRE(x == 0);
}

TEST_CASE("test.infra.allocator.enum")
{
    enum test {
        A, B, C, D,
    };

    auto em = infra::new_object<test>(test::D);
    REQUIRE(em != nullptr);
    REQUIRE(*em == test::D);
    infra::delete_object(em);
}

TEST_CASE("test.infra.allocator.stl.vector")
{
    using vector = std::vector<int, libflexe::infra::allocator<int>>;
    auto v = vector{1,2,3,4,5};
    REQUIRE(v.size() == 5);
    REQUIRE(v[0] == 1);
    REQUIRE(v[4] == 5);
}

TEST_CASE("test.infra.allocator.stl.unordered_map")
{
    using map = libflexe::infra::stl::unordered_map<
        int,
        const char*>;

    map m;
    m = {
        {0, "hello"},
        {1, "world"},
    };
    REQUIRE(m.size() == 2);
    REQUIRE(m[0] == "hello");
    REQUIRE(m[1] == "world");
}

TEST_CASE("test.infra.allocator.stl.map")
{
    using string = std::basic_string<
        char,
        std::char_traits<char>,
        libflexe::infra::allocator<char>>;
    using map = std::map<
        int,
        string,
        std::less<int>,
        libflexe::infra::allocator<std::pair<int, string>>>;
    
    map m;
    m = {
        {0, "hello"},
        {1, "world"},
    };
    REQUIRE(m.size() == 2);
    REQUIRE(m[0] == "hello");
    REQUIRE(m[1] == "world");
}
