/* Copyright (c) 2022-2022, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <catch2/catch.hpp>
#include <memory>

template <typename T>
class udf_allocator {
public:
    using value_type = T;
    using size_type = size_t;
    using difference_type = ptrdiff_t;
    using pointer = T*;
    using const_pointer = const T*;
    using reference = T&;
    using const_reference = const T&;

    template <typename Y>
    struct rebind { using other = udf_allocator<Y>; };

    ~udf_allocator() = default;
    udf_allocator() = default;
    udf_allocator(const udf_allocator &) = default;

    template <typename Y>
    udf_allocator(const udf_allocator<Y> &) noexcept {}

    pointer address(reference x) const noexcept
    {
        return std::addressof(x);
    }

    const_pointer address(const_reference x) const noexcept
    {
        return std::addressof(x);
    }

    size_type max_size() const noexcept
    {
        return size_type(-1) / sizeof(T);
    }

    pointer allocate(size_type n, const void* = nullptr) noexcept
    {
        if (n > max_size()) {
            return nullptr;
        }
        return static_cast<pointer>(::operator new(n * sizeof(value_type), std::nothrow));
    }

    void deallocate(pointer ptr, size_type n) const noexcept
    {
        ::operator delete(ptr);
    }

    template <typename X, typename ...Args>
    void construct(X *ptr, Args&& ...args)
    {
        ::new(ptr) X(std::forward<Args>(args)...);
    }

    template <typename X>
    void destroy(X *ptr)
    {
        ptr->~X();
    }
};

namespace libflexe::infra {

template <typename T, bool F>
struct allocator_selector;

template <typename T>
struct allocator_selector<T, true> {
    using type = udf_allocator<T>;
};
}

#define FLEXE_USE_DEFAULT_ALLOCATOR true

#include "libflexe/infra/allocator.h"
#include "libflexe/infra/stl/string.h"
#include "libflexe/infra/stl/unordered_map.h"

class object;

TEST_CASE("test.infra.allocator.udf")
{
    static_assert(std::is_same_v<udf_allocator<int>, libflexe::infra::allocator<int>>);
    std::hash<libflexe::infra::stl::string> hs;
    libflexe::infra::stl::unordered_map<libflexe::infra::stl::string, object*> x;
}
