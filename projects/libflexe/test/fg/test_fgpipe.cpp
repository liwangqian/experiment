/* Copyright (c) 2023-2023, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <catch2/catch.hpp>
#include "fg/fgdrv.h"
#include "fg/fgcard.h"
#include "fg/fgchip.h"
#include "fg/fgpipe.h"

void *test_malloc(fgdrv_if *self, unsigned int nbytes,
    const char *file, unsigned int line)
{
    return malloc(nbytes);
}

void test_free(fgdrv_if *self, void *ptr)
{
    free(ptr);
}

fgdrv_if drvif = {
    .malloc = test_malloc,
    .free   = test_free,
};

TEST_CASE("test.fg.fgpipe")
{
    fgdrv *drv = fgdrv_create(&drvif);
    REQUIRE(drv != nullptr);
    fgcard *card = fgcard_create(drv, 0);
    REQUIRE(card != nullptr);
    fgchip *chip = fgchip_create(card, 0);
    REQUIRE(chip != nullptr);
    fgpipe *pipe = fgpipe_create(chip, 0);
    REQUIRE(pipe != nullptr);

    fgdrv_destroy(drv);
}
