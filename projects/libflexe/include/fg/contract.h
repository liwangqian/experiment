#ifndef CONTRACT_H_INCLUDED
#define CONTRACT_H_INCLUDED

#include <assert.h>

#define FG_EXPECTS(expr) assert(expr)
#define FG_ENSURES(expr) assert(expr)

#endif /* CONTRACT_H_INCLUDED */
