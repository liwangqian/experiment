#ifndef FGCHIP_H_INCLUDED
#define FGCHIP_H_INCLUDED

#include "typedecl.h"

FG_TYPE(fgchip_init_config)
{
};

FG_TYPE(fgchip_if)
{
    int (*chip_init)(fgchip *self, const fgchip_init_config *config);
    void(*chip_exit)(fgchip *self);
};

FG_STDC_BEGIN

fgchip *fgchip_create(fgcard *card, unsigned int id);
int     fgchip_init(fgchip *self, fgchip_if *intf, const fgchip_init_config *);
int     fgchip_exit(fgchip *self);
int     fgchip_destroy(fgchip *self);

bool    fgchip_is_inited(const fgchip *self);

unsigned int fgchip_get_id(const fgchip *self);
fgcard *fgchip_get_card(const fgchip *self);

int     fgchip_add_pipe(fgchip *self, unsigned int id, fgpipe *pipe);
int     fgchip_del_pipe(fgchip *self, unsigned int id);
fgpipe *fgchip_get_pipe(fgchip *self, unsigned int id);

FG_STDC_END

#endif /* FGCHIP_H_INCLUDED */
