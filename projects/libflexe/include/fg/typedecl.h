#ifndef TYPEDECL_H_INCLUDED
#define TYPEDECL_H_INCLUDED

#include <stdint.h>
#include <stdbool.h>

#define FG_TYPE_DECL(name) \
    typedef struct name name

FG_TYPE_DECL(fgdrv);
FG_TYPE_DECL(fgcard);
FG_TYPE_DECL(fgchip);
FG_TYPE_DECL(fgphy);
FG_TYPE_DECL(fgpipe);
FG_TYPE_DECL(fgport);
FG_TYPE_DECL(fgportdev);
FG_TYPE_DECL(fgpipedev);

#define FG_TYPE(name)   \
    FG_TYPE_DECL(name); \
    struct name

#define FG_INTERFACE(name) \
    FG_TYPE(name)

#ifdef __cplusplus
    #define FG_STDC_BEGIN extern "C" {
    #define FG_STDC_END   }
#else
    #define FG_STDC_BEGIN
    #define FG_STDC_END
#endif

#endif /* TYPEDECL_H_INCLUDED */
