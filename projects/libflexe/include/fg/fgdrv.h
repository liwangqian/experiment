#ifndef FGDRV_H_INCLUDED
#define FGDRV_H_INCLUDED

#include "typedecl.h"

FG_TYPE(fgdrv_if)
{
    void *(*malloc)(fgdrv_if *self, unsigned int nbytes,
        const char *file, unsigned int line);

    void (*free)(fgdrv_if *self, void *ptr);
};

FG_STDC_BEGIN

fgdrv *fgdrv_create(fgdrv_if *intf);
void   fgdrv_destroy(fgdrv *self);

void fgdrv_set_if(fgdrv *self, fgdrv_if *intf);
fgdrv_if *fgdrv_get_if(const fgdrv *self);

int fgdrv_add_card(fgdrv *self, unsigned int id, fgcard *card);
int fgdrv_del_card(fgdrv *self, unsigned int id);
fgcard *fgdrv_get_card(fgdrv *self, unsigned int id);

void *fgdrv_malloc(fgdrv *self, unsigned int nbytes,
    const char *file, unsigned int line);

void  fgdrv_free(fgdrv *self, void *ptr);

#define FGDRV_MALLOC(drv, nbytes) \
    fgdrv_malloc(drv, nbytes, __FILE__, __LINE__)

#define FGDRV_FREE(drv, ptr)    \
do {                            \
    fgdrv_free(drv, ptr);       \
    ptr = NULL;                 \
} while(0)

FG_STDC_END

#endif /* FGDRV_H_INCLUDED */
