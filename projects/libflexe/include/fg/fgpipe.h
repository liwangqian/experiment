#ifndef FGPIPE_H_INCLUDED
#define FGPIPE_H_INCLUDED

#include "typedecl.h"
#include "fgband.h"
#include "fgoduk.h"

FG_TYPE(fgpipe_init_config)
{
};

FG_TYPE(fgpipe_if)
{
    int (*pipe_init)(fgpipe *self, const fgpipe_init_config *config);
    void(*pipe_exit)(fgpipe *self);

    int (*pipe_set_tx_band)(fgpipe *self, fgbandwith);
    int (*pipe_set_rx_band)(fgpipe *self, fgbandwith);
    int (*pipe_set_tx_oduk)(fgpipe *self, fgoduk);
    int (*pipe_set_rx_oduk)(fgpipe *self, fgoduk);

    int (*pipe_get_tx_band)(const fgpipe *self, fgbandwith *);
    int (*pipe_get_rx_band)(const fgpipe *self, fgbandwith *);
    int (*pipe_get_tx_oduk)(const fgpipe *self, fgoduk *);
    int (*pipe_get_rx_oduk)(const fgpipe *self, fgoduk *);

    /* 数据持久化 */
    int (*pipe_save)(const fgpipe *self, void *rsvmemptr, uint32_t len);
    int (*pipe_restore)(fgpipe *self, void *rsvmemptr, uint32_t len);

    /* 健康检查，故障诊断 */
    int (*pipe_debug_check)(const fgpipe *self, void *outbuf, uint32_t len);
    int (*pipe_debug_inspect)(const fgpipe *self, void *outbuf, uint32_t len);
};

FG_STDC_BEGIN

fgpipe *fgpipe_create(fgchip *chip, unsigned int id);
int     fgpipe_init(fgpipe *self, fgpipe_if *intf, const fgpipe_init_config *);
int     fgpipe_exit(fgpipe *self);
int     fgpipe_destroy(fgpipe *self);

unsigned int fgpipe_get_id(const fgpipe *self);
fgchip *fgpipe_get_chip(const fgpipe *self);

int     fgpipe_add_port(fgpipe *self, unsigned int id, fgport *port);
fgport *fgpipe_get_port(fgpipe *self, unsigned int id);
int     fgpipe_del_port(fgpipe *self, unsigned int id);

FG_STDC_END

#endif /* FGPIPE_H_INCLUDED */
