#ifndef FGBAND_H_INCLUDED
#define FGBAND_H_INCLUDED

#include <stdint.h>

typedef uint64_t fgbandwith;

FG_STDC_BEGIN

void fgbandwidth_to_string(fgbandwith bw, char *buf, uint32_t bufsize);

FG_STDC_END

#endif /* FGBAND_H_INCLUDED */
