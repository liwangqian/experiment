#ifndef FGCARD_H_INCLUDED
#define FGCARD_H_INCLUDED

#include "typedecl.h"

FG_TYPE(fgcard_init_config)
{
};

FG_TYPE(fgcard_if)
{
    int (*card_init)(fgcard *self, const fgcard_init_config *config);
    void(*card_exit)(fgcard *self);
};

FG_STDC_BEGIN

fgcard *fgcard_create(fgdrv *drv, unsigned int id);
int     fgcard_init(fgcard *self, fgcard_if *intf);
int     fgcard_exit(fgcard *self);
int     fgcard_destroy(fgcard *self);

void fgcard_set_if(fgcard *self, fgcard_if *intf);
fgcard_if *fgcard_get_if(const fgcard *self);
unsigned int fgcard_get_id(const fgcard *self);

fgdrv  *fgcard_get_drv(const fgcard *self);
int     fgcard_add_chip(fgcard *self, unsigned int id, fgchip *chip);
void    fgcard_del_chip(fgcard *self, unsigned int id);
fgchip *fgcard_get_chip(fgcard *self, unsigned int id);

FG_STDC_END

#endif /* FGCARD_H_INCLUDED */
