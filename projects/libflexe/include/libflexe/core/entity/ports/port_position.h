/* Copyright (c) 2022-2022, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#pragma once

#include <cstdint>

namespace libflexe::core {

class port_position {
public:
    port_position() = default;
    port_position(uint8_t chassis, uint8_t slot, uint8_t card, uint8_t port)
        : chassis_{chassis}, slot_{slot}, card_{card}, port_{port}
    {}

    uint8_t chassis() const noexcept { return chassis_; }
    uint8_t slot()    const noexcept { return slot_;    }
    uint8_t card()    const noexcept { return card_;    }
    uint8_t port()    const noexcept { return port_;    }

    uint32_t position() const noexcept
    {
        return chassis_ << 24u | slot_ << 16u | card_ << 8u | port_;
    }

    bool equal_to(const port_position &other) const noexcept
    {
        return (&other == this) ||
               ((chassis_ == other.chassis_) &&
                (slot_ == other.slot_) &&
                (card_ == other.card_) &&
                (port_ == other.port_));
    }
private:
    uint8_t chassis_{0};
    uint8_t slot_{0};
    uint8_t card_{0};
    uint8_t port_{0};
};

inline bool operator==(const port_position &x, const port_position &y) noexcept
{
    return x.equal_to(y);
}

inline bool operator!=(const port_position &x, const port_position &y) noexcept
{
    return !(x == y);
}

} // namespace libflexe::core
