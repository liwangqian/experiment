/* Copyright (c) 2022-2022, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#pragma once

#include "libflexe/infra/result.h"
#include "libflexe/infra/typelist.h"
#include "libflexe/infra/tl/contains.hpp"
#include "libflexe/core/serialize/type_index.h"
#include "libflexe/infra/endian.h"

#include <cstdint>
#include <cstddef>

namespace libflexe::core {

class serializer {
protected:
    using supported_types = infra::typelist<
        int8_t, int16_t, int32_t, int64_t,
        uint8_t, uint16_t, uint32_t, uint64_t>;

public:
    virtual ~serializer() = default;

    template <typename T,
        typename = std::enable_if_t<infra::tl::contains_v<T, supported_types>>>
    infra::result<void> read(T *v) noexcept
    {
        auto type_check = check_type_index(type_index<T>());
        if (!type_check.success() || !type_check.value()) {
            return type_check.error();
        }

        T tmp;
        auto ret = read_byte_array(reinterpret_cast<int8_t*>(&tmp), sizeof(T));
        if (!ret.success()) {
            return ret;
        }

        *v = infra::endian::be_to_native(tmp);
        return {};
    }

    ////////////////////////////////////////////////////////////////////////////

    infra::result<void> write(int8_t x) noexcept
    {
        write_byte(static_cast<int8_t>(type_index<int8_t>()));
        write_byte(x);
        return {};      
    }

    infra::result<void> write(int16_t) noexcept;
    infra::result<void> write(int32_t) noexcept;
    infra::result<void> write(int64_t) noexcept;

    infra::result<void> write(uint8_t) ;
    infra::result<void> write(uint16_t) noexcept;
    infra::result<void> write(uint32_t) noexcept;
    infra::result<void> write(uint64_t) noexcept;

    infra::result<void> write(const char *) noexcept;

    infra::result<void> write(const int8_t  *, size_t) noexcept;
    infra::result<void> write(const int16_t *, size_t) noexcept;
    infra::result<void> write(const int32_t *, size_t) noexcept;
    infra::result<void> write(const int64_t *, size_t) noexcept;

    infra::result<void> write(const uint8_t  *, size_t) noexcept;
    infra::result<void> write(const uint16_t *, size_t) noexcept;
    infra::result<void> write(const uint32_t *, size_t) noexcept;
    infra::result<void> write(const uint64_t *, size_t) noexcept;

protected:
    virtual infra::result<void> read_byte(int8_t *) = 0;
    virtual infra::result<void> write_byte(int8_t) = 0;
    virtual infra::result<void> read_byte_array(int8_t *, size_t) = 0;
    virtual infra::result<void> write_byte_array(const int8_t *, size_t) = 0;

    infra::result<bool> check_type_index(uint8_t ti) noexcept
    {
        int8_t t = 0;
        auto ret = read_byte(&t);
        if (!ret.success()) {
            return ret.error();
        }

        return (t & 0x1fu) == ti;
    }
};

} // namespace libflexe::core
