/* Copyright (c) 2022-2022, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#pragma once

#include "libflexe/infra/noncopyable.h"
#include "libflexe/infra/result.h"
#include "libflexe/infra/intrusive_ptr.h"
#include "libflexe/infra/stl/unordered_map.h"
#include <mutex>

namespace libflexe::core {

template <typename Entity, typename EntitySerializer>
class basic_repo final : public infra::noncopyable {
public:
    using entity_type = Entity;
    using serializer = EntitySerializer;
    using entity_id = typename Entity::entity_id;
    using entity_ptr = infra::intrusive_ptr<Entity>;
    using const_entity_ptr = const entity_ptr;

    infra::result<entity_ptr> fetch(entity_id entid)
    {
        std::scoped_lock<std::mutex> lock{mtx_};
        auto it = entities_.find(entid);
        if (it != entities_.end()) {
            return it->second;
        }
        return infra::error{infra::error_code::E_NOT_EXIST};
    }

    infra::result<const_entity_ptr> fetch(entity_id entid) const
    {
        std::scoped_lock<std::mutex> lock{mtx_};
        auto it = entities_.find(entid);
        if (it != entities_.end()) {
            return it->second;
        }
        return infra::error{infra::error_code::E_NOT_EXIST};
    }

    infra::result<void> store(entity_ptr);

    infra::result<void> add(entity_ptr ent)
    {
        if (ent == nullptr) {
            return infra::error{infra::error_code::E_NULL_PTR};
        }

        std::scoped_lock<std::mutex> lock{mtx_};
        entities_[ent->id()] = std::move(ent);

        return {};
    }

    infra::result<entity_ptr> remove(entity_id entid)
    {
        std::scoped_lock<std::mutex> lock{mtx_};
        auto it = entities_.find(entid);
        if (it != entities_.end()) {
            entities_.erase(it);
            return *it;
        }
        return infra::error{infra::error_code::E_NOT_EXIST};
    }

    bool contains(entity_id entid) const
    {
        std::scoped_lock<std::mutex> lock{mtx_};
        return entities_.find(entid) != entities_.end();
    }

private:
    mutable std::mutex mtx_;
    infra::stl::unordered_map<entity_id, entity_ptr> entities_;
};

} // namespace libflexe::core
