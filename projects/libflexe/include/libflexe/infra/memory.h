/* Copyright (c) 2022-2022, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#pragma once

#include "allocator.h"

namespace libflexe::infra {

template <typename T, typename ...Args>
inline static T* new_object(Args&& ...args)
{
    auto alloc = infra::allocator<T>();
    auto ptr = alloc.allocate(1);
    if (ptr == nullptr) { return nullptr; }
    alloc.construct(ptr, std::forward<Args>(args)...);
    return ptr;
}

template <typename T, typename ...Args>
inline static T* new_object_n(size_t n, Args&& ...args)
{
    auto alloc = infra::allocator<T>();
    auto ptr = alloc.allocate(n);
    if (ptr == nullptr) { return nullptr; }

    for (size_t i = 0; i < n; ++i) {
        alloc.construct(ptr + i, std::forward<Args>(args)...);
    }
    return ptr;
}

template <typename T>
inline static void delete_object(T *ptr)
{
    if (ptr == nullptr) { return; }

    auto alloc = infra::allocator<T>();
    alloc.destroy(ptr);
    alloc.deallocate(ptr, 1);
}

template <typename T>
inline static void delete_object_n(T *ptr, size_t n)
{
    if (ptr == nullptr) { return; }

    auto alloc = infra::allocator<T>();
    for (size_t i = 0; i < n; ++i) {
        alloc.destroy(ptr + i);
    }
    alloc.deallocate(ptr, n);
}

} // namespace libflexe::infra
