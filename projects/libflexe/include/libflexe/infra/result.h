/* Copyright (c) 2022-2022, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#pragma once

#include "libflexe/infra/error.h"
#include <type_traits>
#include <variant>

namespace libflexe::infra {

template <typename T>
class result {
public:
    ~result() = default;
    result(infra::error &&) noexcept;
    result(const infra::error &) noexcept;
    result(const T &);
    result(T &&) noexcept;

    result(const result &);
    result(result &&) noexcept;
    result &operator=(const result &);
    result &operator=(result &&) noexcept;

    bool success() const noexcept;

    const T &value() const noexcept;

    const infra::error &error() const noexcept;

private:
    bool success_{false};
    std::variant<T, infra::error> v_;
};

template <typename T>
class result<T*> {
public:
    using pointer = std::add_pointer_t<T>;
    ~result();

    result(infra::error &&) noexcept;
    result(const infra::error &) noexcept;
    result(pointer) noexcept;

    result(const result &);
    result(result &&) noexcept;
    result &operator=(const result &);
    result &operator=(result &&) noexcept;

    bool success() const noexcept;

    const pointer value() const noexcept;
    pointer defer() noexcept;

    const infra::error &error() const noexcept;

private:
    bool success_{false};
    std::variant<pointer, infra::error> v_;
};

template <>
class result<void> {
public:
    result() = default;
    result(infra::error &&e) noexcept
        : success_{false}, e_{std::move(e)}
    {
    }

    result(const infra::error &e) noexcept
        : success_{false}, e_{e}
    {
    }

    result(const result &r)
        : success_{r.success_}, e_{r.e_}
    {
    }

    result(result &&r)
        : success_{r.success_}, e_{std::move(r.e_)}
    {
    }

    result &operator=(const result &r)
    {
        if (&r != this) {
            success_ = r.success_;
            e_ = r.e_;
        }
        return *this;
    }

    result &operator=(result &&r)
    {
        if (&r != this) {
            std::swap(success_, r.success_);
            std::swap(e_, r.e_);
        }
        return *this;
    }

    bool success() const noexcept
    {
        return success_;
    }

    const infra::error &error() const noexcept
    {
        return e_;
    }

private:
    bool success_{true};
    infra::error e_;
};

// result\<infra::error> is ill-formed
// use result<void> instead
template <> class result<infra::error>;

} // namespace libflexe::infra

/////////////implement/////////
#include "libflexe/infra/details/result_impl.h"
