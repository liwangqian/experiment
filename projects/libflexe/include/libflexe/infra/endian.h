/* Copyright (c) 2022-2022, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#pragma once

#include <cstdint>

namespace libflexe::infra {
namespace endian {

enum class order {
    le,
    be,
#ifdef LIBFLEXE_ENDIAN_LE
    native = le,
#else
    native = be
#endif
};

inline order endian_order() noexcept
{
    constexpr union {
        char c[4];
        int32_t i;
    } d = { .i = 0x12345678 };

    return (d.c[0] == 0x78) ? order::le : order::be;
}

#define i8(x)  static_cast<int8_t>(x)
#define i16(x) static_cast<int16_t>(x)
#define i32(x) static_cast<int32_t>(x)
#define i64(x) static_cast<int64_t>(x)

#define u8(x)  static_cast<uint8_t>(x)
#define u16(x) static_cast<uint16_t>(x)
#define u32(x) static_cast<uint32_t>(x)
#define u64(x) static_cast<uint64_t>(x)


inline int8_t endian_reverse(int8_t x) noexcept
{
    return x;
}

inline int16_t endian_reverse(int16_t x) noexcept
{
    return u16(u16(x) << 8u | u16(x) >> 8u);
}

inline int32_t endian_reverse(int32_t x) noexcept
{
    auto step16 = u32(x) << 16u | u32(x) >> 16u;
    return ((u32(step16) << 8u) & 0xff00ff00u) |
           ((u32(step16) >> 8u) & 0x00ff00ffu);
}

inline int64_t endian_reverse(int64_t x) noexcept
{
    auto step32 = u64(x) << 32u | u64(x) >> 32u;
    auto step16 = (step32 << 16u) & 0xffff0000ffff0000ull |
                  (step32 >> 16u) & 0x0000ffff0000ffffull;
    return i64(((step16 << 8u) & 0xff00ff00ff00ff00ull) |
               ((step16 >> 8u) & 0x00ff00ff00ff00ffull));
}

inline uint8_t endian_reverse(uint8_t x) noexcept
{
    return x;
}

inline uint16_t endian_reverse(uint16_t x) noexcept
{
    return (x << 8u) | (x >> 8u);
}

inline uint32_t endian_reverse(uint32_t x) noexcept
{
    auto step16 = (x << 16u) | (x >> 16u);
    return ((step16 << 8u) & 0xff00ff00u) |
           ((step16 >> 8u) & 0x00ff00ffu);
}

inline uint64_t endian_reverse(uint64_t x) noexcept
{
    auto step32 = x << 32u | x >> 32u;
    auto step16 = (step32 << 16u) & 0xffff0000ffff0000ull |
                  (step32 >> 16u) & 0x0000ffff0000ffffull;
    return ((step16 << 8u) & 0xff00ff00ff00ff00ull) |
           ((step16 >> 8u) & 0x00ff00ff00ff00ffull);
}

#undef i8
#undef i16
#undef i32
#undef i64

#undef u8
#undef u16
#undef u32
#undef u64

template <typename T>
inline T le_to_native(T x) noexcept
{
    if constexpr (order::le == order::native) {
        return x;
    } else {
        return endian_reverse(x);
    }
}

template <typename T>
inline T native_to_le(T x)
{
    if constexpr (order::le == order::native) {
        return x;
    } else {
        return endian_reverse(x);
    }
}

template <typename T>
inline T be_to_native(T x) noexcept
{
    if constexpr (order::be == order::native) {
        return x;
    } else {
        return endian_reverse(x);
    }
}

template <typename T>
inline T native_to_be(T x)
{
    if constexpr (order::be == order::native) {
        return x;
    } else {
        return endian_reverse(x);
    }
}

} // endian

} // namespace libflexe::infra
