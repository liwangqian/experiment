/* Copyright (c) 2021, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#pragma once

#include "libflexe/infra/typelist.h"
#include <cstdint>

namespace libflexe::infra::tl {

static constexpr std::size_t npos = -1UL;

namespace detail {

template <typename T, std::size_t I, typename TL>
struct indexof_impl;

template <typename T, std::size_t I, typename Y0, typename ...Y>
struct indexof_impl<T, I, typelist<Y0, Y...>> {
    static constexpr auto value = std::is_same_v<T, Y0> ?
        I : indexof_impl<T, I+1, typelist<Y...>>::value;
};

template <typename T, std::size_t I>
struct indexof_impl<T, I, typelist<>> {
    static constexpr auto value = npos;
};

} // namespace detail

template <typename T, typename TL>
struct indexof {
    static constexpr auto value = detail::indexof_impl<T, 0, TL>::value;
};

template <typename T, typename TL>
static constexpr auto indexof_v = indexof<T, TL>::value;

} // namespace libflexe::infra::tl
