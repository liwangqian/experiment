/* Copyright (c) 2022-2022, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#pragma once

#include "libflexe/infra/stl/string.h"
#include "libflexe/infra/enumulator.h"
#include "libflexe/infra/error_category.h"
#include <array>

namespace libflexe::infra {

enum class domain_error_code {
    device_busy,
    domain_error_code_max
};

template <>
class error_category<domain_error_code> {
public:
    constexpr auto name() const noexcept
        -> stl::string_view
    {
        return stl::string_view("domain-error");
    }

    constexpr auto message(domain_error_code ec) const noexcept
        -> stl::string_view
    {
        return messages[to_underlying(ec)];
    }

private:
    using sv = stl::string_view;
    using messages_container = std::array<sv,
        to_underlying(domain_error_code::domain_error_code_max)>;

    static constexpr messages_container messages = {
        sv("device is busy."),
    };
};


class domain_error {
public:
    constexpr domain_error(stl::string_view file, uint32_t line,
        domain_error_code code)
        : file_{file}, line_{line}, code_{code}
    {}

    constexpr auto file() const noexcept
    {
        return file_;
    }

    constexpr auto line() const noexcept
    {
        return line_;
    }

    constexpr auto code() const noexcept
    {
        return code_;
    }

    constexpr void swap(domain_error &other) noexcept
    {
        if (&other != this) {
            // std::swap not defined for string_view
            stl::swap(file_, other.file_);
            std::swap(line_, other.line_);
            std::swap(code_, other.code_);
        }
    }

    constexpr domain_error(const domain_error &) noexcept = default;
    constexpr domain_error &operator=(const domain_error &) noexcept = default;
    constexpr domain_error(domain_error &&) noexcept = default;
    constexpr domain_error &operator=(domain_error &&) noexcept = default;

private:
    stl::string_view file_;
    uint32_t line_{0};
    domain_error_code code_;
    // stl::string params_;
};

#define FLEXE_ERROR(code) libflexe::infra::domain_error{__FILE__, __LINE__, code}

} // namespace libflexe::infra
