/* Copyright (c) 2022-2022, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef BROKER_H_INCLUDED
#define BROKER_H_INCLUDED

#include <future>
#include <mutex>
#include "libflexe/infra/event/ievent.h"
#include "libflexe/infra/stl/string.h"
#include "libflexe/infra/stl/forward_list.h"
#include "libflexe/infra/stl/unordered_map.h"
#include "libflexe/infra/stl/smart_ptr.h"

namespace libflexe::infra::event {

class topic;
class listener;

class broker {
public:
    enum {
        new_topic_evt = 0,
        del_topic_evt,
        broker_max_event = 9
    };

    class new_topic_event : public ievent {
    public:
        new_topic_event(const stl::string &producer,
            const stl::string &topic_name)
            : ievent(broker::new_topic_evt, producer)
            , topic_name{topic_name}
        {}

        const stl::string &topic_name;
    };

    explicit broker(stl::string name);

    int setup();

    const stl::string &name() const noexcept;

    int new_topic(const stl::string &name);
    int del_topic(const stl::string &name);
    
    /// @brief subscribe from the topic of the broker
    /// @param topic_name name of the topic
    /// @param listener_name name of the listener
    /// @param l the listener
    /// @return operation code
    int sub_from_topic(const stl::string &topic_name,
        const stl::string &listener_name, stl::shared_ptr<listener> l);

    stl::string sub_from_topic(const stl::string &topic_name,
        std::function<event_result(ievent&)>);

    void unsub_from_topic(const stl::string &topic_name,
        const stl::string &listener_name);
    
    int pub_to_topic(const stl::string &topic_name, stl::shared_ptr<ievent>);

    int pub_to_topic_async(const stl::string &topic_name,
        stl::shared_ptr<ievent>);

    bool has_topic(const stl::string &topic_name) const noexcept;

    stl::forward_list<stl::string> list_topics() const;

private:
    using topic_ptr = stl::shared_ptr<topic>;

    topic_ptr get_topic(const stl::string &name);


    stl::string name_;
    mutable std::mutex mtx_{};
    stl::unordered_map<stl::string, topic_ptr> topics_{};
};

} // namespace libflexe::infra::event

#endif /* BROKER_H_INCLUDED */
