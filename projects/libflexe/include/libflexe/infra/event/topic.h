/* Copyright (c) 2022-2022, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef LIBFLEXE_EVENT_TOPIC_H
#define LIBFLEXE_EVENT_TOPIC_H

#include <future>
#include <mutex>
#include "libflexe/infra/stl/string.h"
#include "libflexe/infra/stl/forward_list.h"
#include "libflexe/infra/stl/smart_ptr.h"
#include "libflexe/infra/stl/queue.h"

namespace libflexe::infra::event {

class listener;
class ievent;

class topic {
public:
    using listener_ptr = stl::shared_ptr<listener>;

    ~topic();

    explicit topic(stl::string name) noexcept;

    int setup() noexcept;

    const stl::string &name() const noexcept;

    int sub(const stl::string &name, listener_ptr);

    void unsub(const stl::string &name);

    int pub(stl::shared_ptr<ievent> evt);

    int pub_async(stl::shared_ptr<ievent> evt);

    bool contains(const stl::string &name) const;

private:
    void async_loop();

    volatile bool stop_{false};
    mutable std::mutex mtx_;
    std::mutex queue_mtx_;
    std::condition_variable cv_;
    std::thread *thd_{nullptr};
    stl::string name_;
    stl::forward_list<listener_ptr> listeners_;
    stl::queue<stl::shared_ptr<ievent>> evt_queue_;
};

} // namespace libflexe::infra::event

#endif /* LIBFLEXE_EVENT_TOPIC_H */