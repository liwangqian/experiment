/* Copyright (c) 2022-2022, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef LIBFLEXE_EVENT_TRACER_H
#define LIBFLEXE_EVENT_TRACER_H

#include "libflexe/infra/stl/string.h"
#include <chrono>

namespace libflexe::infra::event {

struct tracer {
    using clock = std::chrono::system_clock;
    using timepoint = std::chrono::time_point<clock>;

    explicit tracer(const stl::string &producer)
        : producer_{producer}
    {
    }

    void emit() noexcept
    {
        emit_time_ = clock::now();
    }

    void start() noexcept
    {
        start_time_ = clock::now();
    }

    void finished() noexcept
    {
        finished_time_ = clock::now();
    }

    void accept_by(const stl::string& /* listener */) noexcept
    {
        ++accept_count_;
    }

    const stl::string &producer() const noexcept
    {
        return producer_;
    }

    const timepoint &emit_time() const noexcept
    {
        return emit_time_;
    }

    const timepoint &start_time() const noexcept
    {
        return start_time_;
    }

    const timepoint &finished_time() const noexcept
    {
        return finished_time_;
    }

    unsigned int accept_count() const noexcept
    {
        return accept_count_;
    }

private:
    stl::string producer_{"<anoy>"};
    timepoint emit_time_;
    timepoint start_time_;
    timepoint finished_time_;
    unsigned int accept_count_{0};
};

} // namespace libflexe::infra::event

#endif /* LIBFLEXE_EVENT_TRACER_H */
