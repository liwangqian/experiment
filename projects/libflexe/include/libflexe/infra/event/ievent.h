/* Copyright (c) 2022-2022, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef LIBFLEXE_EVENT_IEVENT_H
#define LIBFLEXE_EVENT_IEVENT_H

#include "libflexe/infra/event/tracer.h"

namespace libflexe::infra::event {

enum class event_result {
    accept,
    reject,
    skip
};

class ievent {
public:
    using event_type = unsigned int;

    virtual ~ievent() = default;

    ievent(event_type type, const stl::string &source)
        : type_{type}, source_{source}
    {
    }

    event_type type() const noexcept
    {
        return type_;
    }

    bool accepted() const noexcept
    {
        return accept_count_ > 0;
    }

    auto accept_count() const noexcept
    {
        return accept_count_;
    }

    stl::string source() const noexcept
    {
        return source_;
    }

protected:
    friend class topic;

    virtual void on_start() noexcept;
    virtual void on_finished() noexcept;
    virtual void on_result(event_result, const stl::string &who) noexcept;

private:
    event_type type_{0};
    stl::string source_{"<annoy>"};
    unsigned int accept_count_{0};
};

} /* namespace libflexe::infra::event */

#endif /* LIBFLEXE_EVENT_IEVENT_H */