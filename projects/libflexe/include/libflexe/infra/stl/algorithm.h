/* Copyright (c) 2022-2022, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef LIBFLEXE_STL_ALGORITHM_H
#define LIBFLEXE_STL_ALGORITHM_H

#include <functional>

namespace libflexe::infra::stl {

namespace details {

struct iter_less_value {
    template<typename Iterator, typename Value>
    constexpr bool operator()(Iterator iter, Value& val) const noexcept
    {
        return *iter < val;
    }
};

}

template <typename ForwardIterator, typename T, typename Compara>
inline constexpr ForwardIterator
lower_bound(ForwardIterator first, ForwardIterator last,
    const T &v, Compara cmp) noexcept
{
    auto len = std::distance(first, last);
    while (len > 0) {
        auto half = len >> 1;
        auto mid = first;
        std::advance(mid, half);
        if (cmp(mid, v)) {
            first = mid;
            ++first;
            len = len - half - 1;
        } else {
	        len = half;
        }
	}
    return first;
}

template <typename ForwardIterator, typename T>
inline constexpr ForwardIterator
lower_bound(ForwardIterator first, ForwardIterator last, const T &v) noexcept
{
    return lower_bound(first, last, v, details::iter_less_value());
}

} /* namespace libflexe::infra::stl */


#endif /* LIBFLEXE_STL_ALGORITHM_H */
