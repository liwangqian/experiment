/* Copyright (c) 2022-2022, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef SMART_PTR_H_INCLUDED
#define SMART_PTR_H_INCLUDED

#include "libflexe/infra/memory.h"
#include <memory>

namespace libflexe::infra::stl {

template <typename T>
using shared_ptr = std::shared_ptr<T>;

template<typename T>
struct default_deleter {
    constexpr default_deleter() noexcept = default;

    template<typename U,
        typename = std::enable_if_t<std::is_convertible_v<U*, T*>>>
    default_deleter(const default_deleter<U>&) noexcept { }

    void operator()(T* ptr) const
    {
        static_assert(!std::is_void_v<T>, "can't delete pointer to incomplete type");
        static_assert(sizeof(T) > 0, "can't delete pointer to incomplete type");
        delete_object(ptr);
    }
};

template<typename T, size_t N>
struct default_deleter<T[N]> {
public:
    constexpr default_deleter() noexcept = default;

    template<typename U,
        typename = std::enable_if_t<std::is_convertible_v<U(*)[N], T(*)[N]>>>
    default_deleter(const default_deleter<U[N]>&) noexcept { }

    template<typename U>
    typename std::enable_if_t<std::is_convertible_v<U(*)[N], T(*)[N]>>
    operator()(U* ptr) const
    {
        static_assert(sizeof(T) > 0, "can't delete pointer to incomplete type");
        delete_object_n(ptr, N);
    }
};

template <typename T>
using unique_ptr = std::unique_ptr<T, default_deleter<T>>;

template <typename T, typename ...Args>
auto make_shared(Args &&... args)
{
    return std::allocate_shared<T>(allocator<T>(), std::forward<Args>(args)...);
}

template <typename T, typename ...Args>
auto make_unique(Args &&... args)
{
    return unique_ptr<T>(new_object<T>(std::forward<Args>(args)...));
}

} // namespace libflexe::infra::stl

#endif /* SMART_PTR_H_INCLUDED */
