/* Copyright (c) 2022-2022, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef LIBFLEXE_STL_THREADPOOL_H
#define LIBFLEXE_STL_THREADPOOL_H

#include <future>
#include <type_traits>
#include <functional>
#include "libflexe/infra/error.h"
#include "libflexe/infra/stl/queue.h"
#include "libflexe/infra/stl/vector.h"

namespace libflexe::infra::stl {

class threadpool {
public:
    using work = std::function<void()>;

    ~threadpool()
    {
        if (stop_) {
            return;
        }

        stop_ = true;
        cv_.notify_all();

        for (auto t : threads_) {
            if (t->joinable()) t->join();
            delete_object(t);
        }
    }

    threadpool() = default;

    int setup(unsigned int thread_count)
    {
        if (!threads_.empty()) {
            return threads_.size();
        }

        stop_ = false;
        for (auto i = 0u; i < thread_count; ++i) {
            auto thd = new_object<std::thread>(&threadpool::loop, this);
            if (thd != nullptr) {
                threads_.push_back(thd);
            }
        }
        return threads_.size();
    }
    
    template <typename Fn>
    int submit(Fn &&fn)
    {
        {
            std::unique_lock<std::mutex> lock{mtx_};
            works_.emplace(std::forward<Fn>(fn));
            cv_.notify_all();
        }
        return OK;
    }

private:
    void loop()
    {
        for (;!stop_;) {
            do {
                excute(take());
            } while (!works_.empty());
        }
    }

    work take()
    {
        work w;
        std::unique_lock<std::mutex> lock{mtx_};
        if (stop_ && works_.empty()) return w;

        cv_.wait(lock, [this]() { return !works_.empty() || !stop_; });
        if (!works_.empty()) {
            w = std::move(works_.front());
            works_.pop();
        }
        return w;
    }

    void excute(work w)
    {
        if (w) w();
    }

    bool stop_{true};
    std::mutex mtx_;
    std::condition_variable cv_;
    stl::vector<std::thread*> threads_;
    stl::queue<work> works_;
};

} /* namespace libflexe::infra::stl */

#endif /* LIBFLEXE_STL_THREADPOOL_H */