/* Copyright (c) 2022-2022, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef LIBFLEXE_STL_PRIORITY_MAP_H
#define LIBFLEXE_STL_PRIORITY_MAP_H

#include <utility>
#include <functional>
#include "libflexe/infra/stl/algorithm.h"
#include "libflexe/infra/allocator.h"
#include "libflexe/infra/memory.h"

namespace libflexe::infra::stl {

struct prime_hash_policy {

    constexpr std::size_t bucket_size_for(std::size_t n) const noexcept
    {
        auto iter = stl::lower_bound(primes, primes + n_primes - 1, n);
        next_resize = *iter;
        return *iter;
    }

    constexpr std::pair<bool, std::size_t>
    need_resize_bucket(std::size_t n_bkts, std::size_t n_elts,
        std::size_t n_ins) const noexcept
    {
        auto n_total = n_elts + n_ins;
        if (n_total <= next_resize) {
            return std::make_pair(false, 0);
        }

        if (n_total > n_bkts) {
            return std::make_pair(true,
                bucket_size_for(std::max(n_total, n_bkts * 2u)));
        }
        return std::make_pair(false, 0);
    }

    static constexpr auto n_primes = sizeof(unsigned long) != 8 ? 256 : 256 + 48;
    static constexpr unsigned long primes[n_primes] = {
        2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67,
        71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139,
        149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223,
        227, 229, 233, 239, 241, 251, 257, 263, 269, 271, 277, 281, 283, 293,
        307, 311, 313, 317, 331, 337, 347, 349, 353, 359, 367, 373, 379, 383,
        389, 397, 401, 409, 419, 421, 431, 433, 439, 443, 449, 457, 461, 463,
        467, 479, 487, 491, 499, 503, 509, 521, 523, 541, 547, 557, 563, 569,
        571, 577, 587, 593, 599, 601, 607, 613, 617, 619, 631, 641, 643, 647,
        653, 659, 661, 673, 677, 683, 691, 701, 709, 719, 727, 733, 739, 743,
        751, 757, 761, 769, 773, 787, 797, 809, 811, 821, 823, 827, 829, 839,
        853, 857, 859, 863, 877, 881, 883, 887, 907, 911, 919, 929, 937, 941,
        947, 953, 967, 971, 977, 983, 991, 997, 1009, 1013, 1019, 1021, 1031,
        1033, 1039, 1049, 1051, 1061, 1063, 1069, 1087, 1091, 1093, 1097, 1103,
        1109, 1117, 1123, 1129, 1151, 1153, 1163, 1171, 1181, 1187, 1193, 1201,
        1213, 1217, 1223, 1229, 1231, 1237, 1249, 1259, 1277, 1279, 1283, 1289,
        1291, 1297, 1301, 1303, 1307, 1319, 1321, 1327, 1361, 1367, 1373, 1381,
        1399, 1409, 1423, 1427, 1429, 1433, 1439, 1447, 1451, 1453, 1459, 1471,
        1481, 1483, 1487, 1489, 1493, 1499, 1511, 1523, 1531, 1543, 1549, 1553,
        1559, 1567, 1571, 1579, 1583, 1597, 1601, 1607, 1609, 1613, 1619, 1621,
        1627, 1637, 1657, 1663, 1667, 1669, 1693, 1697, 1699, 1709, 1721, 1723,
        1733, 1741, 1747, 1753, 1759, 1777, 1783, 1787, 1789, 1801, 1811, 1823,
        1831, 1847, 1861, 1867, 1871, 1873, 1877, 1879, 1889, 1901, 1907, 1913,
        1931, 1933, 1949, 1951, 1973, 1979, 1987, 1993, 1997, 1999, 2003
    };

    mutable std::size_t next_resize{0};
};

template <typename K,
    typename V,
    typename H = std::hash<K>,
    typename P = std::equal_to<K>,
    typename Alloc = infra::allocator<V>>
class priority_map {
public:
    using key_type = K;
    using mapped_type = V;
    using value_type = V;
    using hasher = H;
    using key_equal = P;
    using allocator_type = Alloc;

    class iterator;

    ~priority_map() = default;
    priority_map() = default;

    explicit priority_map(std::size_t n);

    void insert(const key_type &key, const value_type &value);

    iterator find(const key_type &key);

    std::size_t capacity() const noexcept;
    std::size_t size() const noexcept;
    bool empty() const noexcept;


private:
    struct map_node {
        map_node* bkt_next{nullptr};
        map_node* list_next{nullptr};
        mapped_type value;

        map_node(mapped_type &&v)
            : value{std::move(v)}
        {}

        template <typename ...Args>
        map_node(Args && ...args)
            : value{std::forward<Args>(args)...}
        {}
    };

    map_node **bukects{nullptr};
    map_node *list{nullptr};
    std::size_t count{0};

    template <typename ...Args>
    map_node *allocate_node(Args && ...args)
    {
        return new_object<map_node>(std::forward<Args>(args)...);
    }

    void destroy_nodes()
    {
        map_node *iter = list;
        while (iter) {
            list = list->list_next;
            delete_object(iter);
            iter = list;
        }
    }


};

} /* libflexe::infra::stl */


#endif /* LIBFLEXE_STL_PRIORITY_MAP_H */