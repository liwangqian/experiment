/* Copyright (c) 2022-2022, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#pragma once

#include "../allocator.h"
#include <string>
#include <string_view>

namespace libflexe::infra::stl {

template <
    typename CharType,
    typename CharTraits = std::char_traits<CharType>>
using basic_string = std::basic_string<CharType, CharTraits, infra::allocator<CharType>>;

using string = basic_string<char>;
using wstring = basic_string<wchar_t>;

// string view dos not need allocator
using string_view = std::string_view;
using wstring_view = std::wstring_view;

// std::swap not defined for string_view
inline constexpr void swap(string_view &x, string_view &y) noexcept
{
    string_view tmp = x;
    x = y;
    y = x;
}

// std::swap not defined for wstring_view
inline constexpr void swap(wstring_view &x, wstring_view &y) noexcept
{
    wstring_view tmp = x;
    x = y;
    y = x;
}

} // namespace libflexe::infra::stl

namespace std {

#if FLEXE_USE_DEFAULT_ALLOCATOR
template <>
struct hash<libflexe::infra::stl::string> {
    using string = libflexe::infra::stl::string;
    size_t operator()(const string& s) const noexcept
    {
        return std::hash<std::string_view>()({s.c_str(), s.size()});
    }
};
#endif

} // namespace std
