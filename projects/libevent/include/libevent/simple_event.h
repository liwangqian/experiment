/* Copyright (c) 2023-2023, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef SIMPLE_EVENT_H_INCLUDED
#define SIMPLE_EVENT_H_INCLUDED

#include "libevent/base/event.h"
#include "libevent/stl.h"
#include <tuple>

namespace libev {

template <typename ...Args>
class simple_event : public base::event {
public:
    explicit simple_event(event_type type,
        const stl::string &source, Args ...args)
        : base::event{type}
        , source_{source}
        , args_{std::forward<Args>(args)...}
    {
    }

    template <std::size_t Index>
    decltype(auto) arg() const
    {
        return std::get<Index>(args_);
    }

    const stl::string &source() const noexcept override
    {
        return source_;
    }

private:
    stl::string source_;
    std::tuple<Args...> args_;
};

template <typename ...Args>
auto make_simple_event(event_type type, const stl::string &source, Args &&...args)
{
    return stl::make_shared<simple_event<Args...>>(type, source, std::forward<Args>(args)...);
}

} // namespace libev

#endif /* SIMPLE_EVENT_H_INCLUDED */
