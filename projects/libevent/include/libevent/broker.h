/* Copyright (c) 2023-2023, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef BROKER_H_INCLUDED
#define BROKER_H_INCLUDED

#include "libevent/typedecl.h"
#include "libevent/simple_event.h"
#include "libevent/base/topic.h"

namespace libev {

class broker {
public:
    struct config {};

    static constexpr char const *broker_topic = "topic/broker";
    static constexpr event_type evt_new_topic = 1u;
    static constexpr event_type evt_del_topic = 2u;

    using broker_event = simple_event<stl::string>;

    ~broker();

    explicit broker(const stl::string &name);

    status setup(const config &cfg);

    void shutdown() noexcept;

    const stl::string &name() const noexcept;

    status new_topic(const stl::string &name,
        const base::topic::config &);
    status del_topic(const stl::string &name);

    bool has_topic(const stl::string &name) const noexcept;

    status sub_from_topic(const stl::string &topic,
        stl::unique_ptr<base::listener> listener);

    status unsub_from_topic(const stl::string &topic,
        const stl::string &listener);

    status pub_to_topic(const stl::string &topic,
        stl::shared_ptr<base::event>, const schedule_policy &);

    status pub_to_topic_async(const stl::string &topic,
        stl::shared_ptr<base::event>, const schedule_policy &);

    stl::forward_list<stl::string> list_topics() const;

    size_t topics_count() const noexcept;

private:
    stl::shared_ptr<base::topic> get_topic(const stl::string &name) const;
    void emit_broker_event(event_type etype, const stl::string &topic_name);

    stl::string name_;
    mutable stl::shared_mutex mutex_;
    stl::unordered_map<stl::string, stl::shared_ptr<base::topic>> topics_;
};

} // namespace libev

#endif /* BROKER_H_INCLUDED */
