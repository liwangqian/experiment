/* Copyright (c) 2023-2023, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef STL_H_INCLUDED
#define STL_H_INCLUDED

#include <memory>
#include <queue>
#include <string>
#include <forward_list>
#include <shared_mutex>
#include <unordered_map>

namespace libev::stl {
    template <typename T>
    using allocator = std::allocator<T>;

    template <typename T>
    using forward_list = std::forward_list<T>;

    using string = std::string;

    template <typename T>
    using unique_ptr = std::unique_ptr<T>;

    template <typename T, typename ...Args>
    auto make_unique(Args &&... args)
    {
        return unique_ptr<T>(new T(std::forward<Args>(args)...));
    }

    template <typename T, typename ...Args>
    auto make_shared(Args &&... args)
    {
        return std::allocate_shared<T>(
            allocator<T>(), std::forward<Args>(args)...);
    }

    template <typename T>
    using shared_ptr = std::shared_ptr<T>;

    using shared_mutex = std::shared_mutex;
    using mutex = std::mutex;

    template <typename K, typename V>
    using unordered_map = std::unordered_map<K, V>;

    template <typename T>
    using queue = std::queue<T>;

    template <typename T, typename = std::enable_if_t<std::is_enum_v<T>>>
    constexpr auto to_underlying(T v) noexcept
    {
        return static_cast<std::underlying_type_t<T>>(v);
    }

} // namespace libevent::stl

#endif /* STL_H_INCLUDED */
