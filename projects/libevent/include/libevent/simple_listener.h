/* Copyright (c) 2023-2023, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef SIMPLE_LISTENER_H_INCLUDED
#define SIMPLE_LISTENER_H_INCLUDED

#include "libevent/base/listener.h"
#include "libevent/base/event.h"
#include "libevent/stl.h"

namespace libev {

template <typename F>
class simple_listener : public base::listener {
public:
    using functor_type = F;
    explicit simple_listener(functor_type func)
        : base::listener{annoy_name()}
        , func_{std::move(func)}
    {}

    base::event::result on_event(base::event &evt) noexcept override
    {
        return func_(evt);
    }

private:
    static auto null_listener(base::event &)
    {
        return base::event::kskip;
    }

    static stl::string annoy_name()
    {
        static size_t idx = 0;
        return stl::string("annoy_listener#" + std::to_string(idx++));
    }

    functor_type func_;
};

template <typename F>
auto make_simple_listener(F &&f)
{
    return stl::make_unique<simple_listener<F>>(std::forward<F>(f));
}

} // namespace libev

#endif /* SIMPLE_LISTENER_H_INCLUDED */
