/* Copyright (c) 2023-2023, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <catch2/catch.hpp>
#include "libevent/broker.h"
#include "libevent/status.h"
#include "libevent/simple_listener.h"
#include "libevent/simple_event.h"

TEST_CASE("test.libev.broker.create")
{
    libev::broker broker{"test-broker"};
    libev::broker::config cfg;
    REQUIRE(broker.setup(cfg) == LIBEV_STATUS_OK);
}

TEST_CASE("test.libev.broker.shutdown")
{
    libev::broker broker{"test-broker"};
    REQUIRE_NOTHROW(broker.shutdown());
}

TEST_CASE("test.libev.broker.name")
{
    libev::broker broker{"test-broker"};
    libev::broker::config cfg;
    broker.setup(cfg);
    REQUIRE(broker.name() == "test-broker");
}

TEST_CASE("test.libev.broker.topic.new/del")
{
    auto x = libev::stl::to_underlying<libev::priority>(libev::priority::urgent);
    GIVEN("a broker") {
        libev::broker broker{"test-broker"};
        libev::broker::config cfg;
        broker.setup(cfg);

        THEN("new topic ok") {
            REQUIRE(broker.new_topic("topic/test", {}) == LIBEV_STATUS_OK);
            REQUIRE(broker.topics_count() == 1);
            REQUIRE(broker.has_topic("topic/test"));

            AND_THEN("sub from the topic ok") {
                auto ret = broker.sub_from_topic("topic/test",
                    libev::make_simple_listener(
                        [](libev::base::event &evt) {
                            return libev::base::event::kaccept;
                        })
                    );
                REQUIRE(ret == LIBEV_STATUS_OK);
            }

            AND_THEN("pub event to topic ok") {
                auto event = libev::make_simple_event(401, "test", "Page Not Found");
                REQUIRE(event != nullptr);
                auto ret = broker.pub_to_topic("topic/test", event, {});
                REQUIRE(ret == LIBEV_STATUS_OK);
                REQUIRE(event->accepted());
            }

            AND_THEN("del topic ok") {
                REQUIRE(broker.del_topic("topic/test") == LIBEV_STATUS_OK);
                REQUIRE(broker.topics_count() == 0);
                REQUIRE_FALSE(broker.has_topic("topic/test"));
            }
        }
    }
}
