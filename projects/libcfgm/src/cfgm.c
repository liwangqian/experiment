/* Copyright (c) 2022-2022, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "libcfgm/cfgm.h"
#include "libcfgm/error.h"
#include "libcfgm/file.h"
#include "libcfgm/parser.h"

#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

struct cfgm {
    cfgm_parser_factory_t factories[CFGM_FORMAT_MAX];
};

static inline cfgm_parser_factory_t *cfgm_get_parser_factory(
    cfgm_t *inst, cfgm_format_t fmt)
{
    return inst && (fmt < CFGM_FORMAT_MAX) ? &inst->factories[fmt] : NULL;
}

static int cfgm_initialize(cfgm_t *inst)
{
    return CFGM_OK;
}

static void cfgm_deinitialize(cfgm_t *inst)
{
    //
}

cfgm_t *cfgm_create(void)
{
    cfgm_t *inst = (cfgm_t *)malloc(sizeof(cfgm_t));
    if (inst == NULL || cfgm_initialize(inst) != CFGM_OK) {
        return NULL;
    }

    return inst;
}

void cfgm_destroy(cfgm_t *inst)
{
    if (inst != NULL) {
        cfgm_deinitialize(inst);
        free(inst);
    }
}

int cfgm_from_string(cfgm_t *inst, const char *content, unsigned int len,
    cfgm_format_t fmt, const cfgm_rule_t *rules, unsigned int rules_count)
{
    cfgm_parser_factory_t *factory = cfgm_get_parser_factory(inst, fmt);
    if (!cfgm_parser_factory_valid(factory)) {
        return CFGM_EFORMAT;
    }

    cfgm_parser_t *parser = cfgm_parser_factory_create(factory);
    int ret = cfgm_parser_parse(parser, content, len, rules, rules_count);
    cfgm_parser_factory_destroy(factory, parser);

    return ret;
}

int cfgm_from_file(cfgm_t *inst, const char *file,
    const cfgm_rule_t *rules, unsigned int rules_count)
{
    unsigned int size = cfgm_file_size(file);
    if (size == 0) {
        return CFGM_EPARAM;
    }

    cfgm_string_t cfg = {0};
    int ret = cfgm_string_create(size, &cfg);
    if (ret != CFGM_OK) {
        return ret;
    }

    ret = cfgm_file_read(file, &cfg);
    if (ret != CFGM_OK) {
        cfgm_string_destroy(&cfg);
        return ret;
    }

    cfgm_format_t fmt = cfgm_format_from_extension(cfgm_file_extension(file));
    if (fmt == CFGM_FORMAT_INVALID) {
        cfgm_string_destroy(&cfg);
        return CFGM_EFORMAT;
    }

    ret = cfgm_from_string(inst, cfg.buf, cfg.size, fmt, rules, rules_count);
    cfgm_string_destroy(&cfg);
    return ret;
}

#ifdef __cplusplus
}
#endif
