/* Copyright (c) 2022-2022, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "libcfgm/types.h"
#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

#define TYPE_INFO(tt) { .size = sizeof(tt), .desc = #tt }
#define TYPE_MAP(ti, tt)  [ti] = TYPE_INFO(tt)

static const struct {
    int size;
    const char *desc;
} g_type_info[] = {
    TYPE_MAP(CFGM_DTYPE_I8,     int8_t),
    TYPE_MAP(CFGM_DTYPE_I16,    int16_t),
    TYPE_MAP(CFGM_DTYPE_I32,    int32_t),
    TYPE_MAP(CFGM_DTYPE_I64,    int64_t),
    TYPE_MAP(CFGM_DTYPE_U8,     uint8_t),
    TYPE_MAP(CFGM_DTYPE_U16,    uint16_t),
    TYPE_MAP(CFGM_DTYPE_U32,    uint32_t),
    TYPE_MAP(CFGM_DTYPE_U64,    uint64_t),
    TYPE_MAP(CFGM_DTYPE_FLOAT,  float),
    TYPE_MAP(CFGM_DTYPE_DOUBLE, double),
    TYPE_MAP(CFGM_DTYPE_BOOLEAN,bool),

    [CFGM_DTYPE_INVALID] = { 0, "invalid" },
    [CFGM_DTYPE_STRING]  = { 0, "string"  },
    [CFGM_DTYPE_ARRAY]   = { 0, "array"   },
    [CFGM_DTYPE_STRUCT]  = { 0, "struct"  },
};

#define TYPE_INFO_MAP_SIZE (sizeof(g_type_info) / sizeof(g_type_info[0]))
#define TYPE_INFO_SIZE(ti) (g_type_info[ti].size)
#define TYPE_INFO_DESC(ti) (g_type_info[ti].desc)

static char __check_type_info_size[TYPE_INFO_MAP_SIZE == CFGM_DTYPE_MAX ? 0 : -1];

int cfgm_datatype_size(cfgm_datatype_t type)
{
    if (type < CFGM_DTYPE_MAX) {
        return TYPE_INFO_SIZE(type);
    }
    return TYPE_INFO_SIZE(CFGM_DTYPE_INVALID);
}

const char *cfgm_datatype_desc(cfgm_datatype_t type)
{
    if (type < CFGM_DTYPE_MAX) {
        return TYPE_INFO_DESC(type);
    }
    return TYPE_INFO_DESC(CFGM_DTYPE_INVALID);
}

#ifdef __cplusplus
}
#endif
