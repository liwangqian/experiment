/* Copyright (c) 2022-2022, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "token.h"
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct token_list_node {
    struct token_list_node *next;
    cfgm_token_t token;
} token_list_node_t;

/* 使用token factory来管理token的生命周期 */
struct cfgm_token_factory {
    token_list_node_t tokens;
    token_list_node_t *tail;
};

/* 创建一个token factory */
cfgm_token_factory_t *cfgm_token_factory_create()
{
    cfgm_token_factory_t *factory = malloc(sizeof(cfgm_token_factory_t));
    if (factory != NULL) {
        // initialize
        factory->tokens.next = NULL;
        factory->tokens.token.type = TK_INVALID;
        factory->tail = &factory->tokens;
    }
    return factory;
}

/* 销毁一个token factory */
void cfgm_token_factory_destroy(cfgm_token_factory_t *inst)
{
    if (inst == NULL) {
        return;
    }

    token_list_node_t *node = inst->tokens.next;
    while (node) {
        token_list_node_t *tmp = node;
        node = node->next;
        free(tmp);
    }
    free(inst);
}

/* 新建一个token，返回token指针，token的生命周期由factory统一管理 */
cfgm_token_t *cfgm_token_factory_new_token(cfgm_token_factory_t *factory,
    unsigned int type, const cfgm_string_ref_t *value,
    const cfgm_source_info_t *source)
{
    if (factory == NULL) {
        return NULL;
    }

    token_list_node_t *node = malloc(sizeof(token_list_node_t));
    if (node != NULL) {
        node->token.type = type;
        node->token.value = *value;
        node->token.source = *source;
        factory->tail->next = node;
        factory->tail = node;
    }
    return &node->token;
}

#ifdef __cplusplus
}
#endif
