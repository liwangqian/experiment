/* Copyright (c) 2022-2022, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "parser.h"
#include "lexer.h"
#include "libcfgm/utils.h"
#include "libcfgm/error.h"
#include <string.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct base_node {
    struct base_node *next;
    unsigned int type;
} base_node;

typedef struct vector_expression_node {
    base_node base;
    base_node items;
} vector_expression_node;

typedef struct struct_expression_node {
    base_node base;
    base_node fields;
} struct_expression_node;

typedef struct constant_expression_node {
    base_node base;
    union {
        uint8_t   u8;
        uint16_t u16;
        uint32_t u32;
        uint64_t u64;
        int8_t    i8;
        int16_t  i16;
        int32_t  i32;
        int64_t  i64;
        float    flt;
        double   dbl;
        bool     sta;
        cfgm_string_ref_t str;
    };
} constant_expression_node;

typedef struct expression_node {
    base_node base;
    union {
        vector_expression_node *vector;
        struct_expression_node *stru;
        constant_expression_node *constant;
    };
} expression_node;

typedef struct assigned_statement_node {
    base_node base;
    cfgm_string_ref_t ident;
    expression_node *expr;
} assigned_statement_node;

typedef struct cfg_tree {
    assigned_statement_node *statements;
} cfg_tree;

static int parse_assigned_statement(
    cfgm_cml_lexer_t *lexer, cfgm_token_t *tok)
{
    if (tok->type != TK_ID) {
        return CFGM_ESTMT_IDENT;
    }

    cfgm_datatype_t type = CFGM_DTYPE_INVALID;
    cfgm_string_ref_t ident = tok->value;
    tok = cfgm_cml_lexer_lex(lexer);
    if (tok == NULL) return CFGM_ESTMT_UNFINISHED;

    expect(tok->type == TK_PUNCT && tok->value.buf[0] == '=');
    tok = cfgm_cml_lexer_lex(lexer);
    if (tok->type == TK_PUNCT) {
        if (tok->value.buf[0] == '[') {
            type = CFGM_DTYPE_ARRAY;
            do {
                tok = cfgm_cml_lexer_lex(lexer);
                
            }
        }
    }
}

static int parse_statement_list(cfgm_cml_lexer_t *lexer)
{
    int ret = CFGM_OK;
    cfgm_token_t *tok = NULL;
    while ((tok = cfgm_cml_lexer_lex(lexer)) != NULL) {
        ret = parse_assigned_statement(lexer, tok);
        if (ret != CFGM_OK) {
            break;
        }
    }
    return ret;
}

static int cfgm_cml_parser_parse_apply(
    const char *cfg, unsigned int len,
    const cfgm_rule_t *rules, unsigned int rules_count)
{
    cfgm_cml_lexer_t *lexer = cfgm_cml_lexer_create(
        cfg, len, cfgm_token_factory_create());
    if (lexer == NULL) return CFGM_EMEM;

    return parse_statement_list(lexer);
}

static int cfgm_cml_parser_parse(
    struct cfgm_parser *self, const char *cfg, unsigned int len,
    const cfgm_rule_t *rules, unsigned int rules_count)
{
    cfgm_cml_parser_t *inst = container_of(self, cfgm_cml_parser_t, parser);
    cfgm_parser_context_init(&inst->ctx, cfg, len, rules, rules_count);

    int ret = cfgm_cml_parser_parse_apply(cfg, len, rules, rules_count);

    cfgm_parser_context_exit(&inst->ctx);
    return ret;
}

int cfgm_cml_parser_init(cfgm_cml_parser_t *inst)
{
    inst->parser.parse = cfgm_cml_parser_parse;
}

void cfgm_cml_parser_exit(cfgm_cml_parser_t *inst)
{
    inst->parser.parse = NULL;
}

#ifdef __cplusplus
}
#endif
