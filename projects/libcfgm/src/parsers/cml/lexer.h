/* Copyright (c) 2022-2022, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef LIBCFGM_CML_LEXER_H
#define LIBCFGM_CML_LEXER_H

#include "token_type.h"
#include "../parser_error.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct cfgm_cml_lexer {
    const char *input;
    unsigned int size;
    unsigned int index;
    cfgm_parser_error_t error;
    cfgm_source_info_t source;
    cfgm_token_factory_t *factory;
} cfgm_cml_lexer_t;

cfgm_cml_lexer_t *cfgm_cml_lexer_create(const char *input, unsigned int size,
    cfgm_token_factory_t *);

void cfgm_cml_lexer_destroy(cfgm_cml_lexer_t *lexer);

cfgm_token_t *cfgm_cml_lexer_lex(cfgm_cml_lexer_t *lexer);

#ifdef __cplusplus
}
#endif

#endif /* LIBCFGM_CML_LEXER_H */
