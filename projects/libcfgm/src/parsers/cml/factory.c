/* Copyright (c) 2022-2022, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "factory.h"
#include "parser.h"
#include "libcfgm/utils.h"
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

static cfgm_parser_t *cfgm_cml_parser_create(void *priv)
{
    cfgm_cml_parser_t *inst = malloc(sizeof(cfgm_cml_parser_t));
    if (inst == NULL) {
        return NULL;
    }
    if (cfgm_cml_parser_init(inst) != CFGM_OK) {
        (void)free(inst);
    }
    return &inst->parser;
}

static void cfgm_cml_parser_destroy(cfgm_parser_t *inst, void *priv)
{
    if (inst == NULL) {
        return;
    }
    cfgm_cml_parser_t *mptr = container_of(inst, cfgm_cml_parser_t, parser);
    cfgm_cml_parser_exit(mptr);
    (void)free(mptr);
}

void cfgm_cml_factory_init(cfgm_parser_factory_t *inst)
{
    inst->name = "cfgm-cm-parser";
    inst->priv = NULL;
    inst->create = cfgm_cml_parser_create;
    inst->destroy = cfgm_cml_parser_destroy;
}

void cfgm_cml_factory_exit(cfgm_parser_factory_t *inst)
{
    inst->name = NULL;
    inst->priv = NULL;
    inst->create = NULL;
    inst->destroy = NULL;
}

#ifdef __cplusplus
}
#endif
