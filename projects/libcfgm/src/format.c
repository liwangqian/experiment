/* Copyright (c) 2022-2022, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "libcfgm/format.h"
#include <stddef.h>
#include <string.h>

#ifdef __cplusplus
extern "C" {
#endif

static const struct {
    const char *ext;
    cfgm_format_t type;
} g_format_map[] = {
    { "cml",  CFGM_FORMAT_CML  },
    { "ini",  CFGM_FORMAT_INI  },
    { "xml",  CFGM_FORMAT_XML  },
    { "json", CFGM_FORMAT_JSON },
};

#define FORMAT_MAP_SIZE (sizeof(g_format_map) / sizeof(g_format_map[0]))
#define FORMAT_EXT(i) (g_format_map[i].ext)
#define FORMAT_TYPE(i) (g_format_map[i].type)

cfgm_format_t cfgm_format_from_extension(const char *file_ext)
{
    if (file_ext == NULL) {
        return CFGM_FORMAT_INVALID;
    }
    
    for (int i = 0; i < FORMAT_MAP_SIZE; ++i) {
        if (strcmp(file_ext, FORMAT_EXT(i)) == 0) {
            return FORMAT_TYPE(i);
        }
    }
    return CFGM_FORMAT_INVALID;
}

#ifdef __cplusplus
}
#endif
