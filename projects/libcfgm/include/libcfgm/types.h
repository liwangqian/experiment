/* Copyright (c) 2022-2022, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef LIBCFGM_TYPES_H
#define LIBCFGM_TYPES_H

#ifdef __cplusplus
extern "C" {
#endif

typedef enum cfgm_datatype {
    CFGM_DTYPE_INVALID = 0,
    CFGM_DTYPE_I8,
    CFGM_DTYPE_I16,
    CFGM_DTYPE_I32,
    CFGM_DTYPE_I64,
    CFGM_DTYPE_U8,
    CFGM_DTYPE_U16,
    CFGM_DTYPE_U32,
    CFGM_DTYPE_U64,
    CFGM_DTYPE_FLOAT,
    CFGM_DTYPE_DOUBLE,
    CFGM_DTYPE_BOOLEAN,
    CFGM_DTYPE_STRING,
    CFGM_DTYPE_ARRAY,
    CFGM_DTYPE_STRUCT,
    CFGM_DTYPE_MAX
} cfgm_datatype_t;

int cfgm_datatype_size(cfgm_datatype_t);
const char *cfgm_datatype_desc(cfgm_datatype_t);

#ifdef __cplusplus
}
#endif

#endif /* LIBCFGM_TYPES_H */
