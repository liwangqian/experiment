/* Copyright (c) 2022-2022, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef LIBCFGM_ERROR_H
#define LIBCFGM_ERROR_H

#ifdef __cplusplus
extern "C" {
#endif

typedef enum cfgm_error {
    CFGM_OK,
    CFGM_EPARAM,
    CFGM_EPTR,
    CFGM_EMEM,
    CFGM_EINIT,
    CFGM_EEXIST,
    CFGM_EFILE,
    CFGM_EFORMAT,
    CFGM_ERULE,
    CFGM_EPARSE,
    CFGM_EUNFINISHED_STRING,
    CFGM_EMALFORMED_NUMBER,
    CFGM_EBAD_IDENT,
    CFGM_EBAD_STRING,
    CFGM_ESTMT_IDENT,
    CFGM_ESTMT_UNFINISHED,
} cfgm_error_t;

const char *cfgm_error_desc(cfgm_error_t);

#ifdef __cplusplus
}
#endif

#endif /* LIBCFGM_ERROR_H */
