/* Copyright (c) 2022-2022, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef LIBCFGM_PARSER_H
#define LIBCFGM_PARSER_H

#include "libcfgm/rules.h"
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct cfgm_parser {
    /** 根据规则解析给定的字符串
     * @param self 通过工厂方法创建的parser类（或者子类）的实例
     */
    int (*parse)(struct cfgm_parser *self, const char *cfg, unsigned int len,
        const cfgm_rule_t *rules, unsigned int rules_count);
} cfgm_parser_t;

typedef struct cfgm_parser_factory {
    /* 工厂类的名字 */
    const char *name;

    /* 工厂私有数据 */
    void *priv;

    /* 创建解析器实例 */
    cfgm_parser_t *(*create)(void *priv);

    /* 销毁解析器实例 */
    void (*destroy)(cfgm_parser_t *inst, void *priv);
} cfgm_parser_factory_t;

static inline cfgm_parser_t *cfgm_parser_factory_create(
    const cfgm_parser_factory_t *factory)
{
    return factory && factory->create ?
        factory->create(factory->priv) : NULL;
}

static inline void cfgm_parser_factory_destroy(
    const cfgm_parser_factory_t *factory, cfgm_parser_t *inst)
{
    (factory && factory->destroy) ?
        factory->destroy(inst, factory->priv) : (void)0;
}

static inline bool cfgm_parser_factory_valid(
    const cfgm_parser_factory_t *factory)
{
    return factory && factory->create && factory->destroy;
}

static inline bool cfgm_parser_valid(const cfgm_parser_t *parser)
{
    return parser && parser->parse;
}

static inline int cfgm_parser_parse(cfgm_parser_t *parser,
    const char *content, unsigned int len,
    const cfgm_rule_t *rules, unsigned int rules_count)
{
    return (parser && parser->parse) ?
        parser->parse(parser, content, len, rules, rules_count) : CFGM_EPTR;
}

#ifdef __cplusplus
}
#endif

#endif /* LIBCFGM_PARSER_H */
