/* Copyright (c) 2022-2022, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef LIBCFGM_FILE_H
#define LIBCFGM_FILE_H

#include <stdbool.h>
#include "libcfgm/string.h"

#ifdef __cplusplus
extern "C" {
#endif

#define NULL_EXTENSION ""

/// @brief 检查文件是否存在
/// @param file 文件绝对路径
/// @return 文件存在则返回true，否则返回false
bool cfgm_file_exist(const char *file);

/// @brief 检查文件当前用户权限下是否可读
/// @param file 文件路径
/// @return 可读返回true，否则返回false
bool cfgm_file_readable(const char *file);

/// @brief 检查文件当前用户权限下是否可写
/// @param file 文件路径
/// @return 可写返回true，否则返回false
bool cfgm_file_writable(const char *file);

/// @brief 检查文件当前用户权限下是否可写
/// @param file 文件路径
/// @return 可写返回true，否则返回false
bool cfgm_file_excutable(const char *file);

/// @brief 检查文件当前用户权限下是否可读写
/// @param file 文件路径
/// @return 可读写返回true，否则返回false
bool cfgm_file_rwable(const char *file);

/// @brief 获取文件内容的大小
/// @param file 文件绝对路径
/// @return 成功则返回文件内容字节数，否则返回0
unsigned int cfgm_file_size(const char *file);

/// @brief 从文件名中获取文件扩展名
/// @param file 文件名
/// @return 成功则返回文件扩展名，返回空字符串表示没有扩展名，否则返回NULL
/// @warning 返回的字符串指针指向的file文件字符串的内存
const char *cfgm_file_extension(const char *file);

/// @brief 读取文件内容，最多读取content可容纳的字节数
/// @param file 文件绝对路径
/// @param out 读取的文件内容
/// @return 读取成功则返回CFGM_OK，否则返回错误码
int cfgm_file_read(const char *file, cfgm_string_t *out);

#ifdef __cplusplus
}
#endif

#endif /* LIBCFGM_FILE_H */
