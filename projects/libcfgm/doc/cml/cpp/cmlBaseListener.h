
// Generated from cml.g by ANTLR 4.7.2

#pragma once


#include "antlr4-runtime.h"
#include "cmlListener.h"


/**
 * This class provides an empty implementation of cmlListener,
 * which can be extended to create a listener which only needs to handle a subset
 * of the available methods.
 */
class  cmlBaseListener : public cmlListener {
public:

  virtual void enterCfg(cmlParser::CfgContext * /*ctx*/) override { }
  virtual void exitCfg(cmlParser::CfgContext * /*ctx*/) override { }

  virtual void enterStatementList(cmlParser::StatementListContext * /*ctx*/) override { }
  virtual void exitStatementList(cmlParser::StatementListContext * /*ctx*/) override { }

  virtual void enterStatement(cmlParser::StatementContext * /*ctx*/) override { }
  virtual void exitStatement(cmlParser::StatementContext * /*ctx*/) override { }

  virtual void enterExpression(cmlParser::ExpressionContext * /*ctx*/) override { }
  virtual void exitExpression(cmlParser::ExpressionContext * /*ctx*/) override { }

  virtual void enterExpressionList(cmlParser::ExpressionListContext * /*ctx*/) override { }
  virtual void exitExpressionList(cmlParser::ExpressionListContext * /*ctx*/) override { }

  virtual void enterVectorExpression(cmlParser::VectorExpressionContext * /*ctx*/) override { }
  virtual void exitVectorExpression(cmlParser::VectorExpressionContext * /*ctx*/) override { }

  virtual void enterStructExpression(cmlParser::StructExpressionContext * /*ctx*/) override { }
  virtual void exitStructExpression(cmlParser::StructExpressionContext * /*ctx*/) override { }

  virtual void enterNamedFildlist(cmlParser::NamedFildlistContext * /*ctx*/) override { }
  virtual void exitNamedFildlist(cmlParser::NamedFildlistContext * /*ctx*/) override { }

  virtual void enterNamedFiledExpr(cmlParser::NamedFiledExprContext * /*ctx*/) override { }
  virtual void exitNamedFiledExpr(cmlParser::NamedFiledExprContext * /*ctx*/) override { }


  virtual void enterEveryRule(antlr4::ParserRuleContext * /*ctx*/) override { }
  virtual void exitEveryRule(antlr4::ParserRuleContext * /*ctx*/) override { }
  virtual void visitTerminal(antlr4::tree::TerminalNode * /*node*/) override { }
  virtual void visitErrorNode(antlr4::tree::ErrorNode * /*node*/) override { }

};

