
// Generated from cml.g by ANTLR 4.7.2


#include "cmlListener.h"

#include "cmlParser.h"


using namespace antlrcpp;
using namespace antlr4;

cmlParser::cmlParser(TokenStream *input) : Parser(input) {
  _interpreter = new atn::ParserATNSimulator(this, _atn, _decisionToDFA, _sharedContextCache);
}

cmlParser::~cmlParser() {
  delete _interpreter;
}

std::string cmlParser::getGrammarFileName() const {
  return "cml.g";
}

const std::vector<std::string>& cmlParser::getRuleNames() const {
  return _ruleNames;
}

dfa::Vocabulary& cmlParser::getVocabulary() const {
  return _vocabulary;
}


//----------------- CfgContext ------------------------------------------------------------------

cmlParser::CfgContext::CfgContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

cmlParser::StatementListContext* cmlParser::CfgContext::statementList() {
  return getRuleContext<cmlParser::StatementListContext>(0);
}


size_t cmlParser::CfgContext::getRuleIndex() const {
  return cmlParser::RuleCfg;
}

void cmlParser::CfgContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<cmlListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterCfg(this);
}

void cmlParser::CfgContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<cmlListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitCfg(this);
}

cmlParser::CfgContext* cmlParser::cfg() {
  CfgContext *_localctx = _tracker.createInstance<CfgContext>(_ctx, getState());
  enterRule(_localctx, 0, cmlParser::RuleCfg);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(18);
    statementList();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- StatementListContext ------------------------------------------------------------------

cmlParser::StatementListContext::StatementListContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<cmlParser::StatementContext *> cmlParser::StatementListContext::statement() {
  return getRuleContexts<cmlParser::StatementContext>();
}

cmlParser::StatementContext* cmlParser::StatementListContext::statement(size_t i) {
  return getRuleContext<cmlParser::StatementContext>(i);
}


size_t cmlParser::StatementListContext::getRuleIndex() const {
  return cmlParser::RuleStatementList;
}

void cmlParser::StatementListContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<cmlListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterStatementList(this);
}

void cmlParser::StatementListContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<cmlListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitStatementList(this);
}

cmlParser::StatementListContext* cmlParser::statementList() {
  StatementListContext *_localctx = _tracker.createInstance<StatementListContext>(_ctx, getState());
  enterRule(_localctx, 2, cmlParser::RuleStatementList);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(21); 
    _errHandler->sync(this);
    _la = _input->LA(1);
    do {
      setState(20);
      statement();
      setState(23); 
      _errHandler->sync(this);
      _la = _input->LA(1);
    } while (_la == cmlParser::Identifier);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- StatementContext ------------------------------------------------------------------

cmlParser::StatementContext::StatementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* cmlParser::StatementContext::Identifier() {
  return getToken(cmlParser::Identifier, 0);
}

cmlParser::ExpressionContext* cmlParser::StatementContext::expression() {
  return getRuleContext<cmlParser::ExpressionContext>(0);
}


size_t cmlParser::StatementContext::getRuleIndex() const {
  return cmlParser::RuleStatement;
}

void cmlParser::StatementContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<cmlListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterStatement(this);
}

void cmlParser::StatementContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<cmlListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitStatement(this);
}

cmlParser::StatementContext* cmlParser::statement() {
  StatementContext *_localctx = _tracker.createInstance<StatementContext>(_ctx, getState());
  enterRule(_localctx, 4, cmlParser::RuleStatement);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(25);
    match(cmlParser::Identifier);
    setState(26);
    match(cmlParser::T__0);
    setState(27);
    expression();
    setState(28);
    match(cmlParser::T__1);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ExpressionContext ------------------------------------------------------------------

cmlParser::ExpressionContext::ExpressionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

cmlParser::VectorExpressionContext* cmlParser::ExpressionContext::vectorExpression() {
  return getRuleContext<cmlParser::VectorExpressionContext>(0);
}

cmlParser::StructExpressionContext* cmlParser::ExpressionContext::structExpression() {
  return getRuleContext<cmlParser::StructExpressionContext>(0);
}

tree::TerminalNode* cmlParser::ExpressionContext::Constant() {
  return getToken(cmlParser::Constant, 0);
}


size_t cmlParser::ExpressionContext::getRuleIndex() const {
  return cmlParser::RuleExpression;
}

void cmlParser::ExpressionContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<cmlListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterExpression(this);
}

void cmlParser::ExpressionContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<cmlListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitExpression(this);
}

cmlParser::ExpressionContext* cmlParser::expression() {
  ExpressionContext *_localctx = _tracker.createInstance<ExpressionContext>(_ctx, getState());
  enterRule(_localctx, 6, cmlParser::RuleExpression);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(33);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case cmlParser::T__3: {
        enterOuterAlt(_localctx, 1);
        setState(30);
        vectorExpression();
        break;
      }

      case cmlParser::T__5: {
        enterOuterAlt(_localctx, 2);
        setState(31);
        structExpression();
        break;
      }

      case cmlParser::Constant: {
        enterOuterAlt(_localctx, 3);
        setState(32);
        match(cmlParser::Constant);
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ExpressionListContext ------------------------------------------------------------------

cmlParser::ExpressionListContext::ExpressionListContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<cmlParser::ExpressionContext *> cmlParser::ExpressionListContext::expression() {
  return getRuleContexts<cmlParser::ExpressionContext>();
}

cmlParser::ExpressionContext* cmlParser::ExpressionListContext::expression(size_t i) {
  return getRuleContext<cmlParser::ExpressionContext>(i);
}


size_t cmlParser::ExpressionListContext::getRuleIndex() const {
  return cmlParser::RuleExpressionList;
}

void cmlParser::ExpressionListContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<cmlListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterExpressionList(this);
}

void cmlParser::ExpressionListContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<cmlListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitExpressionList(this);
}

cmlParser::ExpressionListContext* cmlParser::expressionList() {
  ExpressionListContext *_localctx = _tracker.createInstance<ExpressionListContext>(_ctx, getState());
  enterRule(_localctx, 8, cmlParser::RuleExpressionList);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(35);
    expression();
    setState(40);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == cmlParser::T__2) {
      setState(36);
      match(cmlParser::T__2);
      setState(37);
      expression();
      setState(42);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- VectorExpressionContext ------------------------------------------------------------------

cmlParser::VectorExpressionContext::VectorExpressionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

cmlParser::ExpressionListContext* cmlParser::VectorExpressionContext::expressionList() {
  return getRuleContext<cmlParser::ExpressionListContext>(0);
}


size_t cmlParser::VectorExpressionContext::getRuleIndex() const {
  return cmlParser::RuleVectorExpression;
}

void cmlParser::VectorExpressionContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<cmlListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterVectorExpression(this);
}

void cmlParser::VectorExpressionContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<cmlListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitVectorExpression(this);
}

cmlParser::VectorExpressionContext* cmlParser::vectorExpression() {
  VectorExpressionContext *_localctx = _tracker.createInstance<VectorExpressionContext>(_ctx, getState());
  enterRule(_localctx, 10, cmlParser::RuleVectorExpression);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(43);
    match(cmlParser::T__3);
    setState(45);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << cmlParser::T__3)
      | (1ULL << cmlParser::T__5)
      | (1ULL << cmlParser::Constant))) != 0)) {
      setState(44);
      expressionList();
    }
    setState(47);
    match(cmlParser::T__4);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- StructExpressionContext ------------------------------------------------------------------

cmlParser::StructExpressionContext::StructExpressionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

cmlParser::NamedFildlistContext* cmlParser::StructExpressionContext::namedFildlist() {
  return getRuleContext<cmlParser::NamedFildlistContext>(0);
}

cmlParser::ExpressionListContext* cmlParser::StructExpressionContext::expressionList() {
  return getRuleContext<cmlParser::ExpressionListContext>(0);
}


size_t cmlParser::StructExpressionContext::getRuleIndex() const {
  return cmlParser::RuleStructExpression;
}

void cmlParser::StructExpressionContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<cmlListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterStructExpression(this);
}

void cmlParser::StructExpressionContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<cmlListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitStructExpression(this);
}

cmlParser::StructExpressionContext* cmlParser::structExpression() {
  StructExpressionContext *_localctx = _tracker.createInstance<StructExpressionContext>(_ctx, getState());
  enterRule(_localctx, 12, cmlParser::RuleStructExpression);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(59);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 6, _ctx)) {
    case 1: {
      enterOuterAlt(_localctx, 1);
      setState(49);
      match(cmlParser::T__5);
      setState(51);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if (_la == cmlParser::Identifier) {
        setState(50);
        namedFildlist();
      }
      setState(53);
      match(cmlParser::T__6);
      break;
    }

    case 2: {
      enterOuterAlt(_localctx, 2);
      setState(54);
      match(cmlParser::T__5);
      setState(56);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if ((((_la & ~ 0x3fULL) == 0) &&
        ((1ULL << _la) & ((1ULL << cmlParser::T__3)
        | (1ULL << cmlParser::T__5)
        | (1ULL << cmlParser::Constant))) != 0)) {
        setState(55);
        expressionList();
      }
      setState(58);
      match(cmlParser::T__6);
      break;
    }

    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- NamedFildlistContext ------------------------------------------------------------------

cmlParser::NamedFildlistContext::NamedFildlistContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<cmlParser::NamedFiledExprContext *> cmlParser::NamedFildlistContext::namedFiledExpr() {
  return getRuleContexts<cmlParser::NamedFiledExprContext>();
}

cmlParser::NamedFiledExprContext* cmlParser::NamedFildlistContext::namedFiledExpr(size_t i) {
  return getRuleContext<cmlParser::NamedFiledExprContext>(i);
}


size_t cmlParser::NamedFildlistContext::getRuleIndex() const {
  return cmlParser::RuleNamedFildlist;
}

void cmlParser::NamedFildlistContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<cmlListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterNamedFildlist(this);
}

void cmlParser::NamedFildlistContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<cmlListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitNamedFildlist(this);
}

cmlParser::NamedFildlistContext* cmlParser::namedFildlist() {
  NamedFildlistContext *_localctx = _tracker.createInstance<NamedFildlistContext>(_ctx, getState());
  enterRule(_localctx, 14, cmlParser::RuleNamedFildlist);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(61);
    namedFiledExpr();
    setState(66);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == cmlParser::T__2) {
      setState(62);
      match(cmlParser::T__2);
      setState(63);
      namedFiledExpr();
      setState(68);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- NamedFiledExprContext ------------------------------------------------------------------

cmlParser::NamedFiledExprContext::NamedFiledExprContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* cmlParser::NamedFiledExprContext::Identifier() {
  return getToken(cmlParser::Identifier, 0);
}

cmlParser::ExpressionContext* cmlParser::NamedFiledExprContext::expression() {
  return getRuleContext<cmlParser::ExpressionContext>(0);
}


size_t cmlParser::NamedFiledExprContext::getRuleIndex() const {
  return cmlParser::RuleNamedFiledExpr;
}

void cmlParser::NamedFiledExprContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<cmlListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterNamedFiledExpr(this);
}

void cmlParser::NamedFiledExprContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<cmlListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitNamedFiledExpr(this);
}

cmlParser::NamedFiledExprContext* cmlParser::namedFiledExpr() {
  NamedFiledExprContext *_localctx = _tracker.createInstance<NamedFiledExprContext>(_ctx, getState());
  enterRule(_localctx, 16, cmlParser::RuleNamedFiledExpr);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(69);
    match(cmlParser::Identifier);
    setState(70);
    match(cmlParser::T__0);
    setState(71);
    expression();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

// Static vars and initialization.
std::vector<dfa::DFA> cmlParser::_decisionToDFA;
atn::PredictionContextCache cmlParser::_sharedContextCache;

// We own the ATN which in turn owns the ATN states.
atn::ATN cmlParser::_atn;
std::vector<uint16_t> cmlParser::_serializedATN;

std::vector<std::string> cmlParser::_ruleNames = {
  "cfg", "statementList", "statement", "expression", "expressionList", "vectorExpression", 
  "structExpression", "namedFildlist", "namedFiledExpr"
};

std::vector<std::string> cmlParser::_literalNames = {
  "", "'='", "';'", "','", "'['", "']'", "'{'", "'}'"
};

std::vector<std::string> cmlParser::_symbolicNames = {
  "", "", "", "", "", "", "", "", "Identifier", "Constant", "StringLiteral", 
  "SingleLineStringLiteral", "WhiteSpace"
};

dfa::Vocabulary cmlParser::_vocabulary(_literalNames, _symbolicNames);

std::vector<std::string> cmlParser::_tokenNames;

cmlParser::Initializer::Initializer() {
	for (size_t i = 0; i < _symbolicNames.size(); ++i) {
		std::string name = _vocabulary.getLiteralName(i);
		if (name.empty()) {
			name = _vocabulary.getSymbolicName(i);
		}

		if (name.empty()) {
			_tokenNames.push_back("<INVALID>");
		} else {
      _tokenNames.push_back(name);
    }
	}

  _serializedATN = {
    0x3, 0x608b, 0xa72a, 0x8133, 0xb9ed, 0x417c, 0x3be7, 0x7786, 0x5964, 
    0x3, 0xe, 0x4c, 0x4, 0x2, 0x9, 0x2, 0x4, 0x3, 0x9, 0x3, 0x4, 0x4, 0x9, 
    0x4, 0x4, 0x5, 0x9, 0x5, 0x4, 0x6, 0x9, 0x6, 0x4, 0x7, 0x9, 0x7, 0x4, 
    0x8, 0x9, 0x8, 0x4, 0x9, 0x9, 0x9, 0x4, 0xa, 0x9, 0xa, 0x3, 0x2, 0x3, 
    0x2, 0x3, 0x3, 0x6, 0x3, 0x18, 0xa, 0x3, 0xd, 0x3, 0xe, 0x3, 0x19, 0x3, 
    0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x5, 0x3, 0x5, 0x3, 
    0x5, 0x5, 0x5, 0x24, 0xa, 0x5, 0x3, 0x6, 0x3, 0x6, 0x3, 0x6, 0x7, 0x6, 
    0x29, 0xa, 0x6, 0xc, 0x6, 0xe, 0x6, 0x2c, 0xb, 0x6, 0x3, 0x7, 0x3, 0x7, 
    0x5, 0x7, 0x30, 0xa, 0x7, 0x3, 0x7, 0x3, 0x7, 0x3, 0x8, 0x3, 0x8, 0x5, 
    0x8, 0x36, 0xa, 0x8, 0x3, 0x8, 0x3, 0x8, 0x3, 0x8, 0x5, 0x8, 0x3b, 0xa, 
    0x8, 0x3, 0x8, 0x5, 0x8, 0x3e, 0xa, 0x8, 0x3, 0x9, 0x3, 0x9, 0x3, 0x9, 
    0x7, 0x9, 0x43, 0xa, 0x9, 0xc, 0x9, 0xe, 0x9, 0x46, 0xb, 0x9, 0x3, 0xa, 
    0x3, 0xa, 0x3, 0xa, 0x3, 0xa, 0x3, 0xa, 0x2, 0x2, 0xb, 0x2, 0x4, 0x6, 
    0x8, 0xa, 0xc, 0xe, 0x10, 0x12, 0x2, 0x2, 0x2, 0x4b, 0x2, 0x14, 0x3, 
    0x2, 0x2, 0x2, 0x4, 0x17, 0x3, 0x2, 0x2, 0x2, 0x6, 0x1b, 0x3, 0x2, 0x2, 
    0x2, 0x8, 0x23, 0x3, 0x2, 0x2, 0x2, 0xa, 0x25, 0x3, 0x2, 0x2, 0x2, 0xc, 
    0x2d, 0x3, 0x2, 0x2, 0x2, 0xe, 0x3d, 0x3, 0x2, 0x2, 0x2, 0x10, 0x3f, 
    0x3, 0x2, 0x2, 0x2, 0x12, 0x47, 0x3, 0x2, 0x2, 0x2, 0x14, 0x15, 0x5, 
    0x4, 0x3, 0x2, 0x15, 0x3, 0x3, 0x2, 0x2, 0x2, 0x16, 0x18, 0x5, 0x6, 
    0x4, 0x2, 0x17, 0x16, 0x3, 0x2, 0x2, 0x2, 0x18, 0x19, 0x3, 0x2, 0x2, 
    0x2, 0x19, 0x17, 0x3, 0x2, 0x2, 0x2, 0x19, 0x1a, 0x3, 0x2, 0x2, 0x2, 
    0x1a, 0x5, 0x3, 0x2, 0x2, 0x2, 0x1b, 0x1c, 0x7, 0xa, 0x2, 0x2, 0x1c, 
    0x1d, 0x7, 0x3, 0x2, 0x2, 0x1d, 0x1e, 0x5, 0x8, 0x5, 0x2, 0x1e, 0x1f, 
    0x7, 0x4, 0x2, 0x2, 0x1f, 0x7, 0x3, 0x2, 0x2, 0x2, 0x20, 0x24, 0x5, 
    0xc, 0x7, 0x2, 0x21, 0x24, 0x5, 0xe, 0x8, 0x2, 0x22, 0x24, 0x7, 0xb, 
    0x2, 0x2, 0x23, 0x20, 0x3, 0x2, 0x2, 0x2, 0x23, 0x21, 0x3, 0x2, 0x2, 
    0x2, 0x23, 0x22, 0x3, 0x2, 0x2, 0x2, 0x24, 0x9, 0x3, 0x2, 0x2, 0x2, 
    0x25, 0x2a, 0x5, 0x8, 0x5, 0x2, 0x26, 0x27, 0x7, 0x5, 0x2, 0x2, 0x27, 
    0x29, 0x5, 0x8, 0x5, 0x2, 0x28, 0x26, 0x3, 0x2, 0x2, 0x2, 0x29, 0x2c, 
    0x3, 0x2, 0x2, 0x2, 0x2a, 0x28, 0x3, 0x2, 0x2, 0x2, 0x2a, 0x2b, 0x3, 
    0x2, 0x2, 0x2, 0x2b, 0xb, 0x3, 0x2, 0x2, 0x2, 0x2c, 0x2a, 0x3, 0x2, 
    0x2, 0x2, 0x2d, 0x2f, 0x7, 0x6, 0x2, 0x2, 0x2e, 0x30, 0x5, 0xa, 0x6, 
    0x2, 0x2f, 0x2e, 0x3, 0x2, 0x2, 0x2, 0x2f, 0x30, 0x3, 0x2, 0x2, 0x2, 
    0x30, 0x31, 0x3, 0x2, 0x2, 0x2, 0x31, 0x32, 0x7, 0x7, 0x2, 0x2, 0x32, 
    0xd, 0x3, 0x2, 0x2, 0x2, 0x33, 0x35, 0x7, 0x8, 0x2, 0x2, 0x34, 0x36, 
    0x5, 0x10, 0x9, 0x2, 0x35, 0x34, 0x3, 0x2, 0x2, 0x2, 0x35, 0x36, 0x3, 
    0x2, 0x2, 0x2, 0x36, 0x37, 0x3, 0x2, 0x2, 0x2, 0x37, 0x3e, 0x7, 0x9, 
    0x2, 0x2, 0x38, 0x3a, 0x7, 0x8, 0x2, 0x2, 0x39, 0x3b, 0x5, 0xa, 0x6, 
    0x2, 0x3a, 0x39, 0x3, 0x2, 0x2, 0x2, 0x3a, 0x3b, 0x3, 0x2, 0x2, 0x2, 
    0x3b, 0x3c, 0x3, 0x2, 0x2, 0x2, 0x3c, 0x3e, 0x7, 0x9, 0x2, 0x2, 0x3d, 
    0x33, 0x3, 0x2, 0x2, 0x2, 0x3d, 0x38, 0x3, 0x2, 0x2, 0x2, 0x3e, 0xf, 
    0x3, 0x2, 0x2, 0x2, 0x3f, 0x44, 0x5, 0x12, 0xa, 0x2, 0x40, 0x41, 0x7, 
    0x5, 0x2, 0x2, 0x41, 0x43, 0x5, 0x12, 0xa, 0x2, 0x42, 0x40, 0x3, 0x2, 
    0x2, 0x2, 0x43, 0x46, 0x3, 0x2, 0x2, 0x2, 0x44, 0x42, 0x3, 0x2, 0x2, 
    0x2, 0x44, 0x45, 0x3, 0x2, 0x2, 0x2, 0x45, 0x11, 0x3, 0x2, 0x2, 0x2, 
    0x46, 0x44, 0x3, 0x2, 0x2, 0x2, 0x47, 0x48, 0x7, 0xa, 0x2, 0x2, 0x48, 
    0x49, 0x7, 0x3, 0x2, 0x2, 0x49, 0x4a, 0x5, 0x8, 0x5, 0x2, 0x4a, 0x13, 
    0x3, 0x2, 0x2, 0x2, 0xa, 0x19, 0x23, 0x2a, 0x2f, 0x35, 0x3a, 0x3d, 0x44, 
  };

  atn::ATNDeserializer deserializer;
  _atn = deserializer.deserialize(_serializedATN);

  size_t count = _atn.getNumberOfDecisions();
  _decisionToDFA.reserve(count);
  for (size_t i = 0; i < count; i++) { 
    _decisionToDFA.emplace_back(_atn.getDecisionState(i), i);
  }
}

cmlParser::Initializer cmlParser::_init;
