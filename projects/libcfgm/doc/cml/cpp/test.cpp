#include "cmlParser.h"
#include "cmlLexer.h"
#include "cmlBaseListener.h"
#include <ANTLRFileStream.h>
#include <iostream>

static inline void out(const std::string &text)
{
  std::cout << text << std::endl;
}
/**
 * This class provides an empty implementation of cmlListener,
 * which can be extended to create a listener which only needs to handle a subset
 * of the available methods.
 */
class  cmlWalkListener : public cmlBaseListener {
public:

  virtual void enterCfg(cmlParser::CfgContext * ctx) override
  {
    out("enter cfg");
    out(ctx->getText());
  }
  virtual void exitCfg(cmlParser::CfgContext * ctx) override
  {
    out("exit cfg");
  }

  virtual void enterStatementList(cmlParser::StatementListContext * ctx) override
  {
    out("enter statementlist");
  }
  virtual void exitStatementList(cmlParser::StatementListContext * ctx) override
  {
    for (auto i = 0; i < ctx->children.size(); ++i)
      out(ctx->children[i]->getText());
    out("exit statementlist");
  }

  virtual void enterStatement(cmlParser::StatementContext * ctx) override
  {
    out("enter statement");
  }
  virtual void exitStatement(cmlParser::StatementContext * ctx) override
  {
    for (auto i = 0; i < ctx->children.size(); ++i)
      out(ctx->children[i]->getText());
    out("exit statement");
  }

  virtual void enterExpression(cmlParser::ExpressionContext * ctx) override
  {
    out("enter expression");
  }
  virtual void exitExpression(cmlParser::ExpressionContext * ctx) override
  {
    for (auto i = 0; i < ctx->children.size(); ++i) {
      out(ctx->children[i]->getText());
    }
    out("exit expression");
  }

  virtual void enterExpressionList(cmlParser::ExpressionListContext * ctx) override
  {
    out("enter expressionlist");
  }
  virtual void exitExpressionList(cmlParser::ExpressionListContext * ctx) override
  {
    for (auto i = 0; i < ctx->children.size(); ++i)
      out(ctx->children[i]->getText());
    out("exit expressionlist");
  }

  virtual void enterVectorExpression(cmlParser::VectorExpressionContext * ctx) override
  {
    out("enter vec expression");
  }
  virtual void exitVectorExpression(cmlParser::VectorExpressionContext * ctx) override
  {
    for (auto i = 0; i < ctx->children.size(); ++i)
      out(ctx->children[i]->getText());
    out("exit vec expression");
  }
};


int main(int argc, char *argv[])
{
    auto input = new antlr4::ANTLRFileStream();
    input->loadFromFile("./config.cml");
    auto lexer = new cmlLexer(input);
    auto tokens = new antlr4::CommonTokenStream(lexer);
    auto parser = new cmlParser(tokens);
    auto listener = new cmlWalkListener();
    parser->addParseListener(listener);
    parser->cfg();
    return 0;
}