
// Generated from cml.g by ANTLR 4.7.2

#pragma once


#include "antlr4-runtime.h"
#include "cmlParser.h"


/**
 * This interface defines an abstract listener for a parse tree produced by cmlParser.
 */
class  cmlListener : public antlr4::tree::ParseTreeListener {
public:

  virtual void enterCfg(cmlParser::CfgContext *ctx) = 0;
  virtual void exitCfg(cmlParser::CfgContext *ctx) = 0;

  virtual void enterStatementList(cmlParser::StatementListContext *ctx) = 0;
  virtual void exitStatementList(cmlParser::StatementListContext *ctx) = 0;

  virtual void enterStatement(cmlParser::StatementContext *ctx) = 0;
  virtual void exitStatement(cmlParser::StatementContext *ctx) = 0;

  virtual void enterExpression(cmlParser::ExpressionContext *ctx) = 0;
  virtual void exitExpression(cmlParser::ExpressionContext *ctx) = 0;

  virtual void enterExpressionList(cmlParser::ExpressionListContext *ctx) = 0;
  virtual void exitExpressionList(cmlParser::ExpressionListContext *ctx) = 0;

  virtual void enterVectorExpression(cmlParser::VectorExpressionContext *ctx) = 0;
  virtual void exitVectorExpression(cmlParser::VectorExpressionContext *ctx) = 0;

  virtual void enterStructExpression(cmlParser::StructExpressionContext *ctx) = 0;
  virtual void exitStructExpression(cmlParser::StructExpressionContext *ctx) = 0;

  virtual void enterNamedFildlist(cmlParser::NamedFildlistContext *ctx) = 0;
  virtual void exitNamedFildlist(cmlParser::NamedFildlistContext *ctx) = 0;

  virtual void enterNamedFiledExpr(cmlParser::NamedFiledExprContext *ctx) = 0;
  virtual void exitNamedFiledExpr(cmlParser::NamedFiledExprContext *ctx) = 0;


};

