// Generated from /mnt/d/Works/Gitee/experiment/projects/libcfgm/doc/cml/cml.g by ANTLR 4.9.2
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class cmlLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.9.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, Identifier=8, 
		Constant=9, StringLiteral=10, SingleLineStringLiteral=11, WhiteSpace=12;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	private static String[] makeRuleNames() {
		return new String[] {
			"T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "T__6", "Identifier", 
			"Constant", "SCharSequence", "SChar", "EscapeSequence", "IntegerSuffix", 
			"FloatingSuffix", "Sign", "Digit", "Nondigit", "NonzeroDigit", "OctalDigit", 
			"DigitSequence", "ExponentPart", "HexadecimalPrefix", "HexadecimalDigit", 
			"HexadecimalConstant", "BinaryConstant", "DecimalConstant", "OctalConstant", 
			"IntegerConstant", "BooleanConstant", "FloatingConstant", "DecimalFloatingConstant", 
			"FractionalConstant", "StringLiteral", "SingleLineStringLiteral", "WhiteSpace"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'='", "';'", "','", "'['", "']'", "'{'", "'}'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, null, null, null, null, null, null, "Identifier", "Constant", 
			"StringLiteral", "SingleLineStringLiteral", "WhiteSpace"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public cmlLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "cml.g"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\16\u0107\b\1\4\2"+
		"\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4"+
		"\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22"+
		"\t\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31"+
		"\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t"+
		" \4!\t!\4\"\t\"\4#\t#\4$\t$\3\2\3\2\3\3\3\3\3\4\3\4\3\5\3\5\3\6\3\6\3"+
		"\7\3\7\3\b\3\b\3\t\3\t\3\t\7\t[\n\t\f\t\16\t^\13\t\3\n\3\n\3\n\3\n\5\n"+
		"d\n\n\3\13\6\13g\n\13\r\13\16\13h\3\f\3\f\5\fm\n\f\3\r\3\r\3\r\3\16\3"+
		"\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3"+
		"\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\5\16\u0089\n\16\3\17\3\17\3\17"+
		"\5\17\u008e\n\17\3\20\3\20\3\21\3\21\3\22\3\22\3\23\3\23\3\24\3\24\3\25"+
		"\6\25\u009b\n\25\r\25\16\25\u009c\3\26\3\26\5\26\u00a1\n\26\3\26\3\26"+
		"\3\27\3\27\3\27\3\30\3\30\3\31\3\31\6\31\u00ac\n\31\r\31\16\31\u00ad\3"+
		"\32\3\32\3\32\6\32\u00b3\n\32\r\32\16\32\u00b4\3\33\3\33\7\33\u00b9\n"+
		"\33\f\33\16\33\u00bc\13\33\3\34\3\34\7\34\u00c0\n\34\f\34\16\34\u00c3"+
		"\13\34\3\35\3\35\5\35\u00c7\n\35\3\35\3\35\5\35\u00cb\n\35\3\35\3\35\5"+
		"\35\u00cf\n\35\3\35\5\35\u00d2\n\35\3\36\3\36\3\36\3\36\3\36\3\36\3\36"+
		"\3\36\3\36\5\36\u00dd\n\36\3\37\3\37\3 \3 \5 \u00e3\n \3 \5 \u00e6\n "+
		"\3 \3 \3 \5 \u00eb\n \5 \u00ed\n \3!\5!\u00f0\n!\3!\3!\3!\3!\3!\5!\u00f7"+
		"\n!\3\"\3\"\3#\3#\5#\u00fd\n#\3#\3#\3$\6$\u0102\n$\r$\16$\u0103\3$\3$"+
		"\2\2%\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\2\27\2\31\2\33\2\35"+
		"\2\37\2!\2#\2%\2\'\2)\2+\2-\2/\2\61\2\63\2\65\2\67\29\2;\2=\2?\2A\2C\f"+
		"E\rG\16\3\2\17\6\2\f\f\17\17$$^^\f\2$$))AA^^cdhhppttvvxx\4\2--//\3\2\62"+
		";\5\2C\\aac|\3\2\63;\3\2\629\4\2GGgg\4\2ZZzz\5\2\62;CHch\4\2DDdd\3\2\62"+
		"\63\5\2\13\f\17\17\"\"\2\u0114\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t"+
		"\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2"+
		"\2\2C\3\2\2\2\2E\3\2\2\2\2G\3\2\2\2\3I\3\2\2\2\5K\3\2\2\2\7M\3\2\2\2\t"+
		"O\3\2\2\2\13Q\3\2\2\2\rS\3\2\2\2\17U\3\2\2\2\21W\3\2\2\2\23c\3\2\2\2\25"+
		"f\3\2\2\2\27l\3\2\2\2\31n\3\2\2\2\33\u0088\3\2\2\2\35\u008d\3\2\2\2\37"+
		"\u008f\3\2\2\2!\u0091\3\2\2\2#\u0093\3\2\2\2%\u0095\3\2\2\2\'\u0097\3"+
		"\2\2\2)\u009a\3\2\2\2+\u009e\3\2\2\2-\u00a4\3\2\2\2/\u00a7\3\2\2\2\61"+
		"\u00a9\3\2\2\2\63\u00af\3\2\2\2\65\u00b6\3\2\2\2\67\u00bd\3\2\2\29\u00d1"+
		"\3\2\2\2;\u00dc\3\2\2\2=\u00de\3\2\2\2?\u00ec\3\2\2\2A\u00f6\3\2\2\2C"+
		"\u00f8\3\2\2\2E\u00fa\3\2\2\2G\u0101\3\2\2\2IJ\7?\2\2J\4\3\2\2\2KL\7="+
		"\2\2L\6\3\2\2\2MN\7.\2\2N\b\3\2\2\2OP\7]\2\2P\n\3\2\2\2QR\7_\2\2R\f\3"+
		"\2\2\2ST\7}\2\2T\16\3\2\2\2UV\7\177\2\2V\20\3\2\2\2W\\\5#\22\2X[\5#\22"+
		"\2Y[\5!\21\2ZX\3\2\2\2ZY\3\2\2\2[^\3\2\2\2\\Z\3\2\2\2\\]\3\2\2\2]\22\3"+
		"\2\2\2^\\\3\2\2\2_d\59\35\2`d\5=\37\2ad\5;\36\2bd\5C\"\2c_\3\2\2\2c`\3"+
		"\2\2\2ca\3\2\2\2cb\3\2\2\2d\24\3\2\2\2eg\5\27\f\2fe\3\2\2\2gh\3\2\2\2"+
		"hf\3\2\2\2hi\3\2\2\2i\26\3\2\2\2jm\n\2\2\2km\5\31\r\2lj\3\2\2\2lk\3\2"+
		"\2\2m\30\3\2\2\2no\7^\2\2op\t\3\2\2p\32\3\2\2\2qr\7k\2\2r\u0089\7:\2\2"+
		"st\7k\2\2tu\7\63\2\2u\u0089\78\2\2vw\7k\2\2wx\7\65\2\2x\u0089\7\64\2\2"+
		"yz\7k\2\2z{\78\2\2{\u0089\7\66\2\2|}\7w\2\2}\u0089\7:\2\2~\177\7w\2\2"+
		"\177\u0080\7\63\2\2\u0080\u0089\78\2\2\u0081\u0082\7w\2\2\u0082\u0083"+
		"\7\65\2\2\u0083\u0089\7\64\2\2\u0084\u0085\7w\2\2\u0085\u0086\78\2\2\u0086"+
		"\u0089\7\66\2\2\u0087\u0089\7w\2\2\u0088q\3\2\2\2\u0088s\3\2\2\2\u0088"+
		"v\3\2\2\2\u0088y\3\2\2\2\u0088|\3\2\2\2\u0088~\3\2\2\2\u0088\u0081\3\2"+
		"\2\2\u0088\u0084\3\2\2\2\u0088\u0087\3\2\2\2\u0089\34\3\2\2\2\u008a\u008e"+
		"\7h\2\2\u008b\u008c\7n\2\2\u008c\u008e\7h\2\2\u008d\u008a\3\2\2\2\u008d"+
		"\u008b\3\2\2\2\u008e\36\3\2\2\2\u008f\u0090\t\4\2\2\u0090 \3\2\2\2\u0091"+
		"\u0092\t\5\2\2\u0092\"\3\2\2\2\u0093\u0094\t\6\2\2\u0094$\3\2\2\2\u0095"+
		"\u0096\t\7\2\2\u0096&\3\2\2\2\u0097\u0098\t\b\2\2\u0098(\3\2\2\2\u0099"+
		"\u009b\5!\21\2\u009a\u0099\3\2\2\2\u009b\u009c\3\2\2\2\u009c\u009a\3\2"+
		"\2\2\u009c\u009d\3\2\2\2\u009d*\3\2\2\2\u009e\u00a0\t\t\2\2\u009f\u00a1"+
		"\5\37\20\2\u00a0\u009f\3\2\2\2\u00a0\u00a1\3\2\2\2\u00a1\u00a2\3\2\2\2"+
		"\u00a2\u00a3\5)\25\2\u00a3,\3\2\2\2\u00a4\u00a5\7\62\2\2\u00a5\u00a6\t"+
		"\n\2\2\u00a6.\3\2\2\2\u00a7\u00a8\t\13\2\2\u00a8\60\3\2\2\2\u00a9\u00ab"+
		"\5-\27\2\u00aa\u00ac\5/\30\2\u00ab\u00aa\3\2\2\2\u00ac\u00ad\3\2\2\2\u00ad"+
		"\u00ab\3\2\2\2\u00ad\u00ae\3\2\2\2\u00ae\62\3\2\2\2\u00af\u00b0\7\62\2"+
		"\2\u00b0\u00b2\t\f\2\2\u00b1\u00b3\t\r\2\2\u00b2\u00b1\3\2\2\2\u00b3\u00b4"+
		"\3\2\2\2\u00b4\u00b2\3\2\2\2\u00b4\u00b5\3\2\2\2\u00b5\64\3\2\2\2\u00b6"+
		"\u00ba\5%\23\2\u00b7\u00b9\5!\21\2\u00b8\u00b7\3\2\2\2\u00b9\u00bc\3\2"+
		"\2\2\u00ba\u00b8\3\2\2\2\u00ba\u00bb\3\2\2\2\u00bb\66\3\2\2\2\u00bc\u00ba"+
		"\3\2\2\2\u00bd\u00c1\7\62\2\2\u00be\u00c0\5\'\24\2\u00bf\u00be\3\2\2\2"+
		"\u00c0\u00c3\3\2\2\2\u00c1\u00bf\3\2\2\2\u00c1\u00c2\3\2\2\2\u00c28\3"+
		"\2\2\2\u00c3\u00c1\3\2\2\2\u00c4\u00c6\5\65\33\2\u00c5\u00c7\5\33\16\2"+
		"\u00c6\u00c5\3\2\2\2\u00c6\u00c7\3\2\2\2\u00c7\u00d2\3\2\2\2\u00c8\u00ca"+
		"\5\67\34\2\u00c9\u00cb\5\33\16\2\u00ca\u00c9\3\2\2\2\u00ca\u00cb\3\2\2"+
		"\2\u00cb\u00d2\3\2\2\2\u00cc\u00ce\5\61\31\2\u00cd\u00cf\5\33\16\2\u00ce"+
		"\u00cd\3\2\2\2\u00ce\u00cf\3\2\2\2\u00cf\u00d2\3\2\2\2\u00d0\u00d2\5\63"+
		"\32\2\u00d1\u00c4\3\2\2\2\u00d1\u00c8\3\2\2\2\u00d1\u00cc\3\2\2\2\u00d1"+
		"\u00d0\3\2\2\2\u00d2:\3\2\2\2\u00d3\u00d4\7v\2\2\u00d4\u00d5\7t\2\2\u00d5"+
		"\u00d6\7w\2\2\u00d6\u00dd\7g\2\2\u00d7\u00d8\7h\2\2\u00d8\u00d9\7c\2\2"+
		"\u00d9\u00da\7n\2\2\u00da\u00db\7u\2\2\u00db\u00dd\7g\2\2\u00dc\u00d3"+
		"\3\2\2\2\u00dc\u00d7\3\2\2\2\u00dd<\3\2\2\2\u00de\u00df\5? \2\u00df>\3"+
		"\2\2\2\u00e0\u00e2\5A!\2\u00e1\u00e3\5+\26\2\u00e2\u00e1\3\2\2\2\u00e2"+
		"\u00e3\3\2\2\2\u00e3\u00e5\3\2\2\2\u00e4\u00e6\5\35\17\2\u00e5\u00e4\3"+
		"\2\2\2\u00e5\u00e6\3\2\2\2\u00e6\u00ed\3\2\2\2\u00e7\u00e8\5)\25\2\u00e8"+
		"\u00ea\5+\26\2\u00e9\u00eb\5\35\17\2\u00ea\u00e9\3\2\2\2\u00ea\u00eb\3"+
		"\2\2\2\u00eb\u00ed\3\2\2\2\u00ec\u00e0\3\2\2\2\u00ec\u00e7\3\2\2\2\u00ed"+
		"@\3\2\2\2\u00ee\u00f0\5)\25\2\u00ef\u00ee\3\2\2\2\u00ef\u00f0\3\2\2\2"+
		"\u00f0\u00f1\3\2\2\2\u00f1\u00f2\7\60\2\2\u00f2\u00f7\5)\25\2\u00f3\u00f4"+
		"\5)\25\2\u00f4\u00f5\7\60\2\2\u00f5\u00f7\3\2\2\2\u00f6\u00ef\3\2\2\2"+
		"\u00f6\u00f3\3\2\2\2\u00f7B\3\2\2\2\u00f8\u00f9\5E#\2\u00f9D\3\2\2\2\u00fa"+
		"\u00fc\7$\2\2\u00fb\u00fd\5\25\13\2\u00fc\u00fb\3\2\2\2\u00fc\u00fd\3"+
		"\2\2\2\u00fd\u00fe\3\2\2\2\u00fe\u00ff\7$\2\2\u00ffF\3\2\2\2\u0100\u0102"+
		"\t\16\2\2\u0101\u0100\3\2\2\2\u0102\u0103\3\2\2\2\u0103\u0101\3\2\2\2"+
		"\u0103\u0104\3\2\2\2\u0104\u0105\3\2\2\2\u0105\u0106\b$\2\2\u0106H\3\2"+
		"\2\2\35\2Z\\chl\u0088\u008d\u009c\u00a0\u00ad\u00b4\u00ba\u00c1\u00c6"+
		"\u00ca\u00ce\u00d1\u00dc\u00e2\u00e5\u00ea\u00ec\u00ef\u00f6\u00fc\u0103"+
		"\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}