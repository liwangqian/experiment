// Generated from /mnt/d/Works/Gitee/experiment/projects/libcfgm/doc/cml/cml.g by ANTLR 4.9.2
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class cmlParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.9.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, Identifier=8, 
		Constant=9, StringLiteral=10, SingleLineStringLiteral=11, WhiteSpace=12;
	public static final int
		RULE_cfg = 0, RULE_statement_list = 1, RULE_assigned_statement = 2, RULE_expression = 3, 
		RULE_expression_list = 4, RULE_vector_expression = 5, RULE_struct_expression = 6, 
		RULE_named_fild_list = 7, RULE_named_filed_expression = 8;
	private static String[] makeRuleNames() {
		return new String[] {
			"cfg", "statement_list", "assigned_statement", "expression", "expression_list", 
			"vector_expression", "struct_expression", "named_fild_list", "named_filed_expression"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'='", "';'", "','", "'['", "']'", "'{'", "'}'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, null, null, null, null, null, null, "Identifier", "Constant", 
			"StringLiteral", "SingleLineStringLiteral", "WhiteSpace"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "cml.g"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public cmlParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class CfgContext extends ParserRuleContext {
		public Statement_listContext statement_list() {
			return getRuleContext(Statement_listContext.class,0);
		}
		public CfgContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_cfg; }
	}

	public final CfgContext cfg() throws RecognitionException {
		CfgContext _localctx = new CfgContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_cfg);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(18);
			statement_list();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Statement_listContext extends ParserRuleContext {
		public List<Assigned_statementContext> assigned_statement() {
			return getRuleContexts(Assigned_statementContext.class);
		}
		public Assigned_statementContext assigned_statement(int i) {
			return getRuleContext(Assigned_statementContext.class,i);
		}
		public Statement_listContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement_list; }
	}

	public final Statement_listContext statement_list() throws RecognitionException {
		Statement_listContext _localctx = new Statement_listContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_statement_list);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(21); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(20);
				assigned_statement();
				}
				}
				setState(23); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==Identifier );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Assigned_statementContext extends ParserRuleContext {
		public TerminalNode Identifier() { return getToken(cmlParser.Identifier, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public Assigned_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assigned_statement; }
	}

	public final Assigned_statementContext assigned_statement() throws RecognitionException {
		Assigned_statementContext _localctx = new Assigned_statementContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_assigned_statement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(25);
			match(Identifier);
			setState(26);
			match(T__0);
			setState(27);
			expression();
			setState(28);
			match(T__1);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public Vector_expressionContext vector_expression() {
			return getRuleContext(Vector_expressionContext.class,0);
		}
		public Struct_expressionContext struct_expression() {
			return getRuleContext(Struct_expressionContext.class,0);
		}
		public TerminalNode Constant() { return getToken(cmlParser.Constant, 0); }
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
	}

	public final ExpressionContext expression() throws RecognitionException {
		ExpressionContext _localctx = new ExpressionContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_expression);
		try {
			setState(33);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__3:
				enterOuterAlt(_localctx, 1);
				{
				setState(30);
				vector_expression();
				}
				break;
			case T__5:
				enterOuterAlt(_localctx, 2);
				{
				setState(31);
				struct_expression();
				}
				break;
			case Constant:
				enterOuterAlt(_localctx, 3);
				{
				setState(32);
				match(Constant);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Expression_listContext extends ParserRuleContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public Expression_listContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression_list; }
	}

	public final Expression_listContext expression_list() throws RecognitionException {
		Expression_listContext _localctx = new Expression_listContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_expression_list);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(35);
			expression();
			setState(40);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__2) {
				{
				{
				setState(36);
				match(T__2);
				setState(37);
				expression();
				}
				}
				setState(42);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Vector_expressionContext extends ParserRuleContext {
		public Expression_listContext expression_list() {
			return getRuleContext(Expression_listContext.class,0);
		}
		public Vector_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_vector_expression; }
	}

	public final Vector_expressionContext vector_expression() throws RecognitionException {
		Vector_expressionContext _localctx = new Vector_expressionContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_vector_expression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(43);
			match(T__3);
			setState(45);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__3) | (1L << T__5) | (1L << Constant))) != 0)) {
				{
				setState(44);
				expression_list();
				}
			}

			setState(47);
			match(T__4);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Struct_expressionContext extends ParserRuleContext {
		public Named_fild_listContext named_fild_list() {
			return getRuleContext(Named_fild_listContext.class,0);
		}
		public Expression_listContext expression_list() {
			return getRuleContext(Expression_listContext.class,0);
		}
		public Struct_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_struct_expression; }
	}

	public final Struct_expressionContext struct_expression() throws RecognitionException {
		Struct_expressionContext _localctx = new Struct_expressionContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_struct_expression);
		int _la;
		try {
			setState(59);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,6,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(49);
				match(T__5);
				setState(51);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==Identifier) {
					{
					setState(50);
					named_fild_list();
					}
				}

				setState(53);
				match(T__6);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(54);
				match(T__5);
				setState(56);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__3) | (1L << T__5) | (1L << Constant))) != 0)) {
					{
					setState(55);
					expression_list();
					}
				}

				setState(58);
				match(T__6);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Named_fild_listContext extends ParserRuleContext {
		public List<Named_filed_expressionContext> named_filed_expression() {
			return getRuleContexts(Named_filed_expressionContext.class);
		}
		public Named_filed_expressionContext named_filed_expression(int i) {
			return getRuleContext(Named_filed_expressionContext.class,i);
		}
		public Named_fild_listContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_named_fild_list; }
	}

	public final Named_fild_listContext named_fild_list() throws RecognitionException {
		Named_fild_listContext _localctx = new Named_fild_listContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_named_fild_list);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(61);
			named_filed_expression();
			setState(66);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__2) {
				{
				{
				setState(62);
				match(T__2);
				setState(63);
				named_filed_expression();
				}
				}
				setState(68);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Named_filed_expressionContext extends ParserRuleContext {
		public TerminalNode Identifier() { return getToken(cmlParser.Identifier, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public Named_filed_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_named_filed_expression; }
	}

	public final Named_filed_expressionContext named_filed_expression() throws RecognitionException {
		Named_filed_expressionContext _localctx = new Named_filed_expressionContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_named_filed_expression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(69);
			match(Identifier);
			setState(70);
			match(T__0);
			setState(71);
			expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\16L\4\2\t\2\4\3\t"+
		"\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\3\2\3\2\3\3"+
		"\6\3\30\n\3\r\3\16\3\31\3\4\3\4\3\4\3\4\3\4\3\5\3\5\3\5\5\5$\n\5\3\6\3"+
		"\6\3\6\7\6)\n\6\f\6\16\6,\13\6\3\7\3\7\5\7\60\n\7\3\7\3\7\3\b\3\b\5\b"+
		"\66\n\b\3\b\3\b\3\b\5\b;\n\b\3\b\5\b>\n\b\3\t\3\t\3\t\7\tC\n\t\f\t\16"+
		"\tF\13\t\3\n\3\n\3\n\3\n\3\n\2\2\13\2\4\6\b\n\f\16\20\22\2\2\2K\2\24\3"+
		"\2\2\2\4\27\3\2\2\2\6\33\3\2\2\2\b#\3\2\2\2\n%\3\2\2\2\f-\3\2\2\2\16="+
		"\3\2\2\2\20?\3\2\2\2\22G\3\2\2\2\24\25\5\4\3\2\25\3\3\2\2\2\26\30\5\6"+
		"\4\2\27\26\3\2\2\2\30\31\3\2\2\2\31\27\3\2\2\2\31\32\3\2\2\2\32\5\3\2"+
		"\2\2\33\34\7\n\2\2\34\35\7\3\2\2\35\36\5\b\5\2\36\37\7\4\2\2\37\7\3\2"+
		"\2\2 $\5\f\7\2!$\5\16\b\2\"$\7\13\2\2# \3\2\2\2#!\3\2\2\2#\"\3\2\2\2$"+
		"\t\3\2\2\2%*\5\b\5\2&\'\7\5\2\2\')\5\b\5\2(&\3\2\2\2),\3\2\2\2*(\3\2\2"+
		"\2*+\3\2\2\2+\13\3\2\2\2,*\3\2\2\2-/\7\6\2\2.\60\5\n\6\2/.\3\2\2\2/\60"+
		"\3\2\2\2\60\61\3\2\2\2\61\62\7\7\2\2\62\r\3\2\2\2\63\65\7\b\2\2\64\66"+
		"\5\20\t\2\65\64\3\2\2\2\65\66\3\2\2\2\66\67\3\2\2\2\67>\7\t\2\28:\7\b"+
		"\2\29;\5\n\6\2:9\3\2\2\2:;\3\2\2\2;<\3\2\2\2<>\7\t\2\2=\63\3\2\2\2=8\3"+
		"\2\2\2>\17\3\2\2\2?D\5\22\n\2@A\7\5\2\2AC\5\22\n\2B@\3\2\2\2CF\3\2\2\2"+
		"DB\3\2\2\2DE\3\2\2\2E\21\3\2\2\2FD\3\2\2\2GH\7\n\2\2HI\7\3\2\2IJ\5\b\5"+
		"\2J\23\3\2\2\2\n\31#*/\65:=D";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}