#include "solution.h"
#include <cassert>

int main(int argc, char *argv[]) {
    vector<vector<int>> input = {{0,1},{1,0}};
    Solution solution;
    cout << solution.countServers(input) << endl;

    input = {{0,1},{1,1}};
    cout << solution.countServers(input) << endl;

    input = {{1,1,0,0},{0,0,1,0},{0,0,1,0},{0,0,0,1}};
    cout << solution.countServers(input) << endl;
}
