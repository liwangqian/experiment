#ifndef SOLUTION_H_INCLUDED
#define SOLUTION_H_INCLUDED

#include <vector>
#include <iostream>
#include <algorithm>
#include <numeric>

using namespace std;

class Solution {
public:
    int countServers(vector<vector<int>>& grid) {
        int c = 0;
        vector<int> counted;
        counted.resize(grid.size());
        for (auto i = 0; i < grid.size(); ++i) {
            int rc = std::accumulate(grid[i].begin(), grid[i].end(), 0);
            if (rc >= 2) {
                c += rc;
                counted[i] = 1;
            }
        }

        for (auto i = 0; i < grid[0].size(); ++i) {
            int rc = 0;
            int cc = 0;
            for (auto j = 0; j < grid.size(); ++j) {
                if (grid[j][i] != 0) {
                    ++rc;
                    if (counted[j] == 0) {
                        ++cc;
                    }
                }
            }
            if (rc >= 2) {
                c += cc;
            }
        }

        return c;
    }
};


#endif /* SOLUTION_H_INCLUDED */
