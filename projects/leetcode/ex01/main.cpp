#include "solution.h"

int main(int argc, char *argv[]) {
    Solution::matrix input = {{0,2},{2,1},{3,4},{2,3},{1,4},{2,0},{0,4}};
    int k = 3;
    int n = 5;
    Solution solution;
    int r = solution.numWays(n, input, k);
    cout << r << endl;
}
