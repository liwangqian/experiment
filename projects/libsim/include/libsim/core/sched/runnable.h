#ifndef LIBSIM_RUNNABLE_H_INCLUDED
#define LIBSIM_RUNNABLE_H_INCLUDED

namespace libsim {

class runnable_context {};

class runnable {
public:
    virtual ~runnable() = default;
    virtual void run(runnable_context &) = 0;
};

} // namespace libsim

#endif /* LIBSIM_RUNNABLE_H_INCLUDED */
