#ifndef LIBSIM_WRITABLE_H_INCLUDED
#define LIBSIM_WRITABLE_H_INCLUDED

#include <cstddef>
#include <functional>

namespace libsim {

class writable {
public:
    virtual ~writable() = default;
    virtual bool write(const std::byte*, size_t count) noexcept = 0;
    virtual bool ready_write() const noexcept = 0;
    virtual void on_ready_write(std::function<void()> &&) noexcept = 0;
};

} // namespace libsim

#endif /* LIBSIM_WRITABLE_H_INCLUDED */
