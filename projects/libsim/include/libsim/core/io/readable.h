#ifndef LIBSIM_READABLE_H_INCLUDED
#define LIBSIM_READABLE_H_INCLUDED

#include <cstddef>
#include <functional>

namespace libsim {

class readable {
public:
    virtual ~readable() = default;
    virtual bool read(std::byte*, size_t count) noexcept = 0;
    virtual bool ready_read() const noexcept = 0;
    virtual void on_ready_read(std::function<void()> &&) noexcept = 0;
};

} // namespace libsim

#endif /* LIBSIM_READABLE_H_INCLUDED */
