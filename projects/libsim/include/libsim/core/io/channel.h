#ifndef LIBSIM_CHANNEL_H_INCLUDED
#define LIBSIM_CHANNEL_H_INCLUDED

#include "libsim/core/io/readable.h"
#include "libsim/core/io/writable.h"
#include "libsim/core/base/element.h"

namespace libsim {

class channel
    : public readable
    , public writable
    , public element {
public:
    using supper_type = element;

    explicit channel(element* parent = nullptr);
};

} // namespace libsim

#endif /* LIBSIM_CHANNEL_H_INCLUDED */
