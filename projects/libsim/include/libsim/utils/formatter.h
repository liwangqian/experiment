#ifndef LIBSIM_FORMATTER_H_INCLUDED
#define LIBSIM_FORMATTER_H_INCLUDED

#include <fmt/format.h>

namespace libsim {

static fmt::formatter<std::string> string_formatter;

} // namespace libsim

#endif /* LIBSIM_FORMATTER_H_INCLUDED */
