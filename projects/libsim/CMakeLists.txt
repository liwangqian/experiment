cmake_minimum_required(VERSION 3.13)
project(libsim)

set(CMAKE_CXX_STANDARD 17)

set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${PROJECT_SOURCE_DIR}/bin)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${PROJECT_SOURCE_DIR}/lib)

include_directories(include)
include_directories(../libcn/include)
add_subdirectory(third_party/fmt EXCLUDE_FROM_ALL)

set(${PROJECT_NAME}_src
    src/core/io/channel.cpp
    src/utils/uuid.cpp)

add_library(${PROJECT_NAME} SHARED ${${PROJECT_NAME}_src})

target_link_libraries(libsim fmt-header-only)

add_subdirectory(test)
