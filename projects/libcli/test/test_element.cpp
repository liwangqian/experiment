/* Copyright (c) 2021, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <catch2/catch.hpp>
#include "libcli/element.h"

TEST_CASE("test.key_element")
{
    libcli::key_element key{"display", "Display current system information"};
    REQUIRE(key.is_key() == true);
    REQUIRE(key.help("") == "Display current system information");
    REQUIRE(key.check("") == true);
    REQUIRE(key.match("disp") == true);
}

TEST_CASE("test.simple_parameter")
{
    libcli::parameter_element<> simple_parameter{"slot-id", "The slot index"};
    REQUIRE(simple_parameter.is_key() == false);
    REQUIRE(simple_parameter.help("") == "The slot index");
}

TEST_CASE("test.complex_parameter")
{
    libcli::parameter_element<libcli::complex_element> complex_parameter{
        "slotid",
        [](const std::string &v) { return true; },
        [](const std::string &anchor) { return anchor + " world"; }
    };

    REQUIRE(complex_parameter.is_key() == false);
    REQUIRE(complex_parameter.help("hello") == "hello world");
    REQUIRE(complex_parameter.check("") == true);
}