/* Copyright (c) 2021, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#pragma once

#include <libmsg/messages/message_wrapper.hpp>
#include <libmsg/messages/close_queue.hpp>
#include <libmsg/utils/result.hpp>

namespace libmsg {

template <typename MsgChannel>
struct message_sender {
    using message_channel = MsgChannel;

    using channel_ptr = typename message_channel::channel_ptr;

    ~message_sender()
    {
        shutdown();
    }

    explicit message_sender(channel_ptr channel)
        : channel{channel}
    {}

    template <typename Message>
    auto send(Message msg)
    {
        return thenable<Message>{msg, channel};
    }

    void shutdown()
    {
        send(libmsg::messages::close_queue{});
    }

    message_sender(message_sender &) = delete;
    message_sender& operator=(message_sender &) = delete;
    message_sender(message_sender &&) noexcept = default;
    message_sender& operator=(message_sender &&) noexcept = default;

private:
    template <typename Message>
    struct thenable {
        ~thenable()
        {
            // exception
            if (channel == nullptr) return;

            auto wrapped_msg = messages::make_message(std::forward<Message>(msg), f);
            if (wrapped_msg) channel->send(wrapped_msg);
        }

        thenable(Message msg, channel_ptr channel)
            : msg{msg}, channel{channel}
        {
        }

        void then(messages::response_callback && f_)
        {
            f = std::move(f_);
        }

    private:
        Message msg;
        channel_ptr channel;
        messages::response_callback f;
    };

private:
    channel_ptr channel{};
};

} // namespace libmsg
