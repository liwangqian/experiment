/* Copyright (c) 2021, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#pragma once

#include <thread>
#include <mutex>
#include <libmsg/message_receiver.hpp>
#include <libmsg/utils/noncopyable.hpp>

namespace libmsg {

template <typename MsgQueue>
class message_server : utils::noncopyable {
public:
    ~message_server()
    {
        sync();
    }

    message_server() = default;

    template <typename ...MsgHandle>
    bool start(MsgHandle ...handle)
    {
        std::lock_guard lock{mtx};
        if (worker_thread == nullptr) {
            worker_thread = new std::thread{[&](){
                receiver.handles(std::forward<MsgHandle>(handle)...)
                    .wait_message();
            }};
            return worker_thread != nullptr;
        }
        return false;
    }

    auto sender()
    {
        return receiver.sender();
    }

    void sync()
    {
        if (worker_thread != nullptr) {
            if (worker_thread->joinable()) {
                worker_thread->join();
            }
            delete worker_thread;
            worker_thread = nullptr;
        }
    }

    void detach()
    {
        if (worker_thread != nullptr &&
            worker_thread->joinable()) {
            worker_thread->detach();
            delete worker_thread;
            worker_thread = nullptr;
        }
    }

private:
    libmsg::message_receiver<MsgQueue> receiver;
    std::mutex mtx;
    std::thread *worker_thread{nullptr};
};

} // namespace libmsg
