/* Copyright (c) 2021, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#pragma once

#include <utility>
#include <memory>
#include <functional>
#include <libmsg/utils/result.hpp>

namespace libmsg {
namespace messages {

using response_callback = std::function<void(const utils::result &)>;

struct message_base {
    virtual ~message_base() = default;

    message_base(response_callback &&f)
        : f{std::move(f)}
    {}

    void response(const utils::result &res)
    {
        if (f) f(res);
    }

private:
    response_callback f;
};

template <typename Msg>
struct wrapped_message : message_base {
    using message_type = Msg;

    wrapped_message(Msg msg, response_callback &&f) noexcept
        : message_base{std::move(f)}, content{std::forward<Msg>(msg)}
    {}

    message_type content;
};

template <typename Msg, typename F>
inline auto make_message(Msg msg, F f)
{
    return std::make_shared<messages::wrapped_message<Msg>>(
        std::forward<Msg>(msg), std::forward<F>(f));
}

} // namespace messages
} // namespace libmsg
