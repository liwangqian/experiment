/* Copyright (c) 2022-2022, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef LIBMSG_MESSAGE_CENTER_HPP
#define LIBMSG_MESSAGE_CENTER_HPP

#include <string_view>
#include <map>
#include <mutex>
#include <memory>
#include <functional>

namespace libmsg {

template <typename Sender>
class message_center {
public:
    using message_sender = Sender;

    void sub_from_topic(std::string_view topic_name,
        std::string_view sender_name, message_sender &&sender)
    {
        std::shared_ptr<topic> tp = nullptr;
        {
            std::unique_lock<std::mutex> lock{mtx};
            tp = find_topic(topic_name);
            if (tp == nullptr) {
                tp = std::make_shared<topic>();
                topics[topic_name] = tp;
            }
        }
        tp->sub(sender_name,   sender);
    }

    template <typename Message>
    void pub_to_topic(std::string_view topic_name, Message &&msg)
    {
        std::shared_ptr<topic> tp = nullptr;
        {
            std::unique_lock<std::mutex> lock{mtx};
            tp = find_topic(topic_name);
            if (tp == nullptr) {
                return;
            }
        }
        tp->pub(std::forward<Message>(msg));
    }

    template <typename Message>
    void broadcast(Message &&msg)
    {
        std::unique_lock<std::mutex> lock{mtx};
        for (auto &t : topics) {
            t.second->pub(std::forward<Message>(msg));
        }
    }

private:
    struct topic {
        std::mutex mtx;
        std::string_view topic_name;
        std::map<std::string_view, message_sender> senders;

        void sub(std::string_view sender_name, message_sender &&sender)
        {
            std::unique_lock<std::mutex> lock{mtx};
            if (senders.find(sender_name) != senders.end()) {
                return;
            }
            senders[sender_name] = sender;
        }

        void unsub(std::string_view sender_name)
        {
            std::unique_lock<std::mutex> lock{mtx};
            senders.erase(sender_name);
        }

        template <typename Message>
        void pub(Message &&msg)
        {
            std::unique_lock<std::mutex> lock{mtx};
            for (auto &sender : senders) {
                sender.send(std::forward<Message>(msg));
            }
        }
    };

    std::mutex mtx;
    std::map<std::string_view, std::shared_ptr<topic>> topics;

    std::shared_ptr<topic> find_topic(std::string_view name)
    {
        auto it = topics.find(name);
        if (it == topics.end()) {
            return nullptr;
        }
        return it->second;
    }
};

} // namespace libmsg

#endif /* LIBMSG_MESSAGE_CENTER_HPP */
