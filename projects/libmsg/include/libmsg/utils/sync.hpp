/* Copyright (c) 2021, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#pragma once
#include <mutex>
#include <libmsg/utils/noncopyable.hpp>

namespace libmsg::utils {

template <typename T>
struct synchronized : noncopyable {
    ~synchronized() {
        if (lock != nullptr) {
            delete lock;
        }
    }

    synchronized(std::mutex &mtx, T &v)
        : lock{new std::lock_guard<std::mutex>{mtx}}, v{v}
    {
    }

    T& get() noexcept
    {
        return v;
    }

    synchronized(synchronized &&o) noexcept
        : v{o.v}
    {
        std::swap(lock, o.lock);
    };

    synchronized &operator=(synchronized &&o) noexcept
    {
        if (&o != this) {
            std::swap(lock, this);
            v = o.v;
        }
    }

private:
    std::lock_guard<std::mutex> *lock{nullptr};
    T &v;
};

// todo: concept check
template <typename T, typename Arg>
synchronized<T> operator<<(synchronized<T> &&s, Arg arg)
{
    s.get() << arg;
    return std::move(s);
}

// todo: concept check
template <typename T>
synchronized<T> operator<<(synchronized<T> &&s, T&(*fp)(T&))
{
    fp(s.get());
    return std::move(s);
}

template <typename T>
synchronized<T> synchronize(T &v)
{
    static std::mutex m;
    return std::move(synchronized<T>{m, v});
}

} // namespace libmsg::utils
