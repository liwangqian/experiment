/* Copyright (c) 2021, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#pragma once

#include <type_traits>
#include <libmsg/utils/result.hpp>
#include <libmsg/utils/callable_traits.hpp>
#include <libmsg/utils/typelist.hpp>
#include <libmsg/utils/tl/at.hpp>
#include <libmsg/utils/tl/size.hpp>

namespace libmsg {
namespace utils {

template <typename Handle>
struct message_handle_trait {
    using trait = callable_trait_selector<std::decay_t<Handle>>;
    using message_type = std::decay_t<tl::at_t<0, typename trait::param_type>>;
};

template <typename Handle>
using get_message_type = typename message_handle_trait<Handle>::message_type;

template <typename Handle>
struct is_message_handle {
    using trait = callable_trait_selector<std::decay_t<Handle>>;
    static constexpr bool value =
        std::is_same_v<typename trait::result_type, utils::result> &&
        is_typelist_v<typename trait::param_type> &&
        (tl::size_v<typename trait::param_type> == 1);
};

} // namespace utils
} // namespace libmsg


