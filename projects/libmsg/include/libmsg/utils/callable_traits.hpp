/* Copyright (c) 2021, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#pragma once

#include <type_traits>
#include <libmsg/utils/typelist.hpp>
#include <libmsg/utils/none_type.hpp>

namespace libmsg {
namespace utils {

template <typename F>
struct callable_trait;

template <typename R, typename ...Arg>
struct callable_trait<R(Arg...)> {
    using result_type = R;
    using param_type = typelist<Arg...>;
    using signature = R(Arg...);
};

template <typename R, typename ...Arg>
struct callable_trait<R(*)(Arg...)> : callable_trait<R(Arg...)> {};

template <typename C, typename R, typename ...Arg>
struct callable_trait<R(C::*)(Arg...)> : callable_trait<R(Arg...)> {};

template <typename C, typename R, typename ...Arg>
struct callable_trait<R(C::*)(Arg...) const> : callable_trait<R(Arg...)> {};

template <typename C, typename R, typename ...Arg>
struct callable_trait<R(C::*)(Arg...) noexcept> : callable_trait<R(Arg...)> {};

template <typename C, typename R, typename ...Arg>
struct callable_trait<R(C::*)(Arg...) const noexcept> : callable_trait<R(Arg...)> {};

template <typename F>
struct is_ordinary_function {
    static constexpr auto value = std::is_function<F>::value ||
    std::is_member_function_pointer<F>::value ||
    std::is_function<typename std::remove_pointer<F>::type>::value;
};

template <typename T>
struct is_callable_class {
    template <class U>
    static auto check(U*) -> decltype(&U::operator(), std::true_type());

    template <class U>
    static auto check(...) -> std::false_type;

    using type = decltype(check<T>(nullptr));
    static constexpr bool value = type::value;
};

template <typename T,
    bool IsOrdinaryFunction = is_ordinary_function<T>::value,
    bool IsCallableClass = is_callable_class<T>::value>
struct callable_trait_selector {
    using trait = callable_trait<T>;
    using result_type = typename trait::result_type;
    using param_type = typename trait::param_type;
    using signature = typename trait::signature;
};

template <typename T>
struct callable_trait_selector<T, false, false> {
    using trait = none_t;
    using result_type = none_t;
    using param_type = none_t;
    using signature = none_t;
};

template <typename T>
struct callable_trait_selector<T, false, true> {
    using trait = callable_trait<decltype(&T::operator())>;
    using result_type = typename trait::result_type;
    using param_type = typename trait::param_type;
    using signature = typename trait::signature;
};

} // namespace utils

} // namespace libmsg
