/* Copyright (c) 2021, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#pragma once

#include <libmsg/utils/error.hpp>
#include <any>

namespace libmsg::utils {

class result {
public:
    ~result() = default;

    result(utils::error &&e)
        : ok_{false}
        , value_{std::forward<utils::error>(e)}
    {
        // nop
    }

    result(const utils::error &e)
        : result{utils::error{e}}
    {
        // nop
    }

    template<typename T>
    result(T value)
        : ok_{true}
        , value_{std::forward<T>(value)}
    {
        // nop
    }

    bool ok() const
    {
        return ok_;
    }

    const auto& error() const
    {
        return std::any_cast<const utils::error&>(value_);
    }

    template <typename T>
    bool is() const noexcept
    {
        return std::any_cast<T>(&value_) != nullptr;
    }

    template <typename T>
    T as() noexcept
    {
        if constexpr (std::is_pointer_v<T>) {
            return std::any_cast<std::remove_pointer_t<T>>(&value_);
        } else {
            return std::any_cast<T>(value_);
        }
    }

    template <typename T>
    const auto& value() const
    {
        return std::any_cast<const std::decay_t<T>&>(value_);
    }

    result(const result &) = default;
    result &operator=(const result &) = default;

    result(result &&) = default;
    result &operator=(result &&) = default;

private:
    bool ok_{false};
    std::any value_;
};

} // namespace libmsg::utils
