/* Copyright (c) 2021, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#pragma once

#include <libmsg/utils/message_handle_trait.hpp>
#include <libmsg/utils/result.hpp>
#include <libmsg/utils/typelist.hpp>
#include <libmsg/utils/tl/indexof.hpp>
#include <libmsg/utils/tl/all.hpp>
#include <libmsg/messages/message_wrapper.hpp>
#include <libmsg/messages/close_queue.hpp>

#include <type_traits>
#include <tuple>

namespace libmsg {

/**
 * @brief 消息分发类，处理特定类型的消息，消息类型根据MsgHandle的第一个参数推导出来
 *
 * @tparam MsgQueue     消息通道类型
 * @tparam MsgHandle    消息处理回调函数类型，必须是'bool(msgtype)'类型
 */
template <typename MsgQueue, typename ...MsgHandle>
struct message_dispatcher {
    using queue_type = MsgQueue;

    using message_types = utils::typelist<utils::get_message_type<MsgHandle>...>;

    using handle_types = utils::typelist<MsgHandle...>;

    using this_type = message_dispatcher<queue_type, message_types>;

    static_assert(utils::tl::all_v<utils::is_message_handle, handle_types>,
        "The signature of message handle must be 'result(message-type)'");

    message_dispatcher(message_dispatcher &&) noexcept = default;
    message_dispatcher &operator=(message_dispatcher &&) noexcept = default;

    message_dispatcher(const message_dispatcher &) = delete;
    message_dispatcher &operator=(const message_dispatcher &) = delete;

    message_dispatcher(queue_type *queue, MsgHandle ...handles)
        : queue{queue}, handles{std::forward<MsgHandle>(handles)...}
    {}

    void wait_message()
    {
        if (queue != nullptr) {
            wait_and_dispatch();
        }
    }

private:
    void wait_and_dispatch()
    {
        for(;;) {
            auto msg = queue->wait_and_pop();
            if (is_close_queue_message(msg.get())) {
                break;
            }

            auto res = try_dispatch(msg.get(), message_types{});
            msg->response(res);
        }
    }

    template <typename T0, typename ...T>
    utils::result try_dispatch(messages::message_base *msg, utils::typelist<T0, T...>)
    {
        if (auto *w = dynamic_cast<messages::wrapped_message<T0>*>(msg)) {
            constexpr auto index = utils::tl::indexof_v<T0, message_types>;
            return std::get<index>(handles)(w->content);
        }
        return try_dispatch(msg, utils::typelist<T...>{});
    }

    utils::result try_dispatch(messages::message_base *msg, utils::typelist<>)
    {
        return make_error(-101, "unexpected message.");
    }

    static bool is_close_queue_message(messages::message_base *msg)
    {
        return dynamic_cast<messages::wrapped_message<messages::close_queue>*>(msg) != nullptr;
    }

private:
    queue_type *queue{nullptr};
    std::tuple<MsgHandle...> handles;
};

} // namespace libmsg
