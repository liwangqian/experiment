/* Copyright (c) 2021, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#pragma once

#include <libmsg/utils/noncopyable.hpp>
#include <libmsg/utils/typelist.hpp>
#include <libmsg/utils/tl/all.hpp>
#include <libmsg/message_dispatcher.hpp>
#include <libmsg/message_sender.hpp>
#include <libmsg/message_channel.hpp>

namespace libmsg {

template <typename MsgQueue>
struct message_receiver : utils::noncopyable {
    using queue_type = MsgQueue;
    using channel_type = message_channel<queue_type>;
    using channel_ptr = typename channel_type::channel_ptr;
    using message_sender = libmsg::message_sender<channel_type>;

    message_receiver(message_receiver &&) noexcept = default;
    message_receiver& operator=(message_receiver &&) noexcept = default;

    explicit message_receiver()
    {
        channel = make_channel(&queue);
    }

    template <typename ...MsgHandle>
    auto handles(MsgHandle ...handle)
    {
        using handle_types = utils::typelist<MsgHandle...>;
        static_assert(utils::tl::all_v<utils::is_message_handle, handle_types>,
            "The signature of message handle must be 'bool(message-type)'");

        using dispatcher_type = message_dispatcher<queue_type, MsgHandle...>;
        return std::move(dispatcher_type{&queue, std::forward<MsgHandle>(handle)...});
    }

    auto sender()
    {
        return message_sender{channel};
    }

private:
    queue_type queue;
    channel_ptr channel{};
};

} // namespace libmsg
