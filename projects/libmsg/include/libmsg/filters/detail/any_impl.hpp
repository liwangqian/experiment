/* Copyright (c) 2021, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#pragma once

#include <libmsg/utils/typelist.hpp>
#include <libmsg/utils/tl/indexof.hpp>
#include <libmsg/utils/message_handle_trait.hpp>
#include <libmsg/messages/message_wrapper.hpp>
#include <tuple>

namespace libmsg::filters::detail {

template <typename ...MsgHandle>
struct any_impl {
    using message_types = libmsg::utils::typelist<libmsg::utils::get_message_type<MsgHandle>...>;

    any_impl(MsgHandle ...handle)
        : handles{std::forward<MsgHandle>(handle)...}
    {}

    bool operator()(libmsg::messages::message_base const &msg)
    {
        return try_handle(msg, message_types{});
    }

private:
    template <typename T0, typename ...T>
    bool try_handle(libmsg::messages::message_base const &msg, libmsg::utils::typelist<T0, T...>)
    {
        if (auto *w = dynamic_cast<const libmsg::messages::wrapped_message<T0>*>(&msg)) {
            constexpr auto index = libmsg::utils::tl::indexof_v<T0, message_types>;
            return std::get<index>(handles)(w->content);
        }
        return try_handle(msg, libmsg::utils::typelist<T...>{});
    }

    bool try_handle(libmsg::messages::message_base const &msg, libmsg::utils::typelist<>)
    {
        return false;
    }

private:
    std::tuple<MsgHandle...> handles;
};

} // namespace libmsg::filters::detail

