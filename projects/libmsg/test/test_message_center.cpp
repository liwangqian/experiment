/* Copyright (c) 2022-2022, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <catch2/catch.hpp>
#include <libmsg/message_queue.hpp>
#include <libmsg/message_server.hpp>
#include <libmsg/message_center.hpp>
#include <libmsg/message_sender.hpp>
#include <libmsg/message_channel.hpp>
#include <libmsg/utils/sync.hpp>
#include <iostream>

using message_center =
    libmsg::message_center<
        libmsg::message_sender<
            libmsg::message_channel<libmsg::message_queue>
        >
    >;

using message_server =
    libmsg::message_server<
        libmsg::message_queue
    >;

TEST_CASE("libmsg.message_center")
{
    message_center mcenter;

    message_server msever_01;
    msever_01.start([](int x) -> libmsg::utils::result {
        return x + 1;
    });

    message_server msever_02;
    msever_02.start([](float x) -> libmsg::utils::result {
        return x + 100.0f;
    });

    mcenter.sub_from_topic("int", "sub01", msever_01.sender());
    mcenter.pub_to_topic("int", int{1});
}