/* Copyright (c) 2021, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <libmsg/message_dispatcher.hpp>
#include <libmsg/message_receiver.hpp>
#include <libmsg/message_sender.hpp>
#include <libmsg/utils/callable_traits.hpp>
#include <libmsg/filters/any.hpp>
#include <libmsg/filters/all.hpp>
#include <libmsg/filters/seq.hpp>
#include <catch2/catch.hpp>
#include <iostream>
#include <thread>

// TEST_CASE("Test message dispatcher.handle")
// {
//     libmsg::message_queue queue;
//     auto receiver = libmsg::make_receiver(&queue);
//     auto s1 = receiver.sender();
//     auto s2 = receiver.sender();

//     auto t1 = std::thread{[&](){
//         receiver.handles(
//             [](double x) {
//                 std::cout << "receive double: " << x << std::endl;
//                 return false;
//             },
//             [](unsigned int x) {
//                 std::cout << "receive int: " << x << std::endl;
//                 return false;
//             },
//             [](float x) {
//                 std::cout << "receive float: " << x << std::endl;
//                 return false;
//             }
//         ).wait_message();
//     }};

//     s1.send(100u);
//     s1.send(1.0);
//     s1.send(2.0f);
//     s2.send(3.0f);
//     s1.send(libmsg::messages::close_queue{});
//     s2.send(200u); // no reponse

//     t1.join();

//     REQUIRE_FALSE(false);
// }
