/* Copyright (c) 2021, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <catch2/catch.hpp>
#include <libmsg/message_queue.hpp>
#include <libmsg/message_server.hpp>
#include <libmsg/utils/sync.hpp>
#include <iostream>

struct message {
    uint32_t value{0};
};

TEST_CASE("Test message_server")
{
    using namespace libmsg;

    message_server<message_queue> server;
    auto sender = server.sender();
    server.start(
        [](const message &msg) -> utils::result {
            return message{msg.value + 1};
        }
    );

    sender.send(message{1})
        .then([&](const utils::result &res) {
            REQUIRE(res.ok());
            REQUIRE(res.value<message>().value == 2);
        });

    sender.send(message{10})
        .then([&](const utils::result &res) {
            REQUIRE(res.ok());
            REQUIRE(res.value<message>().value == 11);
        });
    
    sender.shutdown();

    server.sync();
}

template <typename F>
struct fwrapper {
    fwrapper(F &&f) : f{std::forward<F>(f)} {}

    F f;
};

template <typename F>
fwrapper<F> wrap(F &&f) noexcept
{
    return {f};
}

TEST_CASE("talk with message server")
{
    using namespace libmsg;
    message_server<message_queue> userA;
    auto userAChannel = userA.sender();

    message_server<message_queue> userB;
    auto userBChannel = userB.sender();

    userA.start([&userBChannel](const message &msg) -> utils::result {
        {
            utils::synchronize(std::cout) << "user A received: " << msg.value << std::endl;
            // std::cout << "user A received: " << msg.value << std::endl;
        }
        // userBChannel.send(message{msg.value + 1});
        return true;
    });

    userB.start([&userAChannel](const message &msg) -> utils::result {
        {
            utils::synchronize(std::cout) << "user B received: " << msg.value << std::endl;
            // std::cout << "user B received: " << msg.value << std::endl;
        }
        // userAChannel.send(message{msg.value + 1});
        return true;
    });

    for (auto i = 0u; i < 10; ++i) {
        userAChannel.send(message{i});
        userBChannel.send(message{i});
    }

    userAChannel.shutdown();
    userBChannel.shutdown();

    userA.sync();
    userB.sync();

    std::cout << sizeof(std::function<void(void)>) << std::endl;

    auto ff = []() { return 1; };
    auto f2 = wrap(ff);
    auto f3 = wrap(printf);

    std::cout << sizeof(ff) << std::endl;
    std::cout << sizeof(f2) << std::endl;
    std::cout << sizeof(f3) << std::endl;
}
