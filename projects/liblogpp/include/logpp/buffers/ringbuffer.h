/* Copyright (c) 2022-2022, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef LIBSIM_RINGBUFFER_H_INCLUDED
#define LIBSIM_RINGBUFFER_H_INCLUDED

#include "logpp/utils/noncopyable.h"
#include "logpp/utils/memory.h"

namespace logpp {

template <typename T>
class ringbuffer : public noncopyable {
public:
    using value_type = T;
    using pointer = T*;
    using reference = T&;
    using const_pointer = const T*;
    using const_reference = const T&;

    static_assert(std::is_nothrow_default_constructible_v<T>,
        "T must be default constructible");

    ringbuffer() noexcept = default;

    explicit ringbuffer(size_t max_size)
    {
        init_buffer(max_size);
    }

    ~ringbuffer()
    {
        release();
    }

    void initialize(size_t max_size) noexcept
    {
        if (initialized()) {
            return;
        }
        init_buffer(max_size);
    }

    bool initialized() const noexcept
    {
        return data_ != nullptr;
    }

    bool empty() const noexcept
    {
        return !data_ || data_->size_ == 0;
    }
    
    bool full() const noexcept
    {
        return data_ && data_->size_ == data_->capacity_;
    }

    size_t capacity() const noexcept
    {
        return data_ ? data_->capacity_ : 0;
    }

    size_t size() const noexcept
    {
        return data_ ? data_->size_ : 0;
    }

    bool try_push(const_reference v)
        noexcept(std::is_nothrow_copy_assignable_v<T>)
    {
        if (!initialized()) {
            return false;
        }
        auto index = next_head();
        if (index == npos) return false;
        data_->head_[index] = v;
        return true;
    }

    bool try_pop(reference v)
        noexcept(std::is_nothrow_copy_assignable_v<T>)
    {
        if (!initialized()) {
            return false;
        }
        auto index = next_tail();
        if (index == npos) return false;
        v = data_->mem_start_[index];
        return true;
    }

    bool try_push_swap(reference v)
        noexcept(std::is_nothrow_swappable_v<T>)
    {
        if (!initialized()) {
            return false;
        }
        auto index = next_head();
        if (index == npos) return false;
        v.swap(data_->mem_start_[index]);
        return true;
    }

    bool try_pop_swap(reference v)
        noexcept(std::is_nothrow_swappable_v<T>)
    {
        if (!initialized()) {
            return false;
        }
        auto index = next_tail();
        if (index == npos) return false;
        v.swap(std::ref(data_->mem_start_[index]));
        return true;
    }

    void swap(ringbuffer &x) noexcept
    {
        std::swap(data_, x.data_);
    }

private:
    static constexpr size_t npos = -1U;

    size_t next_head() noexcept
    {
        if (full()) {
            return npos;
        }
        ++data_->size_;
        return next(data_->head_);
    }

    size_t next_tail() noexcept
    {
        if (empty()) {
            return npos;
        }
        --data_->size_;
        return next(data_->tail_);
    }

    size_t next(size_t &curr) noexcept
    {
        auto index = curr++;
        if (curr >= capacity()) curr %= capacity();
        return index;
    }

    void init_buffer(size_t max_size) noexcept
    {
        auto alloc = logpp::allocator<std::byte>();
        auto mptr = alloc.allocate(sizeof(buffer_data) + sizeof(T) * max_size);
        if (mptr == nullptr) {
            return;
        }

        auto tmp = (buffer_data*)mptr;
        auto alloc1 = logpp::allocator<T>();
        for (auto i = 0; i < max_size; ++i) {
            alloc1.construct(&tmp->mem_start_[i]);
        }
        tmp->capacity_ = max_size;
        tmp->size_ = 0;
        tmp->head_ = 0;
        tmp->tail_ = 0;
        data_ = tmp;
    }

    void release()
    {
        if (data_ != nullptr) {
            auto bytes = sizeof(buffer_data) + sizeof(T) * data_->capacity_;
            auto alloc = logpp::allocator<T>();
            for (auto i = 0; i < data_->capacity_; ++i) {
                alloc.destroy(&data_->mem_start_[i]);
            }
            logpp::allocator<std::byte>().deallocate((std::byte*)data_, bytes);
            data_ = nullptr;
        }
    }

    struct buffer_data {
        size_t capacity_{0};
        size_t size_{0};
        size_t head_{0};
        size_t tail_{0};
        value_type mem_start_[];
    };

    buffer_data *data_{nullptr};
};

} // namespace logpp

#endif /* LIBSIM_RINGBUFFER_H_INCLUDED */
