/* Copyright (c) 2022-2022, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef CIRCULAR_BUFFER_H_INCLUDED
#define CIRCULAR_BUFFER_H_INCLUDED

#include "logpp/configs.h"
#include "logpp/concepts/swapable.h"
#include "logpp/utils/noncopyable.h"
#include "logpp/utils/memory.h"
#include "logpp/utils/math.h"

LOGPP_NS_BEGIN

template <typename T,
    size_t MAX_SIZE = math::clp2<size_t>(LOGPP_LOG_BUFFER_SIZE)>
class circular_buffer : public noncopyable {
public:
    using value_type = T;
    using pointer = T*;
    using reference = T&;
    using const_pointer = const T*;
    using const_reference = const T&;

    ~circular_buffer()
    {
        release();
    }

    circular_buffer() noexcept
    {
        initialize();
    }

    circular_buffer(circular_buffer &&rv) noexcept
    {
        swap(rv);
    }

    circular_buffer &operator=(circular_buffer &&rv) noexcept
    {
        swap(rv);
        return *this;
    }

    constexpr size_t max_size() const noexcept { return MAX_SIZE;   }

    size_t capacity() const noexcept { return MAX_SIZE;             }
    size_t size()     const noexcept { return data_ != nullptr ? data_->size_ : 0; }
    bool   full()     const noexcept { return size() == capacity(); }
    bool   empty()    const noexcept { return size() == 0;          }

    void swap(circular_buffer &other)
    {
        if (this == &other) return;
        std::swap(data_, other.data_);
    }

    pointer try_push() noexcept
    {
        auto index = next_head();
        if (index == npos) return nullptr;
        return data_->mem_start_ + index;
    }

    pointer try_pop() noexcept
    {
        auto index = next_tail();
        if (index == npos) return nullptr;
        return data_->mem_start_ + index;
    }

    template <typename X>
    auto try_push_swap(X &v) noexcept
        -> std::enable_if_t<std::is_same_v<X, T> && is_swapable_v<T>, bool>
    {
        auto index = next_head();
        if (index == npos) return false;
        v.swap(data_->mem_start_[index]);
        return true;
    }

    template <typename X>
    auto try_pop_swap(X &v) noexcept
        -> std::enable_if_t<std::is_same_v<X, T> && is_swapable_v<T>, bool>
    {
        auto index = next_tail();
        if (index == npos) return false;
        v.swap(data_->mem_start_[index]);
        return true;
    }

private:
    static constexpr size_t npos = -1U;

    auto mod(size_t index) const noexcept
    {
        return index & (max_size() - 1);
    }
    
    auto next_head() noexcept
    {
        if (full()) return npos;

        ++data_->size_;
        auto index = data_->head_++;
        return mod(index);
    }

    auto next_tail() noexcept
    {
        if (empty()) return npos;

        --data_->size_;
        auto index = data_->tail_++;
        return mod(index);
    }

    void initialize() noexcept
    {
        try {
            data_ = new_object<buffer_data>();
        } catch (...) {
            data_ = nullptr;
        }
    }

    void release()
    {
        if (data_ != nullptr) {
            delete_object(data_);
            data_ = nullptr;
        }
    }

    struct buffer_data {
        size_t size_{0};
        size_t head_{0};
        size_t tail_{0};
        value_type mem_start_[MAX_SIZE];
    };

    buffer_data *data_{nullptr};
};

LOGPP_NS_END

#endif /* CIRCULAR_BUFFER_H_INCLUDED */
