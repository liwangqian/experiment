/* Copyright (c) 2022-2022, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef LOGPP_FILE_SINK_H
#define LOGPP_FILE_SINK_H

#include "logpp/configs.h"
#include "logpp/sinks/isink.h"
#include "logpp/utils/formatter.h"
#include "logpp/utils/noncopyable.h"

#include <fstream>
#include <string>
#include <atomic>

LOGPP_NS_BEGIN

class file_sink : public isink, noncopyable {
public:
    enum text_mode {
        raw = 0L,
        bin = std::ios_base::binary,
    };

    explicit file_sink(const char *file_path, text_mode mode = text_mode::raw)
        : file_path_{file_path}
        , mode_{mode}
    {}

    explicit file_sink(const std::string &file_path, text_mode mode = text_mode::raw)
        : file_path_{file_path}
        , mode_{mode}
    {}

    void open() noexcept override
    {
        if (is_open()) return;

        std::ios_base::openmode m = std::ios_base::out
            | std::ios_base::app
            | static_cast<std::ios_base::openmode>(mode_);

        os_.open(file_path_, m);
    }

    void close() noexcept override { os_.close(); }
    void flush() noexcept override { os_.flush(); }
    void output(message &m) noexcept override
    {
        os_ << format_message(m);
    }

    bool is_open() const noexcept override { return os_.is_open(); }

    void swap(file_sink &other) noexcept
    {
        std::swap(os_, other.os_);
        std::swap(file_path_, other.file_path_);
        std::swap(mode_, other.mode_);
    }

    const std::string &filepath() const noexcept
    {
        return file_path_;
    }

    text_mode textmode() const noexcept
    {
        return mode_;
    }

private:
    std::ofstream os_;
    std::string file_path_;
    text_mode mode_{text_mode::raw};
};

LOGPP_NS_END

#endif
