/* Copyright (c) 2022-2022, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef LOGPP_BASIC_SINK_H
#define LOGPP_BASIC_SINK_H

#include "logpp/configs.h"
#include "logpp/sinks/isink.h"

LOGPP_NS_BEGIN

template <typename LogWriter>
class basic_sink : public isink {
public:
    using this_type = basic_sink<LogWriter>;
    using log_writer_type = LogWriter;

    basic_sink(const log_writer_type &writer)
        : writer_{writer}
    {
    }

    basic_sink(log_writer_type &&writer)
        : writer_{std::move(writer)}
    {
    }

    basic_sink(const basic_sink &) = default;
    basic_sink &operator=(const basic_sink&) = default;
    basic_sink(basic_sink &&) = default;
    basic_sink &operator=(basic_sink&&) = default;

    void open() noexcept override   { writer_.open();   }
    void close() noexcept override  { writer_.close();  }
    void flush() noexcept override  { writer_.flush();  }

    void output(message &m) noexcept override { writer_.output(m); }

    bool is_open() const noexcept override { return writer_.is_open(); }

private:
    log_writer_type writer_;
};

LOGPP_NS_END

#endif
