/* Copyright (c) 2022-2022, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef FORMATTER_H_INCLUDED
#define FORMATTER_H_INCLUDED

#include "logpp/configs.h"
#include "logpp/utils/context.h"
#include "logpp/utils/message.h"
#include "logpp/utils/level.h"

#include <string>
#include <sstream>
#include <ctime>
#include <iomanip>

LOGPP_NS_BEGIN

namespace ascii {

static constexpr char space = 32;
static constexpr char zero = 48;
static constexpr char dot = 46;

}

template <typename OS, typename ValueType>
inline void format_value(OS &os, ValueType &&value,
    size_t width, char fill, std::ios_base::fmtflags flags)
{
    auto c = os.fill(fill);
    auto w = os.width(width);
    auto f = os.setf(flags);
    os << value;
    os.fill(c);
    os.width(w);
    os.setf(f);
}

inline void format_timepoint(const time::timepoint &tp, std::stringstream &os) noexcept
{
    std::tm tm;
    time::to_local_time(tp, &tm);
    auto microsec = std::chrono::duration_cast<time::microseconds>(tp.time_since_epoch());
    // format {2022/04/17 00:00:00.000001}
    os << '{';
    os << std::put_time(&tm, "%Y/%m/%d %H:%M:%S.");
    format_value(os, (microsec.count() % 1000000UL), 6, ascii::zero, os.flags());
    os << '}';
}

inline void format_context(const logpp::context &ctx, std::stringstream &os) noexcept
{
    os << '{';
    format_value(os, ctx.short_file(), LOGPP_SHORT_FILE_LEN + 2, ascii::dot, os.flags());
    os << ':';
    format_value(os, ctx.line(), 4, ascii::space, std::ios_base::left);
    os << '}';
    os << ascii::space << level_string(ctx.level());
}

static inline void format_tid(const std::thread::id &id, std::stringstream &os) noexcept
{
    os << "{tid:" << id << '}';
}

static inline void format_content(const char *content, std::stringstream &os) noexcept
{
    os << ascii::space << content;
}

static inline std::string format_message(const message &m) noexcept
{
    std::stringstream os;
    if (!m.valid()) return os.str();

    // format {{time}}{tid:xxx}{file}:{line} {level}{content}
    format_timepoint(m.time(), os);
    format_tid(m.tid(), os);
    format_context(m.context(), os);
    format_content(m.content(), os);
    return os.str();
}

LOGPP_NS_END

#endif /* FORMATTER_H_INCLUDED */
