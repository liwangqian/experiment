/* Copyright (c) 2022-2022, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef CONTEXT_H_INCLUDED
#define CONTEXT_H_INCLUDED

#include "logpp/configs.h"

#include <cstdint>
#include <cstring>
#include <functional>

LOGPP_NS_BEGIN

class context {
public:
    context() = default;
    context(const char *file, uint16_t line, uint16_t level)
        : file_{file}, line_{line}, level_{level}
    {}

    context(const context &c)
        : file_{c.file_} , line_{c.line_} , level_{c.level_}
    {
    }

    context &operator=(const context &c)
    {
        if (&c != this) {
            file_ = c.file_;
            line_ = c.line_;
            level_ = c.level_;
        }
        return *this;
    }

    auto file() const noexcept  { return file_;  }
    auto line() const noexcept  { return line_;  }
    auto level() const noexcept { return level_; }

    auto short_file() const noexcept
    {
        auto len = strlen(file_);
        auto offset = len > LOGPP_SHORT_FILE_LEN ? (len - LOGPP_SHORT_FILE_LEN) : 0;
        return file_ + offset;
    }

    void swap(context &c)
    {
        std::swap(file_, c.file_);
        std::swap(line_, c.line_);
        std::swap(level_, c.level_);
    }

private:
    const char *file_{nullptr};
    uint16_t line_{0};
    uint16_t level_{0};
};

LOGPP_NS_END

#endif /* CONTEXT_H_INCLUDED */
