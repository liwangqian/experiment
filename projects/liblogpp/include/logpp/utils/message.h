/* Copyright (c) 2022-2022, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef LOGPP_MESSAGE_H
#define LOGPP_MESSAGE_H

#include "logpp/configs.h"
#include "logpp/utils/context.h"
#include "logpp/utils/memory.h"
#include "logpp/utils/noncopyable.h"
#include "logpp/utils/time.h"

#include <cstdarg>
#include <cstddef>
#include <cstdint>
#include <cstdio>
#include <functional>
#include <thread>

LOGPP_NS_BEGIN

class message : public noncopyable {
public:
    ~message() noexcept
    {
        if (impl_ != nullptr) delete_object(impl_);
    }

    message() noexcept
    {
        impl_ = new_object<message::impl>();
    }

    message(message &&) noexcept = default;
    message &operator=(message &&) noexcept = default;

    void format(const logpp::context &ctx, const char *fmt, ...) noexcept
    {
        va_list args;
        va_start(args, fmt);
        vformat(ctx, fmt, args);
        va_end(args);
    }

    void vformat(const logpp::context &ctx, const char *fmt, va_list args) noexcept
    {
        if (!valid()) return;

        impl_->ctx_ = ctx;
        impl_->tp_ = time::clock::now();
        impl_->tid_ = std::this_thread::get_id();

        int n = vsnprintf(impl_->buff_, sizeof(impl_->buff_), fmt, args);
        if (n < 0) {
            n = 0;
            impl_->buff_[0] = '\0';
        }
        impl_->len_ = n;
    }

    const logpp::context &context() const noexcept
    {
        return impl_->ctx_;
    }

    const time::timepoint &time() const noexcept
    {
        return impl_->tp_;
    }

    auto tid() const noexcept
    {
        return impl_->tid_;
    }

    const char *content() const noexcept
    {
        return impl_->buff_;
    }

    size_t length() const noexcept
    {
        return impl_->len_;
    }

    void swap(message &m) noexcept
    {
        if (&m != this) std::swap(impl_, m.impl_);
    }

    bool valid() const noexcept { return impl_ != nullptr; }

private:
    struct impl {
        logpp::context ctx_;
        time::timepoint tp_;
        std::thread::id tid_;
        char buff_[LOGPP_LOG_BUFFER_SIZE]{'\0'};
        size_t len_{0};
    };
    // 采用指针数据，是为了实现快速内存交换
    impl *impl_{nullptr};
};

LOGPP_NS_END

#endif
