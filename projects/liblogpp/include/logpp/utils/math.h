/* Copyright (c) 2023-2023, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef MATH_H_INCLUDED
#define MATH_H_INCLUDED

#include <type_traits>

namespace logpp::math {

// 向下舍入找到值不大于x且与之最接近的2的幂
template <typename Int,
    typename = std::enable_if_t<
        std::conjunction_v<
            std::is_integral<Int>,
            std::is_unsigned<Int>>>>
constexpr Int flp2(Int x)
{
    x = x | (x >> 1u);
    x = x | (x >> 2u);
    x = x | (x >> 4u);
    if constexpr (sizeof(Int) > 1u) {
        x = x | (x >> 8u);
        if constexpr (sizeof(Int) > 2u) {
            x = x | (x >> 16u);
            if constexpr (sizeof(Int) > 4u) {
                x = x | (x >> 32u);
            }
        }
    }
    return x - (x >> 1u);
}

// 向上舍入找到值不小于x且与之最接近的2的幂
template <typename Int,
    typename = std::enable_if_t<
        std::conjunction_v<
            std::is_integral<Int>,
            std::is_unsigned<Int>>>>
constexpr Int clp2(Int x)
{
    x = x - 1u;
    x = x | (x >> 1u);
    x = x | (x >> 2u);
    x = x | (x >> 4u);
    if constexpr (sizeof(Int) > 1u) {
        x = x | (x >> 8u);
        if constexpr (sizeof(Int) > 2u) {
            x = x | (x >> 16u);
            if constexpr (sizeof(Int) > 4u) {
                x = x | (x >> 32u);
            }
        }
    }
    
    return x + 1u;
}

} // namespace logpp::math

#endif /* MATH_H_INCLUDED */
