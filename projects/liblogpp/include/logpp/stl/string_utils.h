/* Copyright (c) 2022-2022, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef LOGPP_STL_STRING_UTILS_H
#define LOGPP_STL_STRING_UTILS_H

#include <string>
#include <string_view>

namespace logpp::utils {

template <typename CharType>
inline constexpr auto length(const std::basic_string<CharType> &s) noexcept
{
    return s.length();
}

template <typename CharType>
inline constexpr auto length(const std::basic_string_view<CharType> &s) noexcept
{
    return s.length();
}

template <typename CharType>
inline constexpr auto length(const CharType *s) noexcept
{
    using traits = std::char_traits<CharType>;
    return traits::length(s);
}

namespace details {

template <typename CharType>
inline constexpr auto box(std::basic_string<CharType> &&s) noexcept
    -> decltype(s)
{
    return std::move(s);
}

template <typename CharType>
inline constexpr auto box(std::basic_string_view<CharType> &&s) noexcept
    -> decltype(auto)
{
    return s;
}

template <typename CharType>
inline constexpr auto box(const CharType *s) noexcept
{
    return std::basic_string_view<CharType>(s, length(s));
}

template <typename CharType, typename Arg1>
inline void concat_helper(std::basic_string<CharType> &out,
    const std::basic_string_view<CharType> &sep, Arg1 && arg1) noexcept
{
    out += arg1;
}

template <typename CharType, typename Arg1, typename ...Args>
inline void concat_helper(std::basic_string<CharType> &out,
    const std::basic_string_view<CharType> &sep,
    Arg1 && arg1, Args && ...args) noexcept
{
    out += arg1;
    out += sep;
    concat_helper(out, sep, std::forward<Args>(args)...);
}

} // namespace details

template <typename CharType, typename Arg1, typename ...Args>
inline auto concat(const CharType *sep, Arg1 && arg1, Args && ...args)
{
    using result_type = std::basic_string<CharType>;
    constexpr auto nargs = sizeof...(args);
    if constexpr (nargs == 0) {
        return result_type{details::box(std::forward<Arg1>(arg1))};
    } else {
        auto sep_ = details::box(sep);
        const auto max_len = length(sep_) * nargs +
            length(std::forward<Arg1>(arg1)) +
            (length(std::forward<Args>(args)) + ...);

        result_type out;
        out.reserve(max_len + 1);
        details::concat_helper(out, sep_,
            details::box(std::forward<Arg1>(arg1)),
            details::box(std::forward<Args>(args))...);
        return out;
    }
}

} // namespace logpp::utils

#endif
