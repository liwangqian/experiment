/* Copyright (c) 2022-2022, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <catch2/catch.hpp>
#include "logpp/sinks/async_sink.h"
#include "logpp/sinks/stdout_sink.h"
#include "logpp/utils/context.h"
#include "logpp/utils/message.h"
#include "logpp/utils/level.h"

using namespace std::chrono_literals;

TEST_CASE("test.sinks.async_sink")
{
    logpp::context context{__FILE__, __LINE__, logpp::LV_INFO};
    logpp::message message{};
    message.format(context, "Message from async sink, %u\n", 2022);

    auto cout_sink = logpp::make_cout_sink();
    REQUIRE(cout_sink != nullptr);
    REQUIRE(cout_sink->is_open());

    using async_stdout_sink = logpp::async_sink<logpp::stdout_sink>;
    auto async_sink = logpp::new_object<async_stdout_sink>(cout_sink);
    REQUIRE(async_sink != nullptr);
    REQUIRE(async_sink->is_open());

    async_sink->start();
    REQUIRE(async_sink->is_running());

    async_sink->output(message);
    std::this_thread::sleep_for(10ms);
    logpp::delete_object(async_sink);
}

