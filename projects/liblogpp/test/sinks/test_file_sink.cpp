/* Copyright (c) 2022-2022, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <catch2/catch.hpp>
#include "logpp/sinks/file_sink.h"

TEST_CASE("test.sinks.file_sink")
{
    logpp::file_sink fsink{"./test.log"};
    fsink.open();
    REQUIRE(fsink.is_open());
    
    logpp::context context{__FILE__, __LINE__, logpp::level::LV_CRITI};
    logpp::message message{};
    message.format(context, "Go! Go! Go!, Run for life!\n");

    fsink.output(message);

    fsink.flush();
    fsink.close();

    REQUIRE_FALSE(fsink.is_open());
}

TEST_CASE("test.sinks.file_sink.swap")
{
    logpp::file_sink fsink{"./test.log"};
    fsink.open();
    REQUIRE(fsink.is_open());

    logpp::file_sink fsink2{"./test02.log"};
    REQUIRE_FALSE(fsink2.is_open());

    fsink.swap(fsink2);
    REQUIRE(fsink2.is_open());
    REQUIRE_FALSE(fsink.is_open());
    REQUIRE(fsink.filepath() == "./test02.log");
    REQUIRE(fsink2.filepath() == "./test.log");
}
