/* Copyright (c) 2023-2023, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <catch2/catch.hpp>
#include <thread>
#include <iostream>
#include "logpp/buffers/message_queue.h"

using namespace std::chrono_literals;

TEST_CASE("test.logpp.buffer.message_queue")
{
    using mq = logpp::message_queue<int>;
    mq::configuration cfg{ .max_size = 1000, .pause_size = 800 };
    mq q;
    REQUIRE(not q.initialized());
    q.initialize(cfg);
    REQUIRE(q.initialized());

    bool stop = false;
    auto t1 = new std::thread{[&]() {
        while (!stop) {
            int v = 0;
            while (q.pop(v, 1s)) {
                // std::cout << v << std::endl;
            }
        }
    }};
    std::this_thread::sleep_for(std::chrono::milliseconds(1));
    REQUIRE(q.empty());
    REQUIRE_FALSE(q.full());
    REQUIRE(q.size() == 0);
    REQUIRE(q.capacity() == 1024);
    REQUIRE(q.max_size() == cfg.max_size);
    for (auto i = 0; i < 800; ++i) {
        REQUIRE(q.try_push(i) == true);
    }
    // REQUIRE(q.try_push(801) == true);
    // REQUIRE(q.push(802, 10ms) == true);

    stop = true;
    q.try_push(803);
    t1->join();
    delete t1;
}
