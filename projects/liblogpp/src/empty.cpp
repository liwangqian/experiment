#include "logpp/sinks/stdout_sink.h"
#include "logpp/filters/repeat_filter.h"
#include "logpp/utils/time.h"
#include "logpp/filters/log_key.h"
#include "logpp/filters/log_record.h"
#include "logpp/buffers/circular_buffer.h"
#include "logpp/filters/async_filter.h"
#include "logpp/filters/async_repeat_filter.h"
#include "logpp/loggers/basic_logger.h"
#include "logpp/sinks/async_file_sink.h"
#include "logpp/sinks/async_stdout_sink.h"
#include "logpp/stl/string_utils.h"
#include "logpp/buffers/ringbuffer.h"
#include "logpp/buffers/message_queue.h"

void demo()
{
    logpp::message_queue<uintptr_t> message_queue;
}