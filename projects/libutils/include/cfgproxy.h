#ifndef LIBSIM_CFGPROXY_H_INCLUDED
#define LIBSIM_CFGPROXY_H_INCLUDED

#include "keywords.h"
#include <stdint.h>
#include <stdbool.h>

LIBUTILS_STDC_BEGIN

LIBUTILS_CLASS(CfgProxy);

LIBUTILS_CLASS(CfgProxyConfig) {
    uint32_t maxCachedItems;
};

LIBUTILS_CLASS(CfgProxyItem) {
    uint32_t type;
    uint32_t len;
};



CfgProxy *CfgProxyCreate(const CfgProxyConfig *config);
void CfgProxyDestroy(CfgProxy *self);

bool CfgProxyPostRequest(CfgProxy *self, const CfgProxyItem *item);

LIBUTILS_STDC_END

#endif /* LIBSIM_CFGPROXY_H_INCLUDED */
