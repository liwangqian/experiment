/*
 * Copyright (c) 2021, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef LIBSIM_ALLOCATOR_H_INCLUDED
#define LIBSIM_ALLOCATOR_H_INCLUDED

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 资源分配器类，提供资源分配接口和接口上下文指针
 */
typedef struct Allocator {
    /**
     * @brief 内存申请接口
     *
     * @param memSize   申请的内存字节大小
     * @param ctx       分配器上下文指针
     * @return          成功申请内存则返回内存指针，否则返回NULL
     */
    void*(*acquire)(size_t memSize, void *ctx);

    /**
     * @brief 内存释放接口
     *
     * @param memPtr    指向待释放的内存指针的指针，成功释放后，该指针置NULL
     * @param ctx       分配器上下文指针
     */
    void (*release)(void **memPtr, void *ctx);

    /**
     * @brief 对象构造函数接口
     *
     * @param memPtr    对象的内存地址指针
     * @param ctx       分配器上下文指针
     * @return true     对象构造成功
     * @return false    对象构造失败
     */
    bool (*ctor)(void *memPtr, void *ctx);

    /**
     * @brief 对象析构函数接口
     *
     * @param memPtr    对象的内存地址指针
     * @param ctx       分配器上下文指针
     */
    void (*dtor)(void *memPtr, void *ctx);

    /* 分配器上下文指针 */
    void *ctx;
} Allocator;

/**
 * @brief 分配器包装函数，调用allocator->acquire完成内存申请。
 *
 * @param allocator     分配器指针
 * @param memSize       申请的内存字节大小
 * @return void*        成功则返回内存地址，否则返回NULL
 */
static inline void *AllocatorAcquire(const Allocator *allocator, size_t memSize)
{
    if (allocator != NULL && allocator->acquire != NULL) {
        return allocator->acquire(memSize, allocator->ctx);
    }
    return NULL;
}

/**
 * @brief 分配器包装函数，调用allocator->release完成内存释放。
 * 
 * @param allocator     分配器指针
 * @param memPtr        指向待释放的内存地址的指针，释放内存后该指针置NULL
 */
static inline void AllocatorRelease(const Allocator *allocator, void **memPtr)
{
    if (allocator != NULL && allocator->release != NULL) {
        allocator->release(memPtr, allocator->ctx);
    }
}

/**
 * @brief 分配器包装函数，调用allocator->ctor完成对象的构造。
 *
 * @param allocator     分配器指针
 * @param memPtr        对象的内存地址指针
 * @return true         对象构造成功
 * @return false        对象构造失败
 */
static inline bool AllocatorConstruct(const Allocator *allocator, void *memPtr)
{
    if (allocator != NULL && allocator->ctor != NULL) {
        return allocator->ctor(memPtr, allocator->ctx);
    }

    return true;
}

/**
 * @brief 分配器包装函数，调用allocator->dtor完成对象的析构。
 *
 * @param allocator     分配器指针
 * @param memPtr        对象的内存地址指针
 */
static inline void AllocatorDestruct(const Allocator *allocator, void *memPtr)
{
    if (allocator != NULL && allocator->dtor != NULL) {
        allocator->dtor(memPtr, allocator->ctx);
    }
}

/**
 * @brief 分配器包装函数，申请对象内存并调用构造函数进行构造，成功则返回创建的对象。
 *
 * @param allocator     分配器指针
 * @param memSize       申请的内存字节大小
 * @return void*        成功则返回内存地址，否则返回NULL
 */
static inline void *AllocatorNew(const Allocator *allocator, size_t memSize)
{
    void *memPtr = AllocatorAcquire(allocator, memSize);
    if (memPtr != NULL) {
        if (AllocatorConstruct(allocator, memPtr)) {
            return memPtr;
        }
        AllocatorRelease(allocator, &memPtr);
    }
    return NULL;
}

/**
 * @brief 分配器包装函数，调用对象析构函数并释放内存。
 * 
 * @param allocator     分配器指针
 * @param memPtr        指向待释放的内存地址的指针，释放内存后该指针置NULL
 */
static inline void AllocatorDelete(const Allocator *allocator, void *memPtr)
{
    AllocatorDestruct(allocator, memPtr);
    AllocatorRelease(allocator, &memPtr);
}

/**
 * @brief 获取默认的分配器
 *
 * @return 默认实现的资源分配器 
 */
const Allocator *GetDefaultAllocator(void);

#ifdef __cplusplus
}
#endif
#endif /* LIBSIM_ALLOCATOR_H_INCLUDED */
