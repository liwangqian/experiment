#ifndef LIBSIM_MESSAGE_QUEUE_H_INCLUDED
#define LIBSIM_MESSAGE_QUEUE_H_INCLUDED

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef enum cutils_mq_error_code {
    MQ_EOK,
    MQ_EPAUSE,
    MQ_EMEM,
    MQ_EPTR,
} cutils_mq_error_code_e;


typedef struct cutils_mq cutils_mq_t;

typedef struct cutils_mq_config {
    size_t queue_size;
} cutils_mq_config_t;

typedef size_t timespan_t;

cutils_mq_t *cutils_mq_create(const cutils_mq_config_t *config);
void cutils_mq_destroy(cutils_mq_t *self);

bool cutils_mq_try_push(cutils_mq_t *self, void *mesg);
bool cutils_mq_try_pop(cutils_mq_t *self, void **mesg);

int  cutils_mq_push(cutils_mq_t *self, void *mesg, timespan_t timeout);
int  cutils_mq_pop(cutils_mq_t *self, void **mesg, timespan_t timeout);

size_t cutils_mq_size(const cutils_mq_t *self);
size_t cutils_mq_capacity(const cutils_mq_t *self);
bool cutils_mq_empty(const cutils_mq_t *self);
bool cutils_mq_full(const cutils_mq_t *self);

#ifdef __cplusplus
}
#endif

#endif /* LIBSIM_MESSAGE_QUEUE_H_INCLUDED */
