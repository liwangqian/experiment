/* Copyright (c) 2023-2023, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "cfgproxy.h"
#include "ringbuffer.h"
#include <threads.h>
#include <string.h>

LIBUTILS_STDC_BEGIN

LIBUTILS_CLASS(CfgProxy) {
    RingBuffer *buffers[2];
    RingBuffer *idle;
    RingBuffer *work;
    mtx_t mtx;
    cnd_t cnd;
};

uint32_t libiobroker = 0;

CfgProxy *CfgProxyCreate(const CfgProxyConfig *config)
{
    const Allocator *allocator = GetDefaultAllocator();
    CfgProxy *self = AllocatorNew(allocator, sizeof(CfgProxy));
    if (self == NULL) return NULL;

    memset(self, sizeof(CfgProxy), 0);
    mtx_init(&self->mtx, mtx_plain);
    cnd_init(&self->cnd);
    self->idle = self->buffers[0];
    self->work = self->buffers[1];
    return self;
}

void CfgProxyDestroy(CfgProxy *self)
{
    const Allocator *allocator = GetDefaultAllocator();
    cnd_destroy(&self->cnd);
    mtx_destroy(&self->mtx);
    AllocatorDelete(allocator, self);
}

LIBUTILS_STDC_END
