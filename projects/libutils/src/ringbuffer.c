/* Copyright (c) 2023-2023, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "ringbuffer.h"
#include <stdlib.h>
#include <string.h>
#include <limits.h>

LIBUTILS_STDC_BEGIN

#define NPOS UINT32_MAX

LIBUTILS_CLASS(RingBuffer) {
    uint64_t capacity;
    uint64_t maxSize;
    Allocator allocator;
    uint64_t size;
    uint64_t head;
    uint64_t tail;
    void *buffer[];
};

static uint64_t clp2(uint64_t x)
{
    x = x - 1u;
    x = x | (x >> 1u);
    x = x | (x >> 2u);
    x = x | (x >> 4u);
    x = x | (x >> 8u);
    x = x | (x >> 16u);
    x = x | (x >> 32u);

    return x + 1u;
}

static inline uint64_t RingBufferNext(RingBuffer *self, uint64_t *curr)
{
    uint64_t index = (*curr)++;
    return index & (self->capacity - 1);
}

static inline uint64_t RingBufferNextTail(RingBuffer *self)
{
    if (RingBufferIsEmpty(self)) {
        return NPOS;
    }
    --self->size;
    return RingBufferNext(self, &self->tail);
}

static inline uint64_t RingBufferNextHead(RingBuffer *self)
{
    if (RingBufferIsFull(self)) {
        return NPOS;
    }
    ++self->size;
    return RingBufferNext(self, &self->head);
}

RingBuffer *RingBufferCreate(uint32_t maxSize, const Allocator *allocator)
{
    uint64_t alignedSize = clp2(maxSize);
    uint64_t nbytes = alignedSize * sizeof(void *) + sizeof(RingBuffer);
    RingBuffer *self = AllocatorAcquire(allocator, nbytes);
    if (self == NULL) {
        return NULL;
    }

    (void)memset(self, 0, nbytes);
    self->capacity = alignedSize;
    self->maxSize = maxSize;
    self->allocator = *allocator;
    return self;
}

void RingBufferDestroy(RingBuffer *self)
{
    RingBufferClear(self);
    void *tmp = (void *)self;
    AllocatorRelease(&self->allocator, &tmp);
}

const Allocator *RingBufferGetAllocator(const RingBuffer *self)
{
    return &self->allocator;
}

uint64_t RingBufferGetCapacity(const RingBuffer *self)
{
    return self->capacity;
}

uint32_t RingBufferGetMaxSize(const RingBuffer *self)
{
    return (uint32_t)self->maxSize;
}

uint32_t RingBufferGetSize(const RingBuffer *self)
{
    return (uint32_t)self->size;
}

bool RingBufferIsEmpty(const RingBuffer *self)
{
    return self->size == 0;
}

bool RingBufferIsFull(const RingBuffer *self)
{
    return self->size == self->maxSize;
}

bool RingBufferPush(RingBuffer *self, void *element)
{
    uint64_t index = RingBufferNextHead(self);
    if (index == NPOS) {
        return false;
    }
    self->buffer[index] = element;
    return true;
}

bool RingBufferPop(RingBuffer *self, void **element)
{
    uint64_t index = RingBufferNextTail(self);
    if (index == NPOS) {
        return false;
    }
    *element = self->buffer[index];
    self->buffer[index] = NULL;
    return true;
}

void RingBufferClear(RingBuffer *self)
{
    for (uint64_t i = 0; i < self->capacity; ++i) {
        if (self->buffer[i]) {
            AllocatorDelete(&self->allocator, self->buffer[i]);
            self->buffer[i] = NULL;
        }
    }
    self->head = 0;
    self->tail = 0;
    self->size = 0;
}

void RingBufferVisit(RingBuffer *self, const RingBufferVisitor *visitor)
{
    for (uint64_t index = self->tail; index < self->head; ++index) {
        void *element = self->buffer[index & (self->capacity - 1)];
        if (element != NULL) {
            visitor->visit(element, visitor->ctx);
        }
    }
}

LIBUTILS_STDC_END
