/*
 * Copyright (c) 2021, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "event.h"
#include "refcount.h"
#include <string.h>
#include <threads.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct EventListener {
    struct EventListener *next;
    EventCallback callback;
    void *listenerData;
    bool once;
} EventListener;

typedef struct EventList {
    struct EventList *next;

    /* 作为头节点，不定义成指针，便于链表遍历操作 */
    EventListener listener;

    /* 单链表尾部指针，便于快速从尾部添加节点 */
    EventListener *tail;

    /* 互斥锁，支持多线程 */
    mtx_t lock;

    /* 队列订阅的事件类型 */
    uint32_t event;

    /* 队列订阅事件的listener数量 */
    uint32_t count;
} EventList;

struct EventObserver {
    Allocator allocator;
    Allocator eventListAllocator;

    /* 作为头节点，不定义成指针，便于链表遍历操作 */
    EventList events;
    mtx_t lock;
};

static inline
EventListener *CreateListener(EventCallback callback, void *listenerData,
    bool once, Allocator *allocator)
{
    EventListener *listener = AllocatorAcquire(allocator, sizeof(EventListener));
    if (listener == NULL) {
        return NULL;
    }

    listener->next = NULL;
    listener->callback = callback;
    listener->listenerData = listenerData;
    listener->once = once;

    return listener;
}

static inline
void DestroyListener(EventListener **listener, Allocator *allocator)
{
    AllocatorRelease(allocator, (void**)listener);
}

static inline
void RemoveAllListeners(EventList *list, Allocator *allocator)
{
    EventListener *listener = list->listener.next;
    list->listener.next = NULL;
    list->tail = &list->listener;
    list->count = 0;

    while (listener) {
        EventListener *curr = listener;
        listener = listener->next;
        DestroyListener(&curr, allocator);
    }
}

static inline
void EventListDestruct(void *memPtr, void *ctx)
{
    EventList *list = (EventList *)memPtr;
    Allocator *allocator = (Allocator*)ctx;
    RemoveAllListeners(list, allocator);
    mtx_destroy(&list->lock);
}

static inline
RCHandle CreateEventList(uint32_t event, Allocator *allocator)
{
    RCHandle handle = RefCountCreate(sizeof(EventList), allocator);
    if (handle == RCHANDLE_INVALID) {
        return RCHANDLE_INVALID;
    }

    EventList *list = (EventList *)RefCountGetData(handle);
    if (mtx_init(&list->lock, mtx_plain) != thrd_success) {
        RefCountUnref(&handle);
        return RCHANDLE_INVALID;
    }

    list->next = NULL;
    (void)memset(&list->listener, 0, sizeof(list->listener));
    list->tail = &list->listener;
    list->event = event;
    list->count = 0;

    return handle;
}

static inline
void DestroyAllEventList(EventList *list, Allocator *allocator)
{
    while (list) {
        EventList *node = list;
        list = list->next;
        RCHandle handle = RefCountGetHandle(node);
        RefCountUnref(&handle);
    }
}

static inline
RCHandle PrependEventList(EventObserver *observer, uint32_t event)
{
    RCHandle handle = CreateEventList(event, &observer->eventListAllocator);
    if (handle == RCHANDLE_INVALID) {
        return RCHANDLE_INVALID;
    }

    EventList *list = (EventList *)RefCountGetData(handle);
    list->next = observer->events.next;
    observer->events.next = list;
    return RefCountRef(handle);
}

static inline
RCHandle FindEventList(EventObserver *observer, uint32_t event)
{
    EventList *list = observer->events.next;
    while (list) {
        if (list->event == event) {
            return RefCountRef(RefCountGetHandle(list));
        }
        list = list->next;
    }
    return RCHANDLE_INVALID;
}

static inline
RCHandle RemoveEventList(EventObserver *observer, uint32_t event)
{
    EventList *prev = &observer->events;
    EventList *curr = prev->next;
    while (curr) {
        if (curr->event == event) {
            prev->next = curr->next;
            return RefCountGetHandle(curr);
        }
        prev = curr;
        curr = curr->next;
    }
    return RCHANDLE_INVALID;
}

static inline
void AppendListener(EventList *list, EventListener *listener)
{
    list->tail->next = listener;
    list->tail = listener;
}

static inline
void PrependListener(EventList *list, EventListener *listener)
{
    listener->next = list->listener.next;
    list->listener.next = listener;
}

static inline
void AddListener(EventObserver *observer, uint32_t event,
    EventListener *listener, bool prepend)
{
    if (mtx_lock(&observer->lock) == thrd_success) {
        RCHandle handle = FindEventList(observer, event);
        if (handle == RCHANDLE_INVALID) {
            handle = PrependEventList(observer, event);
        }
        mtx_unlock(&observer->lock);

        EventList *list = (EventList *)RefCountGetData(handle);
        if (list != NULL && mtx_lock(&list->lock) == thrd_success) {
            prepend ? PrependListener(list, listener)
                    : AppendListener(list, listener);

            ++list->count;
            mtx_unlock(&list->lock);
        }

        RefCountUnref(&handle);
    }
}

static inline
EventListener *RemoveListener(EventList *list, EventCallback callback, void *listenerData)
{
    if (mtx_lock(&list->lock) == thrd_success) {
        EventListener *listener = &list->listener;
        while (listener->next) {
            EventListener *prev = listener;
            listener = listener->next;

            if (listener->callback == callback &&
                listener->listenerData == listenerData) {
                prev->next = listener->next;
                --list->count;

                /* 如果移除的是尾节点，则更新尾指针 */
                if (listener == list->tail) {
                    list->tail = prev;
                }

                mtx_unlock(&list->lock);
                return listener;
            }
        }

        mtx_unlock(&list->lock);
    }

    return NULL;
}

static inline
void NotifyEventSync(EventList *list, void *eventData, Allocator *allocator)
{
    EventListener *prev = &list->listener;
    EventListener *listener = list->listener.next;
    while (listener) {
        bool remove = listener->callback(list->event, eventData, listener->listenerData);
        if (listener->once || remove) {
            prev->next = listener->next;
            --list->count;

            if (list->tail == listener) {
                list->tail = prev;
            }

            DestroyListener(&listener, allocator);
            listener = prev;
        }
        prev = listener;
        listener = listener->next;
    }
}

uint32_t EventAppendListener(EventObserver *observer, uint32_t event,
    EventCallback callback, void *listenerData)
{
    if (observer == NULL || callback == NULL) {
        return -1;
    }

    EventListener *listener = CreateListener(callback, listenerData,
        false, &observer->allocator);
    if (listener == NULL) {
        return -2;
    }

    AddListener(observer, event, listener, false);
    return 0;
}

uint32_t EventAppendOnceListener(EventObserver *observer, uint32_t event,
    EventCallback callback, void *listenerData)
{
    if (observer == NULL || callback == NULL) {
        return -1;
    }

    EventListener *listener = CreateListener(callback, listenerData,
        true, &observer->allocator);
    if (listener == NULL) {
        return -2;
    }

    AddListener(observer, event, listener, false);
    return 0;
}

uint32_t EventPrependListener(EventObserver *observer, uint32_t event,
    EventCallback callback, void *listenerData)
{
    if (observer == NULL || callback == NULL) {
        return -1;
    }

    EventListener *listener = CreateListener(callback, listenerData,
        false, &observer->allocator);
    if (listener == NULL) {
        return -2;
    }

    AddListener(observer, event, listener, true);
    return 0;
}

uint32_t EventPrependOnceListener(EventObserver *observer, uint32_t event,
    EventCallback callback, void *listenerData)
{
    if (observer == NULL || callback == NULL) {
        return -1;
    }

    EventListener *listener = CreateListener(callback, listenerData,
        true, &observer->allocator);
    if (listener == NULL) {
        return -2;
    }

    AddListener(observer, event, listener, true);
    return 0;
}

uint32_t EventRemoveListener(EventObserver *observer, uint32_t event,
    EventCallback callback, void *listenerData)
{
    if (observer == NULL || callback == NULL) {
        return -1;
    }

    if (mtx_lock(&observer->lock) == thrd_success) {
        RCHandle handle = FindEventList(observer, event);
        mtx_unlock(&observer->lock);

        if (handle == RCHANDLE_INVALID) {
            return -2;
        }

        EventList *list = (EventList *)RefCountGetData(handle);
        EventListener *listener = RemoveListener(list, callback, listenerData);
        if (listener != NULL) {
            DestroyListener(&listener, &observer->allocator);
        }

        RefCountUnref(&handle);
        return 0;
    }

    return -3;
}

uint32_t EventRemoveAllListeners(EventObserver *observer, uint32_t event)
{
    if (observer == NULL) {
        return -1;
    }

    if (mtx_lock(&observer->lock) == thrd_success) {
        RCHandle handle = RemoveEventList(observer, event);
        mtx_unlock(&observer->lock);
        RefCountUnref(&handle);
        return 0;
    }

    return -2;
}

uint32_t EventListenerCount(EventObserver *observer, uint32_t event)
{
    if (observer == NULL) {
        return 0;
    }

    uint32_t count = 0;

    mtx_lock(&observer->lock);
    RCHandle handle = FindEventList(observer, event);
    mtx_unlock(&observer->lock);

    if (handle != RCHANDLE_INVALID) {
        EventList *list = (EventList *)RefCountGetData(handle);
        count = list->count;
    }
    RefCountUnref(&handle);    

    return count;
}


void EventEmit(EventObserver *observer, uint32_t event, void *eventData)
{
    if (observer == NULL) {
        return;
    }

    if (mtx_lock(&observer->lock) == thrd_success) {
        RCHandle handle = FindEventList(observer, event);
        mtx_unlock(&observer->lock);

        if (handle == RCHANDLE_INVALID) {
            return;
        }

        EventList *list = (EventList *)RefCountGetData(handle);
        if (mtx_lock(&list->lock) == thrd_success) {
            NotifyEventSync(list, eventData, &observer->allocator);
            mtx_unlock(&list->lock);
        }
        RefCountUnref(&handle);
    }
}

uint32_t EventOn(EventObserver *observer, uint32_t event,
    EventCallback callback, void *listenerData)
{
    return EventAppendListener(observer, event, callback, listenerData);
}

uint32_t EventOnce(EventObserver *observer, uint32_t event,
    EventCallback callback, void *listenerData)
{
    return EventAppendOnceListener(observer, event, callback, listenerData);
}

uint32_t EventOff(EventObserver *observer, uint32_t event,
    EventCallback callback, void *listenerData)
{
    return EventRemoveListener(observer, event, callback, listenerData);
}

EventObserver *EventCreateObserver(Allocator *allocator)
{
    if (allocator == NULL) {
        return NULL;
    }

    EventObserver *observer = AllocatorAcquire(allocator, sizeof(EventObserver));
    if (observer == NULL) {
        return NULL;
    }

    observer->allocator = *allocator;
    observer->eventListAllocator = *allocator;
    observer->eventListAllocator.ctor = NULL;
    observer->eventListAllocator.dtor = EventListDestruct;
    observer->eventListAllocator.ctx  = &observer->allocator;

    (void)memset(&observer->events, 0, sizeof(observer->events));

    if (mtx_init(&observer->lock, mtx_plain) != thrd_success) {
        AllocatorRelease(allocator, (void**)&observer);
        return NULL;
    }

    return observer;
}

void EventDestroyObserver(EventObserver **observer)
{
    if (observer == NULL || *observer == NULL) {
        return;
    }

    EventObserver *tmp = *observer;

    DestroyAllEventList(tmp->events.next, &tmp->eventListAllocator);
    mtx_destroy(&tmp->lock);

    AllocatorRelease(&tmp->allocator, (void**)observer);
}

#ifdef __cplusplus
}
#endif
