/* Copyright (c) 2023-2023, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "ringbuffer.h"
#include <catch2/catch.hpp>

LIBUTILS_CLASS(TestObj) {
    uint32_t id;
};

static uint32_t invokeCount = 0;

void PrintTestObj(void *elem, void *)
{
    invokeCount++;
    auto obj = (TestObj *)elem;
    printf("%u\n", obj->id);
}

TEST_CASE("test.libutils.ringbuffer")
{
    const Allocator *allocator = GetDefaultAllocator();
    RingBuffer *rb = RingBufferCreate(1024, allocator);
    REQUIRE(RingBufferGetCapacity(rb) == 1024);
    REQUIRE(RingBufferGetMaxSize(rb) == 1024);
    REQUIRE(RingBufferGetSize(rb) == 0);
    REQUIRE(RingBufferIsEmpty(rb));
    REQUIRE(!RingBufferIsFull(rb));

    for (uint32_t i = 0; i < 1025; ++i) {
        TestObj *elem = (TestObj *)AllocatorNew(allocator, sizeof(TestObj));
        elem->id = i;
        if (i < 1024) {
            REQUIRE(RingBufferPush(rb, elem));
        } else {
            REQUIRE(!RingBufferPush(rb, elem));
            AllocatorDelete(allocator, elem);
        }
    }

    REQUIRE(RingBufferGetSize(rb) == 1024);
    REQUIRE(!RingBufferIsEmpty(rb));
    REQUIRE(RingBufferIsFull(rb));

    RingBufferVisitor visitor = {
        .visit = PrintTestObj,
    };

    RingBufferVisit(rb, &visitor);
    REQUIRE(invokeCount == 1024);

    RingBufferClear(rb);

    REQUIRE(RingBufferGetSize(rb) == 0);
    REQUIRE(RingBufferIsEmpty(rb));
    REQUIRE(!RingBufferIsFull(rb));

    RingBufferDestroy(rb);
}

LIBUTILS_CLASS(TL_S) {
    uint32_t type;
    uint32_t len;
};

LIBUTILS_CLASS(ConfigSubTp) {
    TL_S tl;
    uint32_t key;
    uint32_t subTp;
    bool enable;
};
