/* Copyright (c) 2021, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <iostream>
#include <mutex>
#include <thread>
#include <vector>

struct lock_guard {
    lock_guard(std::mutex & mtx) : mtx{mtx}
    {
        mtx.lock();
        std::cout << "thread<"
                  << std::this_thread::get_id()
                  << "> get lock."
                  << std::endl;
    }

    ~lock_guard()
    {
        std::cout << "thread<"
                  << std::this_thread::get_id()
                  << "> release lock."
                  << std::endl;
        mtx.unlock();
    }
private:
    std::mutex &mtx;
};

int main(int argc, char *argv[])
{
    int value = 0;
    std::mutex mtx;
    std::vector<std::thread*> threads;
    for (auto i = 0; i < 10; ++i) {
        threads.push_back(new std::thread([&, i]() {
            lock_guard lk{mtx}; // mtx.lock();
            i % 2 ? (value += 1) : (value -= 1);
            // mtx.unlock();
        }));
    }

    for (auto &t : threads) {
        if (t->joinable()) t->join();
        delete t;
        t = nullptr;
    }

    std::cout << "main exiting..., value = " << value << std::endl;
    return 0;
}
