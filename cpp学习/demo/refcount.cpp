/* Copyright (c) 2021, LiWangQian<liwangqian@huawei.com> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <iostream>
#include <thread>
#include <atomic>

template <typename ObjectType>
class ReferCounted {
public:
    template <typename ...Args>
    ReferCounted(Args ...args)
        : count{1}, object{std::forward<Args>(args)...}
    {
        // nop
    }

    auto Ref()
    {
        return count.fetch_add(1);
    }

    auto UnRef()
    {
        return count.fetch_sub(1);
    }

    auto CountRef() const
    {
        return count.load();
    }

    ObjectType &Get()
    {
        return object;
    }

    const ObjectType &Get() const
    {
        return object;
    }

private:
    std::atomic<uint32_t> count{0};
    ObjectType object;
};

template <typename ObjectType>
class ReferCountedObject {
public:
    ~ReferCountedObject()
    {
        Reset();
    }

    template <typename ...Args>
    ReferCountedObject(Args ...args)
        : rc{new ReferCounted<Args...>{std::forward<Args>(args)...}}
    {
        std::cout << "construct object" << std::endl;
    }

    ReferCountedObject(const ReferCountedObject &other)
        : rc{other.rc}
    {
        if (rc != nullptr) {
            (void)rc->Ref();
        }
    }

    ReferCountedObject& operator=(const ReferCountedObject &other)
    {
        if (&other == this) {
            return *this;
        }

        Reset();
        rc = other.rc;
        if (rc != nullptr) {
            rc->Ref();
        }
        return *this;
    }

    void Reset()
    {
        if (rc->UnRef() == 1) {
            std::cout << "destruct object" << std::endl;
            delete rc;
        }
    }

    auto ReferCount() const
    {
        return rc == nullptr ? 0 : rc->CountRef();
    }

private:
    ReferCounted<ObjectType> *rc{nullptr};
};

int main(int argc, char *argv[])
{
    ReferCountedObject<int32_t> rcInt{0};
    {
        auto rcInt1 = rcInt;
	    std::cout << rcInt.ReferCount() << std:: endl;
    }
    std::cout << rcInt.ReferCount() << std:: endl;
}
