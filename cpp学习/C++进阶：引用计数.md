# C++进阶：引用计数

## 原理介绍

RC是Reference Count的缩写，中文翻译为“引用计数”。

引用计数是计算机编程语言中的一种内存管理技术，是指将资源的被引用次数保存起来，当被引用次数变为零时就将其释放的过程。使用引用计数可以实现自动资源管理的目的，同时引用计数还可以用于垃圾回收（GC）算法。

引用计数还应用于享元模式中，对于读多写少以及大量重复对象的场景（比如编译器）能够显著减少内存消耗并且提升程序性能。

![image-20210612232602424](C++%E8%BF%9B%E9%98%B6%EF%BC%9A%E5%BC%95%E7%94%A8%E8%AE%A1%E6%95%B0.assets/image-20210612232602424.png)

## 软件实现

要实现引用计数，就需要增加一个计数器，用来记录对象当前时刻被引用的次数，在新的对象构造时引用计数加1，对象析构时，引用计数减一，并且判断如果当前只有自己引用了这个对象则释放对象锁占用的资源，从而实现对象生命周期的自动管理。为了保证引用计数的线程安全，我们需要使用原子整数作为引用计数器。以下代码是使用C++实现的一种通用引用计数对象类。

```C++
template <typename ObjectType>
class ReferCounted {
public:
    template <typename ...Args>
    ReferCounted(Args ...args)
        : count{1}, object{std::forward<Args>(args)...}
    {
        // nop
    }

    auto Ref()
    {
        return count.fetch_add(1);
    }

    auto UnRef()
    {
        return count.fetch_sub(1);
    }

    auto CountRef() const
    {
        return count.load();
    }

    ObjectType &Get()
    {
        return object;
    }

    const ObjectType &Get() const
    {
        return object;
    }

private:
    std::atomic<uint32_t> count{0};
    ObjectType object;
};

template <typename ObjectType>
class ReferCountedObject {
public:
    ~ReferCountedObject()
    {
        Reset();
    }

    template <typename ...Args>
    ReferCountedObject(Args ...args)
        : rc{new ReferCounted<Args...>{std::forward<Args>(args)...}}
    {
        std::cout << "construct object" << std::endl;
    }

    ReferCountedObject(const ReferCountedObject &other)
        : rc{other.rc}
    {
        if (rc != nullptr) {
            (void)rc->Ref();
        }
    }

    ReferCountedObject& operator=(const ReferCountedObject &other)
    {
        if (&other == this) {
            return *this;
        }

        Reset();
        rc = other.rc;
        if (rc != nullptr) {
            (void)rc->Ref();
        }
        return *this;
    }

    void Reset()
    {
        if (rc->UnRef() == 1) {
            std::cout << "destruct object" << std::endl;
            delete rc;
        }
    }

    auto ReferCount() const
    {
        return rc == nullptr ? 0 : rc->CountRef();
    }

private:
    ReferCounted<ObjectType> *rc{nullptr};
};

int main(int argc, char *argv[])
{
    ReferCountedObject<int32_t> rcInt{0};
    {
        auto rcInt1 = rcInt;
	    std::cout << rcInt.ReferCount() << std:: endl;
    }
    std::cout << rcInt.ReferCount() << std:: endl;
    return 0;
}
```

程序运行输出：

```shell
construct object
2
1
destruct object
```

C语言版本的引用计数实现可以参考[C-ReferCount](...)，由于C语言没有现成的机制实现RAII手法，需要程序员手动调用Ref/UnRef进行对象的引用和解引用，相对来说会繁琐一些，并且容易出错导致引用计数泄露，导致内存无法释放。

