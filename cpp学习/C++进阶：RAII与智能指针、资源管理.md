# C++进阶：RAII与智能指针、资源管理

## 原理介绍

RAII是Resource Acquisition Is Initialization的缩写，中文翻译作“资源获取就是初始化”。是C++语言中用于资源管理，避免资源泄露的一种惯用法。

该惯用法的实现是基于C++语言中对象的构造和析构函数进行的，**<u>资源在对象构造时获取，在对象析构时释放</u>**，C++标准保证任何情况下，已构造的**对象**最终会销毁，即它的析构函数在**对象所在的作用域退出前**一定会被调用。

下面我们通过以下代码来测试C++对象的构造和析构行为：

```C++
struct Object {
    ~Object()
    {
        std::cout << "Object<" << id << "> is destructed..." << std::endl;
    }

    Object(int id) : id{id}
    {
        std::cout << "Object<" << id << "> is constructed..." << std::endl;
    }

private:
    int id{0};
};

// 全局对象，在main函数调用前就已经完成对象的构造
// 在main函数结束后，调用析构函数进行析构
Object obj1{1};

int main(int argc, char *argv[])
{
    std::cout << "main enter" << std::endl;
    Object obj2{2};

    { // 进入局部作用域
        Object obj3{3};
    } // 退出局部作用域，obj3被析构

    std::cout << "main exiting..." << std::endl;
    return 0;
}

```

运行输出：

```shell
Object<1> is constructed...
main enter
Object<2> is constructed...
Object<3> is constructed...
Object<3> is destructed...
main exiting...
Object<2> is destructed...
Object<1> is destructed...
```

## 应用场景

RAII惯用法可用于所有涉及资源申请和释放成对出现的场景，常见的包括锁资源的申请和释放、内存的申请和释放、文件句柄的打开和关闭、连接的打开和关闭等等。

下面我们以锁资源的申请和释放为例进行说明，代码如下：

```C++
struct lock_guard {
    lock_guard(std::mutex & mtx) : mtx{mtx}
    {
        mtx.lock();
        std::cout << "thread<"
            	  << std::this_thread::get_id()
            	  << "> get lock."
            	  << std::endl;
    }

    ~lock_guard()
    {
        std::cout << "thread<"
            	  << std::this_thread::get_id()
            	  << "> release lock."
            	  << std::endl;
        mtx.unlock();
    }
private:
    std::mutex &mtx;
};

int main(int argc, char *argv[])
{
    int value = 0;
    std::mutex mtx;
    std::vector<std::thread*> threads;
    // 创建10个线程，对value进行自加自减操作
    for (auto i = 0; i < 10; ++i) {
        threads.push_back(new std::thread([&, i]() {
            lock_guard lk{mtx}; // mtx.lock();
            i % 2 ? (value += 1) : (value -= 1);
            // mtx.unlock();
        }));
    }
	
    // 等待所有线程完成
    for (auto &t : threads) {
        if (t->joinable()) t->join();
        delete t;
        t = nullptr;
    }
	// 输出结果，value经过10个线程的自增自减操作后，应该为0
    std::cout << "main exiting..., value = " << value << std::endl;
    return 0;
}
```

上述代码，通过使用`lock_guard`，避免了在代码中显式的调用`lock/unlock`接口，使代码保持简洁的同时保证了代码的可靠性。代码运行输出如下：

```shell
thread<140625420093184> get lock.
thread<140625420093184> release lock.
thread<140625394730752> get lock.
thread<140625394730752> release lock.
thread<140625411639040> get lock.
thread<140625411639040> release lock.
thread<140625403184896> get lock.
thread<140625403184896> release lock.
thread<140625386276608> get lock.
thread<140625386276608> release lock.
thread<140625377822464> get lock.
thread<140625377822464> release lock.
thread<140625369368320> get lock.
thread<140625369368320> release lock.
thread<140625360914176> get lock.
thread<140625360914176> release lock.
thread<140625282270976> get lock.
thread<140625282270976> release lock.
thread<140625273816832> get lock.
thread<140625273816832> release lock.
main exiting..., value = 0
```

C++11标准库提供了锁资源的自动管理类，`std::lock_guard<mutex_type>`，这个类就是采用的RAII手法，在构造函数中调用`mutex`对象的`lock`成员函数获取锁，在析构函数中调用`mutex`对象的`unlock`成员函数释放锁，从而避免程序员手写`lock/unlock`调用，避免了因忘记调用`unlock`或者程序异常导致锁资源的泄露。

从前面的代码中，我们还可以看到，在`main`函数退出之前，需要对创建的进行合并(join/同步)处理，避免主函数退出后，线程资源泄露，这种情况其实我们也可以通过RAII手法来实现线程的自动合并/同步/分离。

C++标准库除了提供`lock_guard`用来管理锁资源，还有更灵活的`std::unique_lock<mutex_type>`锁资源管理器，以及采用RAII手法的还有智能指针`shared_ptr<type>`和`unique_ptr<type>`用来做对象内存管理的。
