# C++进阶：类型擦除

## 背景介绍

我们知道，C++属于强类型语言，所有的变量都有确定的类型，一个变量只能保存一种类型的数据。但是像javascript、lua等一类脚本语言，则属于弱类型语言，一个变量可以保存任意类型的数据。如何使用C++实现一个类型，能够保存任意可拷贝构造的对象，使得下面的代码可以运行起来。

```c++
let x = 123;
x = 123.456f;
x = "hello world";
let y = x;
std::cout << y.as<const char*>() << std::endl; // 输出：hello world
```

## 标准库std::any的实现研究

先通过以下代码片段来了解下std::any的用法：

```c++
namespace demo {

class any {
public:
    ~any();

    template <typename T>
    any(T t);

    any(const any& other);

    template <typename T>
    any &operator=(T v);

    any &operator=(const any& other);

    template <typename T>
    T &as();

    template <typename T>
    const T &as() const;

private:
    struct holder {
        virtual ~holder() = default;
        virtual holder *clone() = 0;
    };

    template <typename V>
    struct value_holder : holder {
        value_holder(V v) : v{v} {}
        holder *clone() override
        {
            return new value_holder<V>{v};
        }

        V v;
    };

    holder *value{nullptr};
};

any::~any()
{
    if (value != nullptr) {
        delete value;
    }
}

template <typename T>
any::any(T t)
{
    value = new value_holder<T>{t};
}

any::any(const any& other)
{
    value = other.value->clone();
}

template <typename T>
any &any::operator=(T v)
{
    delete value;
    value = new value_holder<T>{v};
    return *this;
}

any &any::operator=(const any& other)
{
    if (&other == this) {
        return *this;
    }
    delete value;
    value = other.value->clone();
    return *this;
}

template <typename T>
T &any::as()
{
    auto true_holder = dynamic_cast<value_holder<T>*>(value);
    if (true_holder != nullptr) {
        return true_holder->v;
    }
    throw std::bad_cast{};
}

template <typename T>
const T &any::as() const
{
    auto true_holder = dynamic_cast<const value_holder<T>*>(value);
    if (true_holder != nullptr) {
        return true_holder->v;
    }
    throw std::bad_cast{};
}

} // namespace demo

int main(int argc, char *argv[])
{
    using let = demo::any;

    let x = 123u;
    auto i = x.as<uint32_t>();
    std::cout << i << std::endl;
    let y = "hello world";
    std::cout << y.as<const char*>() << std::endl;
    y = 12.345f;
    std::cout << y.as<float>() << std::endl;
    auto z = y;
    std::cout << z.as<float>() << std::endl;
    return 0;
}
```

