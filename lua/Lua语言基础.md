# Lua语言基础

Lua语言是一门功能强大、高效、轻量级以及可嵌入的解析型脚本语言。它支持面向过程编程、面向对象编程、函数式编程、数据驱动编程等各种编程范式，同时也适合用于数据描述。

Lua语言语法简单、非常容易上手。Lua脚本通过Lua虚拟机翻译成字节码后执行，Lua虚拟机是使用C语言开发的，严格遵守ANSIC C标准，因此具有广泛的兼容性，可以在多种平台上运行，语言的设计初衷就是作为胶水语言嵌入到C/C++等宿主语言中。

## 关键字

在Lua语言中，主要包含22关键字：local、nil、function、if、else、elseif、then、do、end、while、for、repeat、until、in、and、or、not、break、true、false、goto、return。

## 运算符

Lua语言支持的运算符如下：

- 数学运算符：加`+`、减`-`、乘`*`、除`/`、取余`%`、求幂`^`、负`-`，这些运算符的操作数只能是number类型的量，否则会报异常。
- 关系运算符：等于`==`、不等于`~=`、小于`<`、大于`>`、小于等于`<=`、大于等于`>=`。
- 逻辑运算符：逻辑与`and`、逻辑或`or`、逻辑非`not`。
- 位操作符(since 5.3)：按位与`&`、按位或`|`、按位异或`~`、左移`<<`、右移`>>`、按位取反`~`，这些操作符的操作数只能是integer类型的，否则会报异常。

## 基础类型

Lua内置的数据类型有nil、number、boolean、string、table、thread、userdata、function，其中：

- nil比较特殊，既是类型，也是值，在Lua中任何未初始化的变量的类型都是nil

  ```lua
  local uninitialized
  print(type(uninitialized), uninitialized) --> nil	nil
  ```

- number类型包含了整数类型和浮点数类型。

  ```Lua
  local x = 0.1
  local y = math.tointeger(math.floor(x)) --> Lua 5.3以上的版本支持tointeger
  print(type(x), x) --> number	0.1
  print(type(y), y) --> number	0
  ```

- boolean为布尔类型，取值为true和false，需要注意的是，在Lua语言中，除了false和nil外，其他的值在条件表达式中的结果都是真值(true)。

  ```lua
  local x = true
  print(type(x), x) --> boolean	true
  local y   --> nil
  print(type(not y), not y) --> boolean	true
  print(type(not not y), not not y) --> boolean	false
  ```

- string类型的值是不可修改的，每次对string类型值的修改都会生成一个新的string类型值。Lua语言中，可以使用双引号或者单引号来定义<u>单行字符串</u>，也可以使用双中括号来定义<u>多行字符串</u>。

  ```lua
  local x = 'Hello World'
  local y = "Hello World"
  local z = [[
  大家好，我来自中国。
  我的祖国是一个伟大的国家。
  ]]
  print(type(x), type(y), type(z)) --> string	 string  string
  ```

  使用`..`来拼接字符串，也可以使用`string.format`来输出格式化字符串，格式化参数和C语言基本一致。

  ```lua
  local x = 'Hello' .. ' ' .. 'Wolrd' --> 'Hello World'
  local y = string.format('%s %s: %d', 'Hello', 'World', 2021)
  ```

- table类型在Lua中是实现复杂数据结构的基础，所有的复杂数据结构都可以通过table来实现，在Lua中，table可以存放任意数据，table表通过一对大括号`{}`来构造。

  ```lua
  local x = {} --> 构造一个空表
  local y = { name = 'kitty', age = 18 } --> 构造一个键值对的map表
  local z = { 'name', 'age', name = 'kitty', age = 18 } --> 构造数组和map混合表
  -- 等效于：z = { [1] = 'name', [2] = 'age', name = 'kitty', age = 18 }
  
  print(z[1], z[2], z.name, z.age) --> name age kitty 18
  -- 表的数组部分的索引从1开始，索引可以是负数，键值对map部分可以通过.来访问。
  ```

  支持使用`#`操作符来获取表的数组部分的长度，如果想要获取表中所有非nil元素的数量，只能使用循环遍历计算。

  ```lua
  local x = { 1, 2, 3 } --> #x == 3
  local y = { 1, 2, 3, name = 'kitty' } --> #x == 3
  local z = { 1, 2, [4] = 3 } --> #x == 2，数组中间不连续，出现了[3] = nil
  
  ```

- thread类型数据用于Lua的协程中，Lua虚拟机是单线程的，通过协程来实现多任务处理。

  ```lua
  local co = coroutine.create(function() print(100) end)
  print(type(co)) --> thread
  ```

- userdata用于Lua和C语言的混合编程中，代表的是C语言中定义的复杂数据类型(结构体)，当前我们主要在C化Lua中涉及到混合编程，用userdata比较少。

## 变量

在Lua语言中，使用local来定义一个局部变量，不带local的变量为全局变量。

```lua
local x = 1 -- x为局部变量
y = x -- y为全局变量
```



## 赋值语句



## 函数



## 表

Lua语言中的table是一种功能强大的类型，是Lua实现模块化编程、面向对象编程、数据描述型语言的基础。

## 模块

在Lua语言中，

## 面向对象编程



## 标准库介绍

Lua语言内置了一些标准库，主要包括：string库、table库、io库、os库、math库、coroutine库、utf8字符库以及debug调试库，下面我们对这些标准库进行简单的介绍。

### string库



### table库



### io库



### os库



### math库



### coroutine库



### utf8库



### debug库



## 搭建编程环境



## 白盒测试介绍



