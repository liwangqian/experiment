local cli = require('cli')

local instance = cli.instance()

instance:command {
    elements = {
        cli.keyword("display", "xxx"),
        cli.keyword("system", "xxx"),
        cli.keyword("cpu-usage", "xx"),
        cli.keyword("memory-usage", "xxx"),
        cli.keyword("brief", "brief information."),
        cli.keyword("verbose", "detail information."),
        cli.parameter("cpuid", "the cpu index.", cli.datatype.uint8(0, 64))
    },
    expressions = {
        "display system cpu-usage <cpuid> brief",
        "display system cpu-usage <cpuid> verbose",
        "display system memory-usage brief",
        "display system memory-usage verbose",
    },
    class = "display_system_info"
}

instance.cmd_tree.root:print()

-- while true do
--     io.write('~>')
--     local cmd = io.read()
--     if cmd == 'quit' then
--         break
--     end
--     instance:excute(cmd)
-- end

for k, v in pairs(cli.datatype) do
    local value = v()
    print(k, value:check(string.rep('F', 9)), value:help())
end

local lexer = require('pl.lexer')
local file = io.open('D:\\Works\\Gitee\\experiment\\projects\\cpplua\\src\\core\\lang\\parser.cpp')
for t, v in lexer.cpp(file) do
    print(t, v)
end
