local stack = {}

function stack.new()
    return setmetatable({ items = {} }, { __index = stack })
end

function stack:push(item)
    table.insert(self.items, item)
end

function stack:pop()
    assert(not self:empty(), 'pop on empty stack.')
    return table.remove(self.items)
end

function stack:top()
    return self.items[#(self.items)]
end

function stack:depth()
    return #(self.items);
end

function stack:empty()
    return self:depth() == 0
end

return stack
