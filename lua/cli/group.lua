local group = {}
function group.new(nodes)
    local g = { nodes = nodes }
    return setmetatable(g, { __index = group })
end

function group:push_back(new_node)
    for _, node in ipairs(self.nodes) do
        node.children = node.children or {};
        table.insert(node.children, new_node)
    end
end

return group
