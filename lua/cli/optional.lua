local optional = {}
local optional_node = { type = 'optional', name = '<cr>' }

function optional.new(nodes)
    local g = { nodes = nodes }
    local new_optional = setmetatable(g, { __index = optional })
    new_optional:push_back(optional_node)
end

function optional:push_back(new_node)
    for _, node in ipairs(self.nodes) do
        node.children = node.children or {};
        table.insert(node.children, new_node)
    end
end

return optional
